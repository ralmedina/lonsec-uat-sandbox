public class FundRating_TriggerHandler {

	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	public FundRating_TriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	private void validateResource(Fund_Rating__c[] pFRList){
		
		Set<Id> cfIdSet = new Set<Id>();
		Map<Id,Covered_Fund__c> cfIdCFMap = new Map<Id,Covered_Fund__c>();
		
		Set<Id> fIdSet = new Set<Id>();
		Map<Id,Fund__c> fIdFMap = new Map<Id,Fund__c>();
		
		//this will be used for controling fund rating to be on srf level if it is part of sector review plan
		Set<Id> srpFundIdSet = new Set<Id>();
		Map<Id,Fund__c> srpFundIdFMap = new Map<Id,Fund__c>();
		
		for (Fund_Rating__c FR : pFRList){
			if (Fr.Fund__c!=null)
				srpFundIdSet.add(FR.Fund__c);
				
			if (FR.Sector_Review_Fund__c!=null){
				cfIdSet.add(FR.Sector_Review_Fund__c);
			}else{
				fIdSet.add(FR.Fund__c);
			}
		}
		
		List<Covered_Fund__c> cfList = [select Id, Fund__c, (select SRP_Function__c, Resource_Name__c from Resources__r) from Covered_Fund__c where Id in :cfIdSet];
		for (Covered_Fund__c cf : cfList){
			cfIdCFMap.put(cf.Id, cf);
		}
		
		List<Fund__c> fList = [select Id, Included_in_SRP__c, (select SRP_Function__c, Resource_Name__c from Resources__r) from Fund__c where Id in :fIdSet OR Id in :srpFundIdSet];
		for (Fund__c f : fList){
			if (fIdSet.contains(f.Id))			fIdFMap.put(f.Id, f);
			if (srpFundIdSet.contains(f.Id))	srpFundIdFMap.put(f.Id, f);
		}
		
		for (Fund_Rating__c FR : pFRList){
			Boolean isValid = false;
			if (FR.Sector_Review_Fund__c!=null){
				Covered_Fund__c cf = cfIdCFMap.get(FR.Sector_Review_Fund__c);
				for (Resource__c r : cf.Resources__r){
					//if (FR.OwnerId == r.Resource_Name__c){
					if (UserInfo.getUserId() == r.Resource_Name__c){
						isValid = true;
					}
				}
			}else{
				Fund__c f = fIdFMap.get(FR.Fund__c);
				//if (f.Included_in_SRP__c)
					
				for (Resource__c r : f.Resources__r){
					if (UserInfo.getUserId() == r.Resource_Name__c){
						isValid = true;
					}
				}
			}
			
			if (!isValid)	FR.Fund__c.addError('Only fund-allocated Resources are allowed to create Fund Ratings');
			
			if (FR.Fund__c!=null){
				Fund__c f = srpFundIdFMap.get(FR.Fund__c);
				if (f.Included_in_SRP__c && FR.Sector_Review_Fund__c==null){
					FR.Sector_Review_Fund__c.addError('This fund is part of a sector review. Please create the Fund Rating on the Sector Review Fund level.');
				}
			}
		}
	}
	
	public void OnBeforeInsert(Fund_Rating__c[] pFRList){
		validateResource(pFRList);
	}
	
	static testMethod void test() {
        Sector__c s = Test_Util.createSector(null);
        insert s;
        
        List<Fund__c> fList = new List<Fund__c>();
        for (Integer i=0 ; i<20 ; i++){
        	Fund__c f = Test_Util.createFund(s.Id);
        	
        	fList.add(f);
        }
        insert fList;
        
        Contact c = new Contact(LastName='tc');
        insert c;
        
        Sector_Review_Plan__c srp = Test_Util.createSRP();
        insert srp;
        
        Included_Sector__c is = Test_Util.createIS(srp.Id, s.Id);
        insert is;
        
        Covered_Fund__c cf = Test_Util.createCF(srp.Id, fList[0].Id);
        insert cf;
        
        Covered_Fund__c cf2 = Test_Util.createCF(srp.Id, fList[1].Id);
        insert cf2;
        
        //User u = new User(LastName='r@test.com',Email='r@test.com');
        //insert u;
        
        Resource__c r = Test_Util.createResource(cf.Id,UserInfo.getUserId());
        r.Role__c = 'Lead Analyst';
        insert r;
        
        Fund_Rating__c FR = Test_Util.createFundRating(fList[0]);
        insert FR;
	}
}