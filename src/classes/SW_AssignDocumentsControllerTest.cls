@isTest()
Public Class SW_AssignDocumentsControllerTest{
    static testMethod void case1(){
        
        Account account = new Account();
        account.Name = 'Sample Account';
        insert account;
        
        Contact contact = new Contact();
        contact.FirstName = 'Sample';
        contact.LastName = 'Contact';
        contact.AccountId = account.Id;
        insert contact;
        
        Sector__c sector = new Sector__c();
        sector.Name = 'Sample Sector';
        insert sector;
        
        Fund__c fund = new Fund__c();
        fund.Name = 'Sample Fund';
        fund.Sector__c = sector.Id;
        insert fund;
        
        Document_Repository__c DocumentRepository = new Document_Repository__c();
        DocumentRepository.Name = 'Sample Document';
        DocumentRepository.Sector__c = sector.Id;
        DocumentRepository.Fund__c = fund.Id;
        insert DocumentRepository;
        
        Document_Repository__c DocumentRepository2 = new Document_repository__c();
        DocumentRepository2.Name = 'Sample Document 2';
        DocumentRepository2.Fund__c = fund.Id;
        DocumentRepository2.Sector__c = sector.Id;
        insert DocumentRepository2;
        
        
        Package_Repository__c PackageRepository = new Package_Repository__c();
        PackageRepository.Name = 'Sample Package Repository';
        insert PackageRepository;
        
        Package_Repository__c PackageRepository2 = new Package_Repository__c();
        PackageRepository2.Name = 'Sample Package Repository2';
        insert PackageRepository2;
        
        Package_Document__c PackageDocument = new Package_Document__c();
        PackageDocument.Document__c = DocumentRepository.Id;
        PackageDocument.Package_Repository__c = PackageRepository.Id;
        insert PackageDocument;
        
        Account_Package__c AccountPackage = new Account_package__c();
        AccountPackage.Client__c = account.Id;
        AccountPackage.Package__c = PackageRepository.Id;
        insert AccountPackage;
        
        Contact_Package__c ContactPackage = new Contact_Package__c();
        ContactPackage.Contact__c = contact.Id;
        ContactPackage.Package__c = PackageRepository.Id;
        insert ContactPackage;
        
        ApexPages.Standardcontroller standardCon = new ApexPages.Standardcontroller(DocumentRepository);
        SW_AssignDocumentsController Controller = new SW_AssignDocumentsController(standardCon);
        
        Controller.getPackageRepositories() ;
        Controller.getClientLists();
        Controller.getContactLists();
        Controller.packageRepId = PackageRepository.Id;
        Controller.getDocumentTable();
        Controller.unselectedDocumentWrapper[0].isChecked = true;
        Controller.addDocuments();
        Controller.documentWrapper[0].isChecked = true;
        Controller.removeDocuments();
        Controller.packageRepId = PackageRepository.Id;
        Controller.searchText = 'Sam';
        Controller.searchDocs();
        
        Controller.clientSearchText = 'Sam';
        Controller.searchPackages();
        
        Controller.accountWrapper[0].ischecked = true;
        Controller.removeAccountPackages();
        
        Controller.UnselectedConDocumentWrapperTab3[0].isChecked = true;
        Controller.removeContactPackagestab3();
        
        Controller.searchAccPackages = 'Sam';
        Controller.accountRepId = account.Id;
        Controller.removeContactPackagestab3();
        
        
        Controller.searchAccPackagestab4 = 'Sam';
        Controller.accountRepIdtab4 = account.Id;
        Controller.searchAccountPackagestab4();
        
        Controller.searchConPackagestab4 = 'Sam';
        Controller.userRepIdtab3 = contact.Id;
        Controller.searchContactPackagestab4();
        
        Controller.searchAccPackages = 'Sam';
        Controller.accountRepId = account.Id;
        Controller.searchAccountPackages();
        
        Controller.accountRepIdtab4  = account.Id;
        Controller.getAccountPackageTableTab4();
        
        Controller.userRepIdtab4 = contact.Id;
        Controller.getUserPackageTableTab4();
        
        Controller.UnselectedAccountDocumentWrapper[0].isChecked = true;
        Controller.addAccountPackages();
        
        Controller.UnselectedPackagesForUsersTab3[0].isChecked = true;
        Controller.addContactPackagestab3();
        
        Controller.ClonePackageDocument();
        Controller.CreateNewPackage();
        
        Controller.clientSearchText = 'Sam';
        Controller.searchAccountClient();
        
        Controller.accountPackages[0].isChecked = true;
        Controller.removeClients();
        
        Controller.UnselectedAccountPackages[0].isChecked = true;
        Controller.addClients();

        Controller.contactSearchText = 'Sam';
        Controller.searchContacts();
        
        Controller.ContactPackages[0].isChecked = true;
        Controller.removeContacts();
        
        Controller.UnselectedContactPackages[0].isChecked = true;
        Controller.addContacts();
        }
}