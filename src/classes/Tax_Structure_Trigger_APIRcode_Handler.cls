public class Tax_Structure_Trigger_APIRcode_Handler{

    public static void onAfterInsertUpdate(Set<Id> parentIdsFund){
    List<Fund__c> fundList = new List<Fund__c>();
        List<Fund__c> fundsToUpdate = new List<Fund__c>();
        //query fund fields and set to map/list
        fundList = [SELECT Id, Name, Manager__c, Sector__c, Sector__r.Name, Model_Portfolio__c, APIR_Code__c, Active_Benchmark_Name__c,
              (Select Fund__c, APIR_Code__c from Tax_Structures__r)
              FROM Fund__c Where Id IN :parentIdsFund];
        
        Map<Id, Set<Id>> sectorIDFundIdSetMap = new Map<Id, Set<Id>>();
        
        
        for(Fund__c fund: fundList){
          // create the fund wrapper for each fund
          //get the APIR codes from tax structure
          String tempAPIR = '';
          for(Tax_Structure__c ts : fund.Tax_Structures__r){
            if (ts.APIR_Code__c != null || ts.APIR_Code__c != ''){
              if (tempAPIR!=''){
                tempAPIR = tempAPIR + '; ' + ts.APIR_Code__c;
              }else{
                tempAPIR = ts.APIR_Code__c;
              }
            }
            
          }
          fund.APIR_Code__c = tempAPIR;
            fundsToUpdate.add(fund);
        }
        update fundsToUpdate;
    }

}