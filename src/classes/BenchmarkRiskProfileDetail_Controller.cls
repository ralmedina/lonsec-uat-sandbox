public class BenchmarkRiskProfileDetail_Controller{
    public List<Benchmark_Risk_Profile_Detail__c> brpList {get;set;}
    public List<brpdWrapper> oldBRPDWrapperList {get;set;}
    public List<brpdWrapper> newBRPDWrapperList {get;set;}
    public String brpId {get;set;}
    public String brpdId {get;set;}
    public Benchmark_Risk_Profile__c b {get;set;}
    
    public class brpdWrapper{
        public Benchmark_Risk_Profile_Detail__c brpd {get;set;}
        public Boolean isDeleted {get;set;}
        public Integer index {get;set;}
        
        public brpdWrapper(Benchmark_Risk_Profile_Detail__c pBrpd, Boolean pIsDeleted, Integer pIndex){
            brpd = pBrpd;
            isDeleted = pIsDeleted;
            index = pIndex;
        }
    }
    
    public BenchmarkRiskProfileDetail_Controller(ApexPages.standardcontroller controller){
        initialize();
    }
    
    public BenchmarkRiskProfileDetail_Controller(){
        initialize();
    }
    
    public void initialize(){ 
        try{
            brpId = Apexpages.currentPage().getParameters().get('brpId'); // benchmark risk profile Id (used if from benchmark risk profile page NEW)
            brpdId = Apexpages.currentPage().getParameters().get('Id'); // benchmark risk profile details Id (used if from benchmark risk profile details page NEW and UPDATE)
            
            oldBRPDWrapperList = new List<brpdWrapper>();
            newBRPDWrapperList = new List<brpdWrapper>();
            
            getRelatedBenchmarkRiskProfileDetais();
            
            Integer i = 0;
            for (Benchmark_Risk_Profile_Detail__c tempBRPD : brpList){
                oldBRPDWrapperList.add(new brpdWrapper(tempBRPD,false,i));
                i++;
            }
        }catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
            System.debug('!!!' + e.getMessage());
        }
    }
    
    public void getRelatedBenchmarkRiskProfileDetais(){
        try{
        List<Benchmark_Risk_Profile_Detail__c> brpListToUpdate = new List<Benchmark_Risk_Profile_Detail__c>();
        List<Benchmark_Risk_Profile_Detail__c> tempListToUpdate = new List<Benchmark_Risk_Profile_Detail__c>();
            if (brpId!=null && brpId!=''){
                b = [select Id, (select Id, Name, Benchmark__c, Benchmark_Code__c, Benchmark_Risk_Profile__c, Date__c, Current_Weight__c, Sector__c from Benchmark_Risk_Profile_Details__r) from Benchmark_Risk_Profile__c where Id = :brpId];
                brpList = b.Benchmark_Risk_Profile_Details__r;
            }else if (brpdId!=null && brpdId!=''){
                Benchmark_Risk_Profile_Detail__c brpd = [select Id, Name, Benchmark__c, Benchmark_Code__c, Benchmark_Risk_Profile__c, Date__c, Current_Weight__c, Sector__c from Benchmark_Risk_Profile_Detail__c where Id = :brpdId];
                b = [select Id, (select Id, Name, Benchmark__c, Benchmark_Code__c, Benchmark_Risk_Profile__c, Date__c, Current_Weight__c, Sector__c from Benchmark_Risk_Profile_Details__r) from Benchmark_Risk_Profile__c where Id = :brpd.Benchmark_Risk_Profile__c];
                brpList = b.Benchmark_Risk_Profile_Details__r;
            }
        }catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
            System.debug('!!!' + e.getMessage());
        }
    }
    
    public PageReference addBenchmarkRiskProfileDetail(){
        try{
        
        brpId = Apexpages.currentPage().getParameters().get('brpId');
        Date newdate = System.today().toStartofMonth();
        
        List<brpdWrapper> tempList = new List<brpdWrapper>();
        
        if (newBRPDWrapperList.size()>0)
            newBRPDWrapperList.add(new brpdWrapper(new Benchmark_Risk_Profile_Detail__c(Benchmark_Risk_Profile__c=b.Id,Date__c=newdate),false,newBRPDWrapperList[newBRPDWrapperList.size()-1].index+1));
        else
            newBRPDWrapperList.add(new brpdWrapper(new Benchmark_Risk_Profile_Detail__c(Benchmark_Risk_Profile__c=b.Id,Date__c=newdate),false,0));
            
        }catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }
        return null;
    }
    
    public PageReference save(){
        // All validations done on the page level
        List<Benchmark_Risk_Profile_Detail__c> brpdToUpsert = new List<Benchmark_Risk_Profile_Detail__c>();
        List<Benchmark_Risk_Profile_Detail__c> brpdToDelete = new List<Benchmark_Risk_Profile_Detail__c>();
        Decimal weight = 0;
        Set<Id> componentIdSet = new Set<Id>();
        Set<Id> sectorIdSet = new Set<Id>();
        for (brpdWrapper brpdw : oldBRPDWrapperList){
            if (!brpdw.isDeleted){
                if (sectorIdSet.contains(brpdw.brpd.Sector__c)){
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Sector already exists. Please select a different Sector.'));
                    return null;
                }
                sectorIdSet.add(brpdw.brpd.Sector__c);
                if (componentIdSet.contains(brpdw.brpd.Benchmark__c)){
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Benchmark should not be the same with the Benchmark.'));
                    return null;
                }
                componentIdSet.add(brpdw.brpd.Benchmark__c);
                if (brpdw.brpd.Current_Weight__c!=null){
                    if (brpdw.brpd.Current_Weight__c<=0){
                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Current Weight should be greater than 0.'));
                        return null;
                    }
                    brpdToUpsert.add(brpdw.brpd);
                    weight += brpdw.brpd.Current_Weight__c;
                }
                else{
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Current Weight should not be blank.'));
                    return null;
                }   
            }
            else    brpdToDelete.add(brpdw.brpd);
        }
        
        for (brpdWrapper brpdw : newBRPDWrapperList){
            if (!brpdw.isDeleted){
                if (sectorIdSet.contains(brpdw.brpd.Sector__c)){
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Sector already exists. Please select a different Sector.'));
                    return null;
                }
                sectorIdSet.add(brpdw.brpd.Sector__c);
                /*if (brpdw.brpd.Benchmark_Code__c == brpdw.brpd.Benchmark_Code__c){
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Benchmark Code should not be the same with the Benchmark Code.'));
                    return null;
                }*/
                if (componentIdSet.contains(brpdw.brpd.Benchmark__c)){
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Benchmark should not be the same with the Benchmark.'));
                    return null;
                }
                componentIdSet.add(brpdw.brpd.Benchmark__c);
                if (brpdw.brpd.Current_Weight__c!=null){
                    if (brpdw.brpd.Current_Weight__c<=0){
                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Current Weight should be greater than 0.'));
                        return null;
                    }
                    brpdToUpsert.add(brpdw.brpd);
                    weight += brpdw.brpd.Current_Weight__c;
                }
                else{
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Current Weight should not be blank.'));
                    return null;
                }       
            }
        }
        
        if (weight!=100 && brpdToUpsert.size()>0){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Total of all benchmark weights must be equal to 100'));
            return null;
        }
        
        if (brpdToUpsert.size()>0)    upsert brpdToUpsert;
        if (brpdToDelete.size()>0)    delete brpdToDelete;
        return new PageReference('/'+b.Id);
    }
    
    public PageReference cancel(){
        return new PageReference('/'+b.Id);
    }
    
    public void updateDate(){
        brpId = Apexpages.currentPage().getParameters().get('brpId');
        Date newdate = System.today().toStartofMonth();
        
        List<Benchmark_Risk_Profile_Detail__c> brpListToUpdate = new List<Benchmark_Risk_Profile_Detail__c>();
        List<Benchmark_Risk_Profile_Detail__c> tempListToUpdate = new List<Benchmark_Risk_Profile_Detail__c>();
        if (brpId!=null && brpId!=''){
            brpListToUpdate = [select Id, Name, Benchmark__c, Benchmark_Code__c, Benchmark_Risk_Profile__c, Date__c, Current_Weight__c, Sector__c from Benchmark_Risk_Profile_Detail__c where Benchmark_Risk_Profile__c = :brpId];
            //brpList = b.Benchmark_Risk_Profile_Details__r;
            //brpListToUpdate = b.Benchmark_Risk_Profile_Details__r;
            for(Benchmark_Risk_Profile_Detail__c brpd: brpListToUpdate){
                brpd.date__c = newdate;
                tempListToUpdate.add(brpd);
            }
            update tempListToUpdate;
        }
    }
}