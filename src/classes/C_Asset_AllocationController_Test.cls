@isTest()
public class C_Asset_AllocationController_Test{
    static testMethod void case1(){
        Sector__c sector = new Sector__c();
            sector.Name = 'Test Sector';
        insert sector;
        
        Fund__c fund = new Fund__c();
            fund.Name = 'Test Fund';
            fund.Sector__c = sector.Id;
        insert fund;
        
        Fund_Profile__c fundprofile = new Fund_Profile__c();
            fundprofile.Fund__c = fund.Id;
        insert fundprofile;
        
        Test.setCurrentPageReference(new PageReference('Page.C_Asset_Allocation'));
        System.currentPageReference().getParameters().put('fpId', fundprofile.Id);
        
        C_Asset_AllocationController controller = new C_Asset_AllocationController();
        controller.addAssetAllocation();
        controller.save();
        controller.cancel();
    }
}