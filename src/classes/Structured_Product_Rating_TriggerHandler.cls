public class Structured_Product_Rating_TriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    public Structured_Product_Rating_TriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
    Map<Id,Structured_Product_Review__c> sprIdSPRMap = new Map<Id,Structured_Product_Review__c>();
    
    private void validateResource(Structured_Product_Analyst_Rating__c[] pSPARList){
        
        Set<Id> sprIdSet = new Set<Id>();
        
        
        for (Structured_Product_Analyst_Rating__c spar : pSPARList){
            if (spar.Structured_Product_Review__c!=null){
                sprIdSet.add(spar.Structured_Product_Review__c);
            }
        }
        
        List<Structured_Product_Review__c> sprList = [select Id, Product_Type__c, (select Analyst_Manager__c from Stucture_Rating_Analysts__r) from Structured_Product_Review__c where Id in :sprIdSet];
        for (Structured_Product_Review__c spr : sprList){
            sprIdSPRMap.put(spr.Id, spr);
        }
        
        for (Structured_Product_Analyst_Rating__c spar : pSPARList){
            if (spar.Structured_Product_Review__c!=null){
                Boolean isValid = false;
                Structured_Product_Review__c spr = sprIdSPRMap.get(spar.Structured_Product_Review__c);
                for (Stucture_Rating_Analyst__c r : spr.Stucture_Rating_Analysts__r){
                    if (spar.OwnerId == r.Analyst_Manager__c){ // check if the creator of the record is a resource of the review parent
                        isValid = true;
                    }
                }
                
                if (!isValid)   spar.Structured_Product_Review__c.addError('Only allocated Analysts are allowed to create Structured Fund Review Ratings');
            }
        }
    }
    
    public void OnBeforeInsert(Structured_Product_Analyst_Rating__c[] pSPARList){
        validateResource(pSPARList);
    }
    
    public void OnAfterUpdate(Structured_Product_Analyst_Rating__c[] pSPARList){
        approveParent(pSPARList);
    }
    
    private void approveParent(Structured_Product_Analyst_Rating__c[] pSPARList){
        
        Set<Id> sprIdSet = new Set<Id>();
        for (Structured_Product_Analyst_Rating__c spar : pSPARList){
            
            //if (spar.Structured_Product_Analyst_Rating_Status__c == 'Approved')   
                sprIdSet.add(spar.Structured_Product_Review__c);
        }
        
        List<Structured_Product_Review__c> sprToApprove = new List<Structured_Product_Review__c>();
        Map<Id,Id>  sprIdRecordTypeId = new Map<Id,Id>();
        for (Structured_Product_Review__c spr : [select Id, Rating_Status__c, 
                                                    (select Structured_Product_Analyst_Rating_Status__c 
                                                    from Structured_Product_Analyst_Ratings__r) 
                                                from Structured_Product_Review__c where ID IN :sprIdSet]){
            Boolean setToApproved = true;
            for (Structured_Product_Analyst_Rating__c spar: spr.Structured_Product_Analyst_Ratings__r){
                if(spar.Structured_Product_Analyst_Rating_Status__c!='Approved'){   
                    setToApproved = false;
                    break;
                }
            }
            if (setToApproved){
                spr.Rating_Status__c = 'Approved';
            }else{
                spr.Rating_Status__c = 'In Progress';
            }
            
            sprToApprove.add(spr);
        }
        if (sprToApprove.size()>0)  update sprToApprove;
            
    }
    
    static testMethod void test () {
        Sector__c s = Test_Util.createSector(null);
        insert s;
        
        List<Fund__c> fList = new List<Fund__c>();
        for (Integer i=0 ; i<20 ; i++){
            Fund__c f = Test_Util.createFund(s.Id);
            f.Indexed__c = true;
            f.Structured_Product_Type__c = 'Passive - Loan';
            fList.add(f);
        }
        insert fList;
        
        Structured_Product_Review__c spr = new Structured_Product_Review__c(Product_Name__c=fList[0].Id);
        insert spr;
        
        Structured_Product_Analyst_Rating__c spar = new Structured_Product_Analyst_Rating__c(Structured_Product_Review__c=spr.Id);
        insert spar;
    }
}