/*Description: This is class is written to  get all tax structure records related 
				to current fund id and to populate related data on table 
@author      :Rimali Gurav  
@date        :27th May 2013
 */

public with sharing class PC_TaxStructurePerformanceController {
	
	public String debugStr {get;set;}
	public String debugStrStat {get;set;}
	public String mainJSONDebug {get;set;}
	
	//declaration of variables and list
	public Id currntPgeFundId{get;set;}
	public string selectedTaxName{get;set;}
	public list<SelectOption> lstTaxName = new list<SelectOption>();
	public string objTaxName;
	public list<PC_MatlabAPICall.wrapperclass> wlist {get;set;}
	public Growth_Series__c datePick {get;set;}
	
	public Boolean isCompositeBenchmark {get;set;}
	
	//Constructor deffination
	public PC_TaxStructurePerformanceController(){
		isCompositeBenchmark = false;
		debugStr = '';
		mainJSONDebug = '';
		debugStrStat = '';
		datePick = new Growth_Series__c();
		datePick.Month_End__c = date.today();
		// convert the selected date to the last day of the selected month
		datePick.Month_End__c = datePick.Month_End__c.addDays( (Date.daysInMonth( datePick.Month_End__c.year(),datePick.Month_End__c.month()) - datePick.Month_End__c.day() ) );
		//instantiating all the wrapperclass lists
		wlist = new list<PC_MatlabAPICall.wrapperclass>();
		currntPgeFundId = ApexPages.currentPage().getParameters().get('Id');//to get current record id
		
	}
	
	Map<String,Tax_Structure__c> tsIdTsMap = new Map<String,Tax_Structure__c>();
	
	//method to get picklist
	public void Initialize(){
		lstTaxName.add(new SelectOption('None','None'));//used to get first value of select option to be none
		//iterating over Tax_structure object to fill the select list with APIR code
		for(Tax_Structure__c objTax:[Select Fund__r.Cash_Benchmark_Code__c, 
											Fund__r.Cash_Benchmark_Code__r.Data_Provider__c,
											Name, 
											Id, 
											Fund__r.Id,
											Fund__r.Override_Fund_Benchmark__c,
											Fund__r.Sector__r.Benchmark__c,
											APIR_Code__c,
											(select Investment_Code__c from Investment_Codes__r where //Data_Provider__c = 'Financial Express' AND
											 Used_for_Growth_Series__c = true)
									 From Tax_Structure__c 
									 Where Id = :currntPgeFundId]){
									 	
			//added the Investment code values to list
			for (Investment_Code__c ic : objTax.Investment_Codes__r){						 	
				lstTaxName.add(new SelectOption(ic.Id,ic.Investment_Code__c));
			}
			// add ts to the map for reference oof the becnhmark id
			tsIdTsMap.put(objTax.Id,objTax);
	  }
	}
	public list<SelectOption> getTaxName(){
		return lstTaxName;
	}
	
	public List<Growth_Series__c> getGrowthSeries(String addedCondition, String referenceId){
		List<Growth_Series__c> tempGSList = new List<Growth_Series__c>();
		String qryString = 'Select Is_Lonsec_Data__c, Is_Validated_Data__c, Lonsec_Value__c, Month_End__c, Tax_Structure_Investment_Code__c, Value_FE__c, LastModifiedDate, Lonsec_Ex_Dev__c, Ex_Dev_FE__c From Growth_Series__c ';
		String condition = ' Where Tax_Structure_Investment_Code__c =\'' + referenceId + '\'';
		
		// this is to remove time from the date field to prevent error when querying
		addedCondition = addedCondition.replaceAll('00:00:00', '');
		
		tempGSList = database.query(qryString + condition + addedCondition + ' Order by Month_End__c DESC');
		
		// if true, it is called from the benchmark page
		if (tempGSList.size()<1){
			condition = ' Where Benchmark_Code__c =\'' 	+ referenceId + '\'';
			
			//condition = ' Where Benchmark__c =\'' + referenceId + '\'';
			tempGSList = database.query(qryString + condition + addedCondition + ' Order by Month_End__c DESC');
			//pageUsedFrom = 'Benchmark';
		}else{
			//pageUsedFrom = 'Tax Structure';
		}
		
		return tempGSList;
	}
	
	// get benchmark growthseries (composite or not)
	Map<String,String> benchmarkIdBenchmarkProviderMap = new Map<String,String>();
	Map<String,String> benchmarkIdBenchmarkCodeIdMap = new Map<String,String>();
	public String createBenchmarkJSONString(String mainBenchmarkId, String addedCondition){
		benchmarkIdBenchmarkProviderMap = new Map<String,String>();
		benchmarkIdBenchmarkCodeIdMap 	= new Map<String,String>();
		
		String benchmarkString = ''; // this contains the consolidated benchmark JSON string
		
		//List<Benchmark__c> bList = new List<Benchmark__c>();
		Map<Id,Double> bIdWeightMap = new Map<Id,Double>();
		Map<Id,List<Growth_Series__c>> benchmarkIdGrowthSeriesListMap = new Map<Id,List<Growth_Series__c>>();
		Map<Id,Date> benchmarkIdStartDateMap = new Map<Id,Date>(); // this contains the benchmark growthseries start date
		Map<Id,Date> benchmarkIdEndDateMap = new Map<Id,Date>(); // this contains the benchmark growthseries end date
		Benchmark__c mainBenchmark;
		List<Benchmark_Component__c> bcList = [	select Name, Composite_Benchmark__c, Component_Benchmark__c, Id, Benchmark_Weight__c,
												Component_Benchmark__r.Benchmark_Code_Used_For_Growth_Series__c,
												Component_Benchmark__r.Benchmark_Code_Used_For_Growth_Series__r.Data_Provider__c  
												from Benchmark_Component__c where Composite_Benchmark__c = :mainBenchmarkId];
		// if true, it is a composite benchmark
		if (bcList.size()>0){
			isCompositeBenchmark = true;
			for (Benchmark_Component__c tempBC : bcList){
				benchmarkIdBenchmarkProviderMap.put(tempBC.Component_Benchmark__c, tempBC.Component_Benchmark__r.Benchmark_Code_Used_For_Growth_Series__r.Data_Provider__c);
				benchmarkIdBenchmarkCodeIdMap.put(tempBC.Component_Benchmark__c, tempBC.Component_Benchmark__r.Benchmark_Code_Used_For_Growth_Series__c);
			}
		}else{
			isCompositeBenchmark = false;
			mainBenchmark = [select Id, Benchmark_Code_Used_For_Growth_Series__c, Benchmark_Code_Used_For_Growth_Series__r.Data_Provider__c from Benchmark__c where Id = :mainBenchmarkId];
		}
		
		if (bcList.size()>0){
			// get all benchmark components whose composite benchmark = mainBenchmarkId
			for (Benchmark_Component__c bc : bcList){
				
				// get weight of each benchmark component
				bIdWeightMap.put(bc.Component_Benchmark__c, bc.Benchmark_Weight__c/100);
				
				// putting query here using getGrowthSeries() since maximum related record is 5
				List<Growth_Series__c> tempGSList = getGrowthSeries(addedCondition, bc.Component_Benchmark__r.Benchmark_Code_Used_For_Growth_Series__c);
				benchmarkIdGrowthSeriesListMap.put(bc.Component_Benchmark__c, tempGSList);
				
				// minimum size of growth series data should be 2 since it cannot be computed if it has only one row of data
				if (tempGSList.size()>1){
					// get last record of the list since it is in DESC order.
					benchmarkIdStartDateMap.put(bc.Component_Benchmark__c, tempGSList[tempGSList.size()-1].Month_End__c);
					
					// get first record of the list since it is in DESC order.
					benchmarkIdEndDateMap.put(bc.Component_Benchmark__c, tempGSList[0].Month_End__c);
				}
			}
			Date startDate = getCommonDate(benchmarkIdStartDateMap, 'start');
			Date endDate = getCommonDate(benchmarkIdStartDateMap, 'end');
			
			for (String key : benchmarkIdGrowthSeriesListMap.keySet()){
				List<Growth_Series__c> tempGSList = new List<Growth_Series__c>();
				for (Growth_Series__c gs : benchmarkIdGrowthSeriesListMap.get(key)){
					if (gs.Month_End__c >= startDate && gs.Month_End__c <= endDate)
						tempGSList.add(gs);
				}
				benchmarkString += (benchmarkString == '') ? '' : ',';
				benchmarkString += 	'{"Weight": ' + 
									bIdWeightMap.get(key) + ', ' + 
									PC_MatlabAPICall.convertToJSON(tempGSList, 'Benchmark', benchmarkIdBenchmarkProviderMap.get(key)) 
									+ '}';
			}
		}else{
			List<Growth_Series__c> gsList = getGrowthSeries(addedCondition, mainBenchmark.Benchmark_Code_Used_For_Growth_Series__c);
			benchmarkString = '{"Weight": 1, ' + PC_MatlabAPICall.convertToJSON(gsList, 'Benchmark', mainBenchmark.Benchmark_Code_Used_For_Growth_Series__r.Data_Provider__c) + '}';
		}
		return '"BenchmarkName": [ ' +benchmarkString + ' ]';
			
	}
	
	// This method returns the common date of the date map
	// @pDateMap contains map of benchmarkId and Dates (can be start date or end date)
	// @typeOfDate may be = start or end
	public Date getCommonDate(Map<Id,Date> pDateMap, String typeOfDate){
		// convert the selected date to the last day of the selected month
		datePick.Month_End__c = datePick.Month_End__c.addDays( (Date.daysInMonth( datePick.Month_End__c.year(),datePick.Month_End__c.month()) - datePick.Month_End__c.day() ) );
		
		// this is used to filter the date range which is upto 10yrs before date today
		Date dt = (typeOfDate == 'start') ? datePick.Month_End__c.addYears(-10) : datePick.Month_End__c;
		
		// initial start date is 10 yrs before the date selected in the date picker
		// initial end date is last day of current month
		Date finalDate = dt;
		
		for (Date d : pDateMap.values()){
			if (typeOfDate == 'start')	
				if (finalDate < d)	
					finalDate = d;
			else 
				if (finalDate > d)	
					finalDate = d;
		}
		return finalDate;
	}
	
	//method to diplay all table values using wrapperclass
	public void displayWrapperValue(){
		wlist.clear();
		
		if(selectedTaxName != 'None'){
			debugStrStat += 'selectedTaxName: '+selectedTaxName;
			try{
				// convert the selected date to the last day of the selected month
				datePick.Month_End__c = datePick.Month_End__c.addDays( (Date.daysInMonth( datePick.Month_End__c.year(),datePick.Month_End__c.month()) - datePick.Month_End__c.day() ) );
				
				// this is used to filter the date range which is upto 10yrs before date today
				Date dt = datePick.Month_End__c.addYears(-10);
			
				// get tax growthseries
				Investment_Code__c tempIC = [select Tax_Structure__c, Investment_Code__c, Data_Provider__c from Investment_Code__c where Id = :selectedTaxName];
				List<Growth_Series__c> tgsList = [	select Month_End__c, Lonsec_Value__c, Value_FE__c, Is_Validated_Data__c, Lonsec_Ex_Dev__c, Ex_Dev_FE__c,
														Is_Lonsec_Data__c
														from Growth_Series__c where Tax_Structure_Investment_Code__c = :selectedTaxName 
														AND Month_End__c <= :datePick.Month_End__c
														AND Month_End__c >= :dt 
														Order by Month_End__c DESC];
				
				debugStrStat += ' ==== size of Tax Growth Series is ' + tgsList.size();
				
				// get the tax structure's fund's benchmark growth series
				Tax_Structure__c tempTS = tsIdTsMap.get(tempIC.Tax_Structure__c);
				String benchmarkId = '';
				if (tempTS.Fund__r.Override_Fund_Benchmark__c!=null)	benchmarkId = tempTS.Fund__r.Override_Fund_Benchmark__c;
				else													benchmarkId = tempTS.Fund__r.Sector__r.Benchmark__c;
				
				/*List<Growth_Series__c> bgsList = [select Benchmark__c, Month_End__c, Lonsec_Value__c, Value_FE__c, Is_Validated_Data__c,
													Is_Lonsec_Data__c, Lonsec_Ex_Dev__c, Ex_Dev_FE__c
													from Growth_Series__c where Benchmark__c = :benchmarkId
													AND Month_End__c <= :datePick.Month_End__c
													AND Month_End__c >= :dt
													Order by Month_End__c DESC];
													
				
				debugStrStat += ' ==== size of Benchmark Growth Series is ' + bgsList.size();
				
				// get the DEFAULT cash reference which is "UBS Bank Bill Index"
				// WHERE TO GET THIS? HOW?
				*/
				// call the matlab API to pass the tax growth series and benchmark growth series data related to this tax structure
				String tgsJSON = PC_MatlabAPICall.convertToJSON(tgsList, 'Fund', tempIC.Data_Provider__c);
				tgsJSON = '"MyFund1": { "Benchmark": "BenchmarkName", ' + tgsJSON + '}';
				
				// create the Benchmark GrowthSeries JSON String
				// loop this for multiple benchmarks
				String bgsJSON = '';
				//String bgsJSON = '"BenchmarkName": [';
				String benchmarkWeights = '';
				
				/*
				// loop this for multiple benchmark weights
				benchmarkWeights = PC_MatlabAPICall.convertToJSON(bgsList, 'Benchmark', 'Lonsec');
				Double weight = 1;
				benchmarkWeights = '{"Weight": ' + weight + ', ' + benchmarkWeights + '}';
				
				bgsJSON = '"BenchmarkName": [' + benchmarkWeights + ']'; // this is for wrapping up one benchmark
				*/
				String dateCondition = ' AND Month_End__c <= ' + (Date)datePick.Month_End__c + ' AND Month_End__c >= ' + (Date)dt;
				
				bgsJSON = createBenchmarkJSONString(benchmarkId, dateCondition);
				
				String cgsJSON = '';
				// get the (Cash is also the benchmark growthseries) or DEFAULT cash reference which is "UBS Bank Bill Index"  
				if (tempTS.Fund__r.Cash_Benchmark_Code__c != null)
					cgsJSON = PC_MatlabAPICall.convertToJSON(getGrowthSeries(dateCondition, tempTS.Fund__r.Cash_Benchmark_Code__c), 'Cash', TempTS.Fund__r.Cash_Benchmark_Code__r.Data_Provider__c);
				else
					cgsJSON = PC_MatlabAPICall.getDefaultCashBasis();
				
				debugStrStat += ' |||| JSON TGS |||| ' + tgsJSON;
				debugStrStat += ' |||| JSON BGS |||| ' + bgsJSON;
				debugStrStat += ' |||| JSON CASH |||| ' + cgsJSON;
				
				// wrap JSON string to before passing to API
				String mainJSON = PC_MatlabAPICall.wrapJSONString(tgsJSON, bgsJSON, cgsJSON, 'Tax Structure Performance');
				debugStr = mainJSON;//tgsJSON +','+ bgsJSON +','+PC_MatlabAPICall.getDefaultCashBasis();
				mainJSONDebug = mainJSON;
				JSONParser parser = PC_MatlabAPICall.parseJSONResponse(mainJSON); // this returns the response of the matlab API fed to a JSONParser
				// start parsing the json string by each section
				debugStrStat = string.valueOf(parser);
				while (parser.nextToken() != null) {
		        	
		        	if (parser.getCurrentName()!=null){
		        		
		        		if (parser.getCurrentName() == 'TaxStructurePerformance'){
		        			System.debug('===== TaxStructurePerformance Text: '+parser.getText());
		        			debugStrStat+= ' parser.getText ========' + parser.getText();
		        			parser.nextToken();
		        			wlist = PC_MatlabAPICall.parseTaxStructurePerformance(parser.getText());
		        			debugStr = parser.getText();
		        		}	
		        	}
		        }
			}catch(Exception e){
	        	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage() + +e.getStackTraceString()));
	        	debugStrStat += 'EXCEPTION:: '+e.getMessage() + +e.getStackTraceString();
	        }
			
		}
	
	}
	/*public class wrapperclass{
	    //declaration of wrapper class variables
	    public String  categoryName {get;set;}
		public decimal w1mth{get;set;} // 1
		public decimal w3mth{get;set;} // 3
		public decimal w6mth{get;set;} // 6
		public decimal w1Yr{get;set;}  // 12
		public decimal w2Yr{get;set;}  // 24
		public decimal w3Yr{get;set;}  // 36
		public decimal w5Yr{get;set;}  // 60
		public decimal w7Yr{get;set;}  // 72
		public decimal w10Yr{get;set;} // 120
		
	}*/
	
	
}