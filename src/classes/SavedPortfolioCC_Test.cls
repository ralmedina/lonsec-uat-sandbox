@isTest()
public class SavedPortfolioCC_Test{
    static testMethod void case1_constructor_AND_save(){
        Account a = new Account();
            a.Name = 'Test Account';
        insert a;
        
        Contact c = new Contact();
            c.FirstName = 'Test First Name';
            c.LastName = 'Test Last Name';
            c.AccountId = a.Id;
        insert c;
        
        Profile pf = [SELECT Id FROM Profile WHERE Name='Custom HVC Portal User'];
        User u = new User();
            u.Alias = 'test';
            u.Email='test@testuser.com';
            u.EmailEncodingKey='UTF-8';
            u.LastName='testlastname';
            u.LanguageLocaleKey='en_US';
            u.LocaleSidKey='en_US';
            u.ProfileId = pf.Id;
            u.TimeZoneSidKey='America/Los_Angeles';
            u.UserName='testusername@test.com';
            u.ContactId = c.Id;
        insert u;
        
        Sector__c sector = new Sector__c();
            sector.Name = 'Test Sector';
        insert sector;
        
        Fund__c fund = new Fund__c();
            fund.Name = 'Test Fund';
            fund.Sector__c = sector.Id;
        insert fund;
        
        Tax_Structure__c taxStruc = new Tax_Structure__c();
            taxStruc.Tax_Structure__c = 'Test Tax';
            taxStruc.Tax_Structure_Type__c = 'Investment Trust (ITR)';
            taxStruc.Fund__c = fund.Id;
            taxStruc.Wholesale__c = true;
            taxStruc.Status__c = 'Active';
            taxStruc.Status_Date__c = System.today();
        insert taxStruc;
        
        Investment_Code__c invCode = new Investment_Code__c();
            invCode.Investment_Code__c = 'A111';
            invCode.Tax_Structure__c = taxStruc.Id;
        insert invCode;
        
        System.runAs(u) {
            SavedPortfolioCC controller =  new SavedPortfolioCC();
            controller.investCode = 'A111';
            controller.save();
        }
    }
    
    static testMethod void case1_getSavedPortfolios_AND_getSavedPortfoliosToEdit_AND_getDocumentList_AND_createPortfolio_AND_save_AND_savePortfolioFund_AND_searchInvestCode(){
        Account a = new Account();
            a.Name = 'Test Account';
        insert a;
        
        Contact c = new Contact();
            c.FirstName = 'Test First Name';
            c.LastName = 'Test Last Name';
            c.AccountId = a.Id;
        insert c;
        
        Profile pf = [SELECT Id FROM Profile WHERE Name='Custom HVC Portal User'];
        User u = new User();
            u.Alias = 'test';
            u.Email='test@testuser.com';
            u.EmailEncodingKey='UTF-8';
            u.LastName='testlastname';
            u.LanguageLocaleKey='en_US';
            u.LocaleSidKey='en_US';
            u.ProfileId = pf.Id;
            u.TimeZoneSidKey='America/Los_Angeles';
            u.UserName='testusername@test.com';
            u.ContactId = c.Id;
        insert u;
        
        Sector__c sector = new Sector__c();
            sector.Name = 'Test Sector';
        insert sector;
        
        Fund__c fund = new Fund__c();
            fund.Name = 'Test Fund';
            fund.Sector__c = sector.Id;
        insert fund;
        
        Tax_Structure__c taxStruc = new Tax_Structure__c();
            taxStruc.Tax_Structure__c = 'Test Tax';
            taxStruc.Tax_Structure_Type__c = 'Investment Trust (ITR)';
            taxStruc.Fund__c = fund.Id;
            taxStruc.Wholesale__c = true;
            taxStruc.Status__c = 'Active';
            taxStruc.Status_Date__c = System.today();
        insert taxStruc;
        
        Investment_Code__c invCode = new Investment_Code__c();
            invCode.Investment_Code__c = 'A';
            invCode.Tax_Structure__c = taxStruc.Id;
        insert invCode;
        
        Saved_Portfolio__c sp = new Saved_Portfolio__c();
            sp.Name = 'Test Portfolio';
            sp.Contact__c = c.Id;
        insert sp;
        
        Saved_Portfolio_Fund__c spf = new Saved_Portfolio_Fund__c();
            spf.Saved_Portfolio__c = sp.Id;
            spf.Fund__c = fund.Id;
            spf.Weighting__c = 100;
        insert spf;
        
        System.runAs(u) {
            SavedPortfolioCC controller =  new SavedPortfolioCC();
            controller.investCode = 'A111';
            controller.getSavedPortfolios();
            controller.getSavedPortfoliosToEdit();
            controller.selectedValue = sp.Id;
            controller.getDocumentList();
            controller.inputName = 'Test Class Portfolio';
            controller.checkShare = false;
            controller.createPortfolio();
            controller.save();
            controller.savePortfolioFund();
            controller.txtInvestCode = invCode.Investment_Code__c;
            controller.searchInvestCode();
            
            SavedPortfolioCC.TempFundWrapper handler = new SavedPortfolioCC.TempFundWrapper(fund.Id, 'tfwInvCode', 'tfwFundName', 'tfwStructName', 'tfwFundSector');
            handler.tfwselected = true;
            controller.addToSelectedPortfolioFund();
            controller.deleteSavedPortfolioFund();
            controller.allowEditSelectPortfolio();
            controller.updatePortfolio();
            controller.cancelCreate();
            controller.cancelUpdatePortfolio();
            controller.deletePortfolio();
            controller.showMessageRightPortfolio();
            controller.allowCopyPortfolio();
            controller.copyPortfolio();
        }
    }
}