public class FundAttribute_NewController {

	public Fund_Attribute__c fa {get;set;}
	public Id fundId {get;set;}
	//public String selName {get;set;}
	public String selectedValues {get;set;}
	public Id faId {get;set;}
	public String debugStr {get;set;}
	public String debugStr2 {get;set;}
	Set<String> selectedValuesSet = new Set<String>();

	public FundAttribute_NewController(Apexpages.standardController controller){
		faId = ApexPages.currentPage().getParameters().get('Id');
		fa = (Fund_Attribute__c)controller.getRecord();
		if (ApexPages.currentPage().getParameters().get('selName')!=null)
			fa.Fund_Attribute_Name__c = ApexPages.currentPage().getParameters().get('selName');
		
		fundId = (faId!=null) ? fa.Fund__c : ApexPages.currentPage().getParameters().get('fundId');
		fa.Fund__c = fundId;
		selectedValues = '';
		debugStr = '';
		getCurrentNameAndValues();
		
		/*faId = (ApexPages.currentPage().getParameters().get('faId')!=null) ? ApexPages.currentPage().getParameters().get('faId') : ApexPages.currentPage().getParameters().get('Id');
		fa = (faId!=null) ? [select Id, Fund_Attribute_Name__c, Fund_Attribute_Values__c, Fund__c from Fund_Attribute__c where Id = :faId] : new Fund_Attribute__c();

		fundId = (faId!=null) ? fa.Fund__c : ApexPages.currentPage().getParameters().get('fundId');
		fa.Fund__c = fundId;
		selectedValues = '';
		debugStr = '';
		getCurrentNameAndValues();*/
	}
	
	Map<String,Fund_Attribute__c> faNameFAMap = new Map<String,Fund_Attribute__c>();
	
	public void getCurrentNameAndValues(){
		faNameFAMap = new Map<String,Fund_Attribute__c>();
		Fund__c f;
		if (faId!=null)
			f = [select Id, (select Id, Fund__c, Fund_Attribute_Name__c, Fund_Attribute_Values__c from Fund_Attribute__r where Id != :faId) from Fund__c where Id = :fundId];
		else
			f = [select Id, (select Id, Fund__c, Fund_Attribute_Name__c, Fund_Attribute_Values__c from Fund_Attribute__r) from Fund__c where Id = :fundId];
		//List<SelectOption> optionList = new List<SelectOption>();
		for (Fund_Attribute__c fa : f.Fund_Attribute__r){
			//optionList.add(new SelectOption(fa.Fund_Attribute_Name__c+'-'+fa.Fund_Attribute_Values__c, fa.Fund_Attribute_Name__c+'-'+fa.Fund_Attribute_Values__c));
			//selectedValues += (selectedValues!='') ? '0'+fa.Fund_Attribute_Name__c+'-'+fa.Fund_Attribute_Name_Value__c : fa.Fund_Attribute_Name__c+'-'+fa.Fund_Attribute_Name_Value__c;
			String[] values = String.valueOf(fa.Fund_Attribute_Values__c).split(';');
			for (Integer i=0; i<values.size(); i++){
				selectedValuesSet.add(fa.Fund_Attribute_Name__c+'-'+values[i]);
				//selectedValues += (selectedValues!='') ? '0'+fa.Fund_Attribute_Name__c+'-'+values[i] : fa.Fund_Attribute_Name__c+'-'+values[i];
			}
			faNameFAMap.put(fa.Fund_Attribute_Name__c,fa);
		}
		//return optionList;
	}
	
	
	
	//public List<selectOption> getPickValues(String field_name, String first_val) {
    public List<selectOption> getPickValues() {
    	
		List<selectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
	  	//if (first_val != null) { //if there is a first value being provided
	  	//   options.add(new selectOption(first_val, first_val)); //add the first option
	  	//}
	  	Schema.sObjectType sobject_type = Fund_Attribute__c.getSObjectType(); //grab the sobject that was passed
	  	Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
	  	Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
	  	
	  	String selectedName = (fa.Fund_Attribute_Name__c!=null) ? fa.Fund_Attribute_Name__c : '';
	  	if (field_map.containsKey('Value_'+selectedName.replaceAll(' ','_')+'__c')){
		  	List<Schema.PicklistEntry> pick_list_values = field_map.get('Value_'+selectedName.replaceAll(' ','_')+'__c').getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
		  	for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
		  		if (!selectedValuesSet.contains(selectedName+'-'+a.getValue()))
		        	options.add(new selectOption(a.getValue(), a.getLabel())); //add the value and label to our final list
		  	}
	  	}
	  	return options; //return the List
}
	public PageReference dummy(){
		/*PageReference newRef;
		if (faNameFAMap.containsKey(fa.Fund_Attribute_Name__c)){
    		fa = faNameFAMap.get(fa.Fund_Attribute_Name__c);
    		
    		newRef = Page.FundAttribute_New;
			//newRef.getParameters().put('fundId', fundId);
			newRef.getParameters().put('Id', fa.Id);
			//newRef.getParameters().put('selName', fa.Fund_Attribute_Name__c);
			newRef.setRedirect(true);
    		return newRef;
			 
    	}else{
    		//fa = faNameFAMap.get(fa.Fund_Attribute_Name__c);
    		//String selName = fa.Fund_Attribute_Name__c;
    		//if (faId!=null){
    		//	faId = null;
    		//	getCurrentNameAndValues();
    			
    		//}
    		//fa = new Fund_Attribute__c();
    		//fa.Fund_Attribute_Name__c = selName;
    		
    	}*/
    	return null;
	}
	
	public PageReference save(){
		if (!upsertRecord())	return null;
		if (faId!=null)	return new PageReference('/'+fa.Id);
		else			return new PageReference('/'+fundId);
	}
	
	public PageReference saveNew(){
		if (!upsertRecord())	return null;
		//fundId = ApexPages.currentPage().getParameters().get('fundId');
		//fa = new Fund_Attribute__c();
		//fa.Fund__c = fundId;
		//selectedValues = '';
		//debugStr = '';
		//getCurrentNameAndValues();
		//getPickValues();
		//return new PageReference('/apex/FundAttribute_New?fundId='+fundId);
		
		PageReference newRef = Page.FundAttribute_New;
		newRef.getParameters().put('fundId', fundId);
		newRef.setRedirect(true);
		return newRef;
		
	}
	
	public PageReference cancel(){
		if (faId!=null)	return new PageReference('/'+fa.Id);
		else			return new PageReference('/'+fundId);
	}
	
	public Boolean upsertRecord(){
		
		
		if (faNameFAMap.containsKey(fa.Fund_Attribute_Name__c)){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Selected attribute name exists for this Fund. Please select another one or edit the existing one.'));
			return false;
		}
		selectedValues = String.valueOf(selectedValues).replaceAll(', ','; ');
		selectedValues = selectedValues.removeStart('[');
		selectedValues = selectedValues.removeEnd(']');
		
		if (selectedValues=='' || selectedValues==null){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Please select an attribute value.'));
			return false;
		}
		
		fa.Fund_Attribute_Values__c = selectedValues;
		
		Boolean isOkay = true;
		Fund__c f;
		if (faId!=null)
			f = [select Id, (select Fund_Attribute_Name__c, Fund_Attribute_Values__c from Fund_Attribute__r where Id != :faId AND Fund_Attribute_Name__c = :fa.Fund_Attribute_Name__c AND Fund_Attribute_Values__c = :fa.Fund_Attribute_Values__c) from Fund__c where Id = :fundId];
		else
			f = [select Id, (select Fund_Attribute_Name__c, Fund_Attribute_Values__c from Fund_Attribute__r where Fund_Attribute_Name__c = :fa.Fund_Attribute_Name__c AND Fund_Attribute_Values__c = :fa.Fund_Attribute_Values__c) from Fund__c where Id = :fundId];
		//List<SelectOption> optionList = new List<SelectOption>();
		for (Fund_Attribute__c fa : f.Fund_Attribute__r){
			isOkay = false;
		}
		
		if (isOkay)	upsert fa;
		else {
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Selected attribute already exists for this Fund. Please select another one.'));
			return false;
		}
		return true;
	}

}