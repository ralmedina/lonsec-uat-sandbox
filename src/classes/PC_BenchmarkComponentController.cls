public class PC_BenchmarkComponentController {
	
	public List<Benchmark_Component__c> bcList {get;set;}
	public List<bcWrapper> oldBCWrapperList {get;set;}
	public List<bcWrapper> newBCWrapperList {get;set;}
	public String bId {get;set;}
	public String bcId {get;set;} 
	public Benchmark__c b {get;set;}
	public Integer indexToRemove {get;set;}
	
	public class bcWrapper{
		public Benchmark_Component__c bc {get;set;}
		public Boolean isDeleted {get;set;}
		public Integer index {get;set;}
		
		public bcWrapper(Benchmark_Component__c pBc, Boolean pIsDeleted, Integer pIndex){
			bc = pBc;
			isDeleted = pIsDeleted;
			index = pIndex;
		}
	}
	
	public PC_BenchmarkComponentController(ApexPages.standardcontroller controller){
		initialize();
	}
	
	public PC_BenchmarkComponentController(){
		initialize();
	}
	
	public void initialize(){
		
		try{
			bId = Apexpages.currentPage().getParameters().get('bId'); // benchmark Id (used if from benchmark page NEW)
			bcId = Apexpages.currentPage().getParameters().get('Id'); // benchmark component Id (used if from benchmark component page NEW and UPDATE)
			
			oldBCWrapperList = new List<bcWrapper>();
			newBCWrapperList = new List<bcWrapper>();
			
			getRelatedBenchmarkComponents();
			
			Integer i = 0;
			for (Benchmark_Component__c tempBC : bcList){
				oldBCWrapperList.add(new bcWrapper(tempBC,false,i));
				i++;
			}
		}catch(Exception e){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
		}
	}
	
	public void getRelatedBenchmarkComponents(){
		try{
			if (bId!=null && bId!=''){
				b = [select Id, (select Name, Composite_Benchmark__c, Component_Benchmark__c, Id, Benchmark_Weight__c from Benchmark_Components__r) from Benchmark__c where Id = :bId];
				bcList = b.Benchmark_Components__r;
			}else if (bcId!=null && bcId!=''){
				Benchmark_Component__c bc = [select Name, Composite_Benchmark__c, Id, Component_Benchmark__c from Benchmark_Component__c where Id = :bcId];
				b = [select Id, (select Name, Composite_Benchmark__c, Component_Benchmark__c, Id, Benchmark_Weight__c from Benchmark_Components__r) from Benchmark__c where Id = :bc.Composite_Benchmark__c];
				bcList = b.Benchmark_Components__r;
			}
		}catch(Exception e){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
		}
	}
	
	public PageReference addBenchmarkComponent(){
		try{
		List<bcWrapper> tempList = new List<bcWrapper>();
		//bcWrapperList = listToDisplay;
		/*for (bcWrapper bcw : bcWrapperList){ 
			tempList = bcWrapperList;
			if (tempList.size()>0)
				tempList.add(new bcWrapper(new Benchmark_Component__c(Component_Benchmark__c=b.Id),false,tempList[tempList.size()-1].index+1));
			else
				tempList.add(new bcWrapper(new Benchmark_Component__c(Component_Benchmark__c=b.Id),false,0));
			
		}*/
		if (newBCWrapperList.size()>0)
			newBCWrapperList.add(new bcWrapper(new Benchmark_Component__c(Composite_Benchmark__c=b.Id),false,newBCWrapperList[newBCWrapperList.size()-1].index+1));
		else
			newBCWrapperList.add(new bcWrapper(new Benchmark_Component__c(Composite_Benchmark__c=b.Id),false,0));
		//bcWrapperList = tempList;
		//listToDisplay = tempList;
		}catch(Exception e){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
		}
		return null;
	}
	
	/*public PageReference removeBC(){
		indexToRemove = Integer.valueOf(ApexPages.currentPage().getParameters().get('indexToRemove'));
		Integer i = 0;
		for (bcWrapper bcw : bcWrapperList){
			if (bcw.index == indexToRemove){
				break;
			}
			i++;
		}
		bcWrapperList.remove(i);
		return null;
	}*/
	
	
	public PageReference save(){
		// All validations done on the page level
		List<Benchmark_Component__c> bcToUpsert = new List<Benchmark_Component__c>();
		List<Benchmark_Component__c> bcToDelete = new List<Benchmark_Component__c>();
		Decimal weight = 0;
		//Set<Id> compositeIdSet = new Set<Id>();
		Set<Id> componentIdSet = new Set<Id>();
		for (bcWrapper bcw : oldBCWrapperList){
			if (!bcw.isDeleted){
				//if (compositeIdSet.contains(bcw.bc.Composite_Benchmark__c)){
				//	ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Composite benchmark already exists.'));
				//	return null;
				//}
				if (componentIdSet.contains(bcw.bc.Component_Benchmark__c)){
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Component Benchmark already exists. Please select a different Benchmark.'));
					return null;
				}
				//compositeIdSet.add(bcw.bc.Composite_Benchmark__c);
				componentIdSet.add(bcw.bc.Component_Benchmark__c);
				
				if (bcw.bc.Component_Benchmark__c == bcw.bc.Composite_Benchmark__c){
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Composite Benchmark should not be the same with the Component Benchmark.'));
					return null;
				}
				if (bcw.bc.Benchmark_Weight__c!=null){
					if (bcw.bc.Benchmark_Weight__c<=0){
						ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Benchmark weight should be greater than 0.'));
						return null;
					}
					bcToUpsert.add(bcw.bc);
					weight += bcw.bc.Benchmark_Weight__c;
				}
				else{
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Benchmark weight should not be blank.'));
					return null;
				}	
			}
			else				bcToDelete.add(bcw.bc);
		}
		
		for (bcWrapper bcw : newBCWrapperList){
			if (!bcw.isDeleted){
				//if (compositeIdSet.contains(bcw.bc.Composite_Benchmark__c)){
				//	ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Composite benchmark already exists.'));
				//	return null;
				//}
				if (componentIdSet.contains(bcw.bc.Component_Benchmark__c)){
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Component Benchmark already exists. Please select a different Benchmark.'));
					return null;
				}
				//compositeIdSet.add(bcw.bc.Composite_Benchmark__c);
				componentIdSet.add(bcw.bc.Component_Benchmark__c);
				if (bcw.bc.Component_Benchmark__c == bcw.bc.Composite_Benchmark__c){
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Composite Benchmark should not be the same with the Component Benchmark.'));
					return null;
				}
				if (bcw.bc.Benchmark_Weight__c!=null){
					if (bcw.bc.Benchmark_Weight__c<=0){
						ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Benchmark weight should be greater than 0.'));
						return null;
					}
					bcToUpsert.add(bcw.bc);
					weight += bcw.bc.Benchmark_Weight__c;
				}
				else{
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Benchmark weight should not be blank.'));
					return null;
				}		
			}
		}
		
		if (weight!=100 && bcToUpsert.size()>0){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Total of all benchmark weights must be equal to 100'));
			return null;
		}
		
		if (bcToUpsert.size()>0)	upsert bcToUpsert;
		if (bcToDelete.size()>0)	delete bcToDelete;
		return new PageReference('/'+b.Id);
	}
	
	public PageReference cancel(){
		
		return new PageReference('/'+b.Id);
	}
	
	
	/*private void validateBenchmarkWeight(Benchmark_Component__c[] pBCList, String action){
		
		Map<Id,Id> bcIdBMIdMap = new Map<Id,Id>();
		Map<Id,Double> bmIdBMWeightMap = new Map<Id,Double>();
				
		//Gather all Benchmarks involved
		for (Benchmark_Component__c bc : pBCList){
			if (bc.Component_Benchmark__c!=null){
				bcIdBMIdMap.put(bc.Id,bc.Component_Benchmark__c);
			}
		}
		
		//Gather all benchmark components of involved benchmarks and get current sum of benchmark weights of all the components
		List<Benchmark__c> benchmarkList = [select Id, (select Id, Benchmark_Weight__c from Benchmark_Components1__r) from Benchmark__c where Id in :bcIdBMIdMap.values()];
		for (Benchmark__c bm : benchmarkList){
			Double bmWeight = 0;
			
			for(Benchmark_Component__c existingBC : bm.Benchmark_Components1__r){
				if (action == 'insert'){
					bmWeight += existingBC.Benchmark_Weight__c;
				}else if (action == 'update'){
					if (!bcIdBMIdMap.containsKey(existingBC.Id))	bmWeight += existingBC.Benchmark_Weight__c;
				}
				
			}
			bmIdBMWeightMap.put(bm.Id, bmWeight);
		}
		
		// check if total benchmark weights of all components in a benchmark will exceed 100%
		for (Benchmark_Component__c bc : pBCList){
			Double existingTotalWeight = 0;
			if (bmIdBMWeightMap.containsKey(bc.Component_Benchmark__c)){
				existingTotalWeight = bmIdBMWeightMap.get(bc.Component_Benchmark__c);
			}
			
			if ((existingTotalWeight + bc.Benchmark_Weight__c) > 100){
				bc.Benchmark_Weight__c.addError('Total of Benchmark Weights must equal 100%. Please update the necessary benchmark components first before adding new ones.');
			}
		}
		
	}*/

}