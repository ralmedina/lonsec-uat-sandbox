public with sharing class FundManagementMainSearchController {


	final static Integer MAX_REC = 100;
	public String selectedPageNum {get;set;}
	public Integer totalPageNum {get;set;}
	public Map<Integer, List<fundWrapper>> mainList {get;set;}
	public List<fundWrapper> listToDisplay {get;set;}
	public List<SelectOption> pageNumOptions {get;set;}
	
	public PageReference getRefreshList(){
		listToDisplay = mainList.get(Integer.valueOf(selectedPageNum));
		return null;
	}
	
	public List<SelectOption> getPageNumbers(){
		pageNumOptions = new List<SelectOption>();
		for (Integer i=1 ; i<=totalPageNum ; i++){
			pageNumOptions.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
		}
		return pageNumOptions;
	}

	public String sortOrder {get;set;}
	public String sortColumn {get;set;}

	public String debugString {get;set;}
	public String debugString2 {get;set;}
	public String searchStr {get;set;}
	public List<fundWrapper> fundList {get;set;}
	
	public Boolean attFundIdentifier {get;set;}
	public Boolean attFundName {get;set;}
	public Boolean att1stSector {get;set;}
	public Boolean att2ndSector {get;set;}
	public Boolean attManager {get;set;}
	public Boolean attBenchmark {get;set;}
	public Boolean attRated {get;set;}
	
	
	public class fundWrapper{
		public String fundName {get;set;}
		public Fund__c fund {get;set;}
		public String fundIdentifier {get;set;}
		public String apir {get;set;}
		public String citiCode {get;set;}
		public String lonsecId {get;set;}
		
		public fundWrapper(Fund__c pFund, String pFundIdentifier, String pApir, String pCitiCode, String pLonsecID){
			//if (srchType == 'Sector')		fundName = pFund.Sector__r.Name;
			//else if (srchType == 'Fund')	fundName = pFund.Sector__r.Name + ' -> ' + pFund.Name;
			//else 							fundName = pFund.Sector__r.Name + ' -> ' + pFund.Name;
			fundName = pFund.Sector__r.Name + ' -> ' + pFund.Name;
			fund = pFund;
			fundIdentifier = pFundIdentifier;
			apir = pApir;
			citiCode = pCitiCode;
			lonsecID = pLonsecID;
		}
	}
	
	public List<SelectOption> getsearchOptions(){
		List<SelectOption> tempOpt = new List<SelectOption>();
		tempOpt.add(new SelectOption('Sector','Sector'));
		tempOpt.add(new SelectOption('Fund','Fund'));
		tempOpt.add(new SelectOption('Manager','Manager'));
		return tempOpt;
	}

	// Constructor
	public FundManagementMainSearchController(){
		
		pageNumOptions = new List<SelectOption>();
		selectedPageNum = '1';
		totalPageNum = 1;
		
		sortOrder = 'ASC';
		sortColumn = 'Name';
		
		debugString = '';
		searchStr = '';
		fundList = new List<fundWrapper>();
		attFundIdentifier 	= false;
		attFundName 		= false;
		att1stSector 		= false;
		att2ndSector 		= false;
		attManager 			= false;
		attBenchmark 		= false;
		attRated 			= false;
	}
	
	Map<Id,String> apirMap = new Map<Id,String>();
	Map<Id,String> citiMap = new Map<Id,String>();
	Map<Id,String> lonsecIDMap = new Map<Id,String>();
	
	public List<fundWrapper> getFundList(){
		
		mainList = new Map<Integer, List<fundWrapper>>();
		
		sortColumn = (ApexPages.currentPage().getParameters().get('sortColumn')!=null) ? ApexPages.currentPage().getParameters().get('sortColumn') : 'Name';
		sortOrder = (ApexPages.currentPage().getParameters().get('sortOrder')!=null) ? ApexPages.currentPage().getParameters().get('sortOrder') : 'ASC';
		
		String orderBy = ' Order By ' + sortColumn + ' ' + sortOrder;
		String qryStr 	= 'select Active_Benchmark_CITI_Code__c, Rated__c, Id, Name, Sector__c, Sector__r.Name, Sector__r.Sector__c, Manager__c, Manager__r.Name, Override_Fund_Benchmark__r.Name, Sector_Benchmark__c ';
		String condition = '';
		String tsFundIdStr = '';
		String conditionSub = '';
		String ratedCond = '';
		String ratedCondSub = '';
		
		fundList.clear();
		
		
		
		if (attFundName){
			condition += (condition!='') ? ' OR ' : '';
			condition += ' Name like \'%' + searchStr + '%\'';
			conditionSub += (conditionSub!='') ? ' OR ' : '';
			conditionSub += ' Fund__r.Name like \'%' + searchStr + '%\'';
		}
		if (att1stSector){
			condition += (condition!='') ? ' OR ' : '';
			condition += ' Sector__r.Sector__r.Name like \'%' + searchStr + '%\'';
			conditionSub += (conditionSub!='') ? ' OR ' : '';
			conditionSub += ' Fund__r.Sector__r.Sector__r.Name like \'%' + searchStr + '%\'';
		}
		if (att2ndSector){
			condition += (condition!='') ? ' OR ' : '';
			condition += ' Sector__r.Name like \'%' + searchStr + '%\'';
			conditionSub += (conditionSub!='') ? ' OR ' : '';
			conditionSub += ' Fund__r.Sector__r.Name like \'%' + searchStr + '%\'';
		}
		if (attManager){
			condition += (condition!='') ? ' OR ' : '';
			condition += ' Manager__r.Name like \'%' + searchStr + '%\'';
			conditionSub += (conditionSub!='') ? ' OR ' : '';
			conditionSub += ' Fund__r.Manager__r.Name like \'%' + searchStr + '%\'';
		}
		if (attBenchmark){
			condition += (condition!='') ? ' OR ' : '';
			condition += ' Override_Fund_Benchmark__r.Name like \'%' + searchStr + '%\' OR Sector_Benchmark__c like \'%' + searchStr + '%\'';
			conditionSub += (conditionSub!='') ? ' OR ' : '';
			conditionSub += ' Fund__r.Override_Fund_Benchmark__r.Name like \'%' + searchStr + '%\' OR Fund__r.Sector_Benchmark__c like \'%' + searchStr + '%\'';
		}
		if (attRated){
			//ratedCondSub += (conditionSub!='') ? ' AND ' : '';
			ratedCondSub += ' AND Fund__r.Rated__c = true ';
		}else{
			//ratedCondSub += (conditionSub!='') ? ' AND ' : '';
			ratedCondSub += ' AND Fund__r.Rated__c = true ';
		}
		
		String subqueryStr = 'Select Fund__c, APIR_Code__c, Legacy_Tax_Structure_ID__c from Tax_Structure__c where ';
		try{
		if (attFundIdentifier){
			if (conditionSub!='') conditionSub = ' OR ' +conditionSub;
			subqueryStr += '(APIR_Code__c like \'%' + searchStr + '%\'';
			subqueryStr += ' OR Legacy_Tax_Structure_ID__c like \'%' + searchStr + '%\'';
			subqueryStr += ' OR Fund__r.Active_Benchmark_CITI_Code__c like \'%' + searchStr + '%\' ';
			subqueryStr += conditionSub +') ';
			subqueryStr += ratedCondSub+' limit 1000';
			
			transient List<Tax_Structure__c> tempTS = database.query(subqueryStr);
			
			for (Tax_Structure__c ts : tempTS){
				tsFundIdStr += (tsFundIdStr!='') ? ', \''+ts.Fund__c+'\'' : '\''+ts.Fund__c+'\'';
				if (ts.APIR_Code__c!=null && ts.APIR_Code__c!='')
					if (apirMap.containsKey(ts.Fund__c)){
						apirMap.put(ts.Fund__c,apirMap.get(ts.Fund__c)+','+ts.APIR_Code__c);
					}else
						apirMap.put(ts.Fund__c,ts.APIR_Code__c);
				
				if (ts.Legacy_Tax_Structure_ID__c!=null && ts.Legacy_Tax_Structure_ID__c!='')
					if (lonsecIDMap.containsKey(ts.Fund__c)){
						lonsecIDMap.put(ts.Fund__c,lonsecIDMap.get(ts.Fund__c)+','+ts.Legacy_Tax_Structure_ID__c);
					}else
						lonsecIDMap.put(ts.Fund__c,ts.Legacy_Tax_Structure_ID__c);
			}
			if (tsFundIdStr!=''){
				if (condition!='')	tsFundIdStr = ' OR ID IN (' + tsFundIdStr + ') OR ';
				else 					tsFundIdStr = ' ID IN (' + tsFundIdStr + ') OR ' ;
			}
			condition += tsFundIdStr + ' Active_Benchmark_CITI_Code__c like \'%' + searchStr + '%\'';
		}else{
			subqueryStr += '(' + conditionSub + ') ';
			subqueryStr += ratedCondSub + ' limit 1000';
			
			transient List<Tax_Structure__c> tempTS = database.query(subqueryStr);
			
			for (Tax_Structure__c ts : tempTS){
				tsFundIdStr += (tsFundIdStr!='') ? ', \''+ts.Fund__c+'\'' : '\''+ts.Fund__c+'\'';
				if (ts.APIR_Code__c!=null && ts.APIR_Code__c!='')
					if (apirMap.containsKey(ts.Fund__c)){
						apirMap.put(ts.Fund__c,apirMap.get(ts.Fund__c)+','+ts.APIR_Code__c);
					}else
						apirMap.put(ts.Fund__c,ts.APIR_Code__c);
				
				if (ts.Legacy_Tax_Structure_ID__c!=null && ts.Legacy_Tax_Structure_ID__c!='')
					if (lonsecIDMap.containsKey(ts.Fund__c)){
						lonsecIDMap.put(ts.Fund__c,lonsecIDMap.get(ts.Fund__c)+','+ts.Legacy_Tax_Structure_ID__c);
					}else
						lonsecIDMap.put(ts.Fund__c,ts.Legacy_Tax_Structure_ID__c);
			}
			if (tsFundIdStr!=''){
				if (condition!='')	tsFundIdStr = ' OR ID IN (' + tsFundIdStr + ')';
				else 					tsFundIdStr = ' ID IN (' + tsFundIdStr + ')' ;
			}
			condition += tsFundIdStr;
		}
		}catch(exception e){
			debugString = 'ERROR IN SUBCONDITION:   '+subqueryStr;
			System.debug('subqueryStr: '+subqueryStr);
		}
		
		if (attRated){
			ratedCond += (condition!='') ? ' AND ' : '';
			ratedCond += ' Rated__c = true ';
		}else{
			ratedCond += (condition!='') ? ' AND ' : '';
			ratedCond += ' Rated__c = false ';
		}
		
		qryStr += ' from Fund__c ';
		condition = (condition!='') ? ' where (' + condition + ') ' + ratedCond + orderBy + ' limit 1000' : '';
		try{
		
		debugString = 'QUERY IN MAINCONDITION: '+qryStr+condition;
		for (Fund__c f : database.query(qryStr+condition)){
			String apir 	= (apirMap.containsKey(f.Id)) 		? apirMap.get(f.Id) 		: 'N/A';
			String citi 	= (f.Active_Benchmark_CITI_Code__c!='' && f.Active_Benchmark_CITI_Code__c!=null) ? f.Active_Benchmark_CITI_Code__c : 'N/A';
			String lonsecID = (lonsecIDMap.containsKey(f.Id)) 	? lonsecIDMap.get(f.Id) 	: 'N/A';
			fundList.add(new fundWrapper(f,apir+citi+lonsecID,apir,citi,lonsecID));
		}
		}catch(exception e){
			debugString = 'ERROR IN MAINCONDITION:   '+qryStr+condition;
		}
		
		Integer pageNum = 1;
		list<Fundwrapper> tempList = new List<fundWrapper>();
		for (fundWrapper fw : fundList){
			if (tempList.size() == MAX_REC){
				pageNum++;
				tempList = new list<Fundwrapper>();
				tempList.add(fw);
			}else{
				tempList.add(fw);
			}
			mainList.put(pageNum,tempList);
		}
		totalPageNum = pageNum;
		listToDisplay = mainList.get(1);
		
		return fundList;
	}
	
	public void goSearch(){
		
		if (validate()) return;
		getFundList();
	}
	
	
	
	public String classInputPostalCode {get;set;}
	public String errorMessage {get;set;}
	
	public Boolean validate() {
		if (searchStr==null || searchStr=='' || searchStr.length()<3){
        	classInputPostalCode = 'error';  // put the errorclass, red borders
        	errorMessage = 'Please input at least 3 characters';
        	return true;
        }else if (	!attFundIdentifier &&
					!attFundName &&
					!att1stSector &&
					!att2ndSector &&
					!attManager &&
					!attBenchmark){
        	classInputPostalCode = 'error';  // put the errorclass, red borders
        	errorMessage = 'Please select atleast one search attribute except "Rated by Lonsec"';
        	return true;
        } 
        else {
        	classInputPostalCode = '';
        	errorMessage = '';
        	return false;
		}
	}
}