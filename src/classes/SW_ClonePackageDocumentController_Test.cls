@isTest
Public Class SW_ClonePackageDocumentController_Test{
    static testMethod void case1(){
    
        Account account = new Account();
        account.Name = 'Sample Account';
        insert account;
        
        Contact contact = new Contact();
        contact.FirstName = 'Sample';
        contact.LastName = 'Contact';
        contact.AccountId = account.Id;
        insert contact;
        
        Sector__c sector = new Sector__c();
        sector.Name = 'Sample Sector';
        insert sector;
        
        Fund__c fund = new Fund__c();
        fund.Name = 'Sample Fund';
        fund.Sector__c = sector.Id;
        insert fund;
        
        Document_Repository__c DocumentRepository = new Document_Repository__c();
        DocumentRepository.Name = 'Sample Document';
        DocumentRepository.Sector__c = sector.Id;
        DocumentRepository.Fund__c = fund.Id;
        insert DocumentRepository;
           
        Package_Repository__c PackageRepository = new Package_Repository__c();
        PackageRepository.Name = 'Sample Package Repository';
        insert PackageRepository;
        
        Package_Document__c PackageDocument = new Package_Document__c();
        PackageDocument.Document__c = DocumentRepository.Id;
        PackageDocument.Package_Repository__c = PackageRepository.Id;
        insert PackageDocument;
        
        Set<Id> docIds = new Set<Id>();
        docIds.add(DocumentRepository.Id);
        
        ApexPages.CurrentPage().getParameters().put('docs', String.valueOf(docIds));
        ApexPages.Standardcontroller standardCon = new ApexPages.Standardcontroller(PackageDocument);
        SW_ClonePackageDocumentController Controller = new SW_ClonePackageDocumentController(standardCon);
        
        
        Controller.PackageName = 'Sample Test Package';
        Controller.cloneDocuments();
    }
}