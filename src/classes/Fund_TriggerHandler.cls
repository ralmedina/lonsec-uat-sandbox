public class Fund_TriggerHandler {
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	public Fund_TriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	private void countParentSectorGrandchildFunds(Fund__c[] pFList){
		
		Set<Id> parentSectorIDSet = new Set<Id>();
		Set<Id> sectorIDSet = new Set<Id>();
		Set<Id> allChildSectorIDSet = new Set<Id>();
		
		for (Fund__c f : pFList){
			if (f.Sector__c!=null){
				sectorIDSet.add(f.Sector__c);
			}
		}
		
		// query the the child sector to get the parent sector id
		List<Sector__c> sectorsList = [select Id, Number_of_Sector_Funds__c, Sector__c from Sector__c where Id in :sectorIDSet];
		for (Sector__c s : sectorsList){
			if (s.Sector__c!=null){
				parentSectorIDSet.add(s.Sector__c);
			}
		}
		
		
		// 1st loop to count child funds of each child sector
		List<Sector__c> allSectorsList = [select Id, Number_of_Sector_Funds__c, Sector__c, Number_of_Parent_Sector_Funds__c, (select Id from Funds__r) from Sector__c where Sector__r.Id in :parentSectorIDSet OR Id in :parentSectorIDSet];
		Map<Id,Integer> sectorIdSectorFundsMap = new Map<Id, Integer>();
		Map<Id,Integer> parentSectorIDTotalFundsofChildSectors = new Map<Id, Integer>();
		for (Sector__c s : allSectorsList){
			System.debug(' === s.Funds__r.size() == ' +s.Funds__r.size());
			//if (!parentSectorIDSet.contains(s.Id)){	
				if (s.Funds__r.size()>0){
					sectorIdSectorFundsMap.put(s.Id,s.Funds__r.size());
					s.Number_of_Sector_Funds__c = s.Funds__r.size();
				}else{
					sectorIdSectorFundsMap.put(s.Id,0);
					s.Number_of_Sector_Funds__c = 0;
				}
			//}
			if (s.Sector__c!=null){
				if (parentSectorIDTotalFundsofChildSectors.containsKey(s.Sector__c)){
					parentSectorIDTotalFundsofChildSectors.put(s.Sector__c, (parentSectorIDTotalFundsofChildSectors.get(s.Sector__c)+s.Funds__r.size()));
				}else{
					parentSectorIDTotalFundsofChildSectors.put(s.Sector__c, s.Funds__r.size());
				}
			}
		}
		
		// 2nd loop to assign total child funds of all child sectors to the parent sector
		for (Sector__c s : allSectorsList){
			if (parentSectorIDSet.contains(s.Id)){	
				s.Number_of_Parent_Sector_Funds__c = parentSectorIDTotalFundsofChildSectors.get(s.Id);
			}
		}
		
		System.debug(' ==== : '+parentSectorIDSet);
		
		if (allSectorsList.size()>0)	update allSectorsList;
	}
	
	public void OnAfterInsertUpdateDelete(Fund__c[] pFList){
		countParentSectorGrandchildFunds(pFList);
	}
}