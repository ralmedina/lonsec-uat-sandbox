public without sharing class SitePOCController {
	
	public List<Document_Repository__c> drList {get;set;}
	static Map<Id, Attachment> attMap = new Map<Id, Attachment>();
	public SitePOCController(){
		getDocuments();
	}
	
	public void getDocuments(){
		drList = [	Select SystemModstamp, Sector__c, Publish_Date__c, OwnerId, Name, 
					IsDeleted, Id, Fund__c, Document_Type__c, CreatedDate, CreatedById, Author__c,
					(select Id from Attachments)
					From Document_Repository__c];
		
		
		for (Attachment tempA : [Select SystemModstamp, ParentId, OwnerId, Name, 
								IsPrivate, IsDeleted, Id, Description, 
								ContentType, BodyLength, Body From Attachment ]){
			attMap.put(tempA.Id,tempA);
		}
		
	}
	Attachment att2;
	public PageReference redirectPage(){
		PageReference pr = new PageReference('/servlet/servlet.FileDownload?file='+att2.Id);
		return pr;
	}
	
	public void cloneDoc(){
		try{
			String drId = apexpages.currentPage().getParameters().get('drId');
			String docId = apexpages.currentPage().getParameters().get('docId');
			//Document_Repository__c sDR = [select Id from Document_Repository__c where Id = :drId];
			//Document_Repository__c newDR = sDR.clone(false,true,false,false); 
			//insert newDR;
			
			User u = [select ContactId from User where Id = :UserInfo.getUserId()];
			Document_Access__c da = new Document_Access__c();
			da.Contact__c = u.ContactId;
			insert da;
			
			//ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.severity.INFO, sDR.Id + ' === ' + newDR.Id);
		
			Attachment att = [select id, name, body from Attachment where ParentId = :drId];
			 
            //Attachment att2 = new Attachment(name = att.name, body = att.body, parentid = newDR.Id);
            att2 = new Attachment(name = att.name, body = att.body, parentid = da.Id);
            insert att2;

			// create code to download the document on the page add a code aftercomplete event to open it
			
			//PageReference pr = new PageReference('/servlet/servlet.FileDownload?file='+att2.Id);
			
			//return null;
		}catch(Exception e){
			ApexPages.Message myMsg3 = new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage()+e.getLineNumber());
			
			//return null;
		}
	}
	
	public void downloadDoc(){
		try{
			String docId = apexpages.currentPage().getParameters().get('docId');
			
			// create code to copy the document
			User u = [select ContactId from User where Id = :UserInfo.getUserId()];
			Document_Access__c da = new Document_Access__c();
			da.Contact__c = u.ContactId;

			insert da;
			
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.INFO, da.Id + ' === ');
			
			Attachment att = [	select Name, IsPrivate, IsDeleted, Id, Description, 
								ContentType, BodyLength, Body from Attachment where Id = :docId];
			
			Attachment a = att;
			a.ParentId = da.Id;
			insert a;
			
			ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.severity.INFO, da.Id + ' === ' + a.Id);
		
			// create code to download the document on the page add a code aftercomplete event to open it
		}catch(Exception e){
			ApexPages.Message myMsg3 = new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage()+e.getLineNumber());
		}
	}
}