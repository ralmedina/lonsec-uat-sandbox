public class SRP_CalculateRatingController {
	
	public String srpId {get;set;}
	public Sector_Review_Plan__c srp {get;set;}
	public String debugString {get;set;}

	public SRP_CalculateRatingController(ApexPages.StandardController controller){
		srpId = ApexPages.currentPage().getParameters().get('Id');
	}
	
	public PageReference runCalculations(){
		srp = 	[select Id, 
					(select Id, Sector__c, Sector__r.Sector_Rating_Model__c from Included_Sectors__r), 
					(select Id, Fund__c, Fund__r.Sector__c, Raw_Fund_Score__c from Covered_Funds__r) 
				from Sector_Review_Plan__c 
				where id = :srpId];
		
		Set<Id> sectorIdSet = new Set<Id>();
		Set<Id> fundIdSet = new Set<Id>();
		
		Map<Id,Id> sectorIDModelRatingIdMap = new Map<Id,Id>();
		Map<Id,Double> sectorIDMaxRatingMap = new Map<Id,Double>();
		
		for (Included_Sector__c is : srp.Included_Sectors__r){
			if (is.Sector__c!=null){
				sectorIdSet.add(is.Sector__c);
				sectorIDModelRatingIdMap.put(is.Sector__c,is.Sector__r.Sector_Rating_Model__c);
			}
		}
		
		for (Covered_Fund__c cf : srp.Covered_Funds__r){
			if (cf.Fund__c!=null){
				if (sectorIdSet.contains(cf.Fund__r.Sector__c)) // this is to check if the fund is part of a sector in the srp	
					fundIdSet.add(cf.Fund__c);
					
				// get the max raw fund score of all funds included in the srp per sector 
				if (sectorIDMaxRatingMap.containsKey(cf.Fund__r.Sector__c)){
					if (sectorIDMaxRatingMap.get(cf.Fund__r.Sector__c) < cf.Raw_Fund_Score__c)	sectorIDMaxRatingMap.put(cf.Fund__r.Sector__c, cf.Raw_Fund_Score__c);
				}else{
					sectorIDMaxRatingMap.put(cf.Fund__r.Sector__c, cf.Raw_Fund_Score__c);
				}
			}
		}
		
		List<Fund__c> fundsToUpdate = [select Fund_Rating_Model__c, Id, Sector__c, Raw_Fund_Score__c from Fund__c where Id IN :fundIdSet];
		for (Fund__c f : fundsToUpdate){
			if (sectorIDModelRatingIdMap.containsKey(f.Sector__c)){
				// the the fund's sector's rating model and assign it to the fund
				f.Fund_Rating_Model__c = sectorIDModelRatingIdMap.get(f.Sector__c);
			}
		}
		if (fundsToUpdate.size()>0)	update fundsToUpdate; // this updates the fund with the sector's current rating model
		
		List<Sector__c> sectorsToUpdate = [select Id, Max_Raw_Fund_Value__c from Sector__c where ID IN :sectorIDMaxRatingMap.keySet()];
		for (Sector__c s : sectorsToUpdate){
			s.Max_Raw_Fund_Value__c = sectorIDMaxRatingMap.get(s.Id);
		}
		if (sectorsToUpdate.size()>0)	update sectorsToUpdate; // this updates the sector with the max raw fund score among all the funds included in the srp
		
		runAutoFill();
		
		return redirectPage();
	}
	
	public PageReference runAutoFill(){
		List<Covered_Fund__c> cfList = 	[select Id, Normalized_Fund_Score__c, Model_Rating__c
										from Covered_Fund__c 
										where Sector_Review_Plan_Record__c = :srpId];
		
		for (Covered_Fund__c cf : cfList){
			if (cf.Normalized_Fund_Score__c > 85 )												cf.Model_Rating__c = 'HR';
			else if (cf.Normalized_Fund_Score__c <= 85 	&& cf.Normalized_Fund_Score__c > 75)	cf.Model_Rating__c = 'R';
			else if (cf.Normalized_Fund_Score__c <= 75 	&& cf.Normalized_Fund_Score__c > 65)	cf.Model_Rating__c = 'IG';
			else if (cf.Normalized_Fund_Score__c <= 65 	&& cf.Normalized_Fund_Score__c > 55)	cf.Model_Rating__c = 'H';
			else if (cf.Normalized_Fund_Score__c <= 55 )										cf.Model_Rating__c = 'Rd';
		}
		if (cfList.size()>0)	update cfList; // this updates the SRP funds with the correct model rating
		return null;
		//return redirectPage();
	}
	
	public PageReference redirectPage(){
		return new PageReference('/../'+srpId);
	}
	
	static testMethod void test () {
        Sector__c s = Test_Util.createSector(null);
        insert s;
        
        List<Fund__c> fList = new List<Fund__c>();
        for (Integer i=0 ; i<20 ; i++){
        	Fund__c f = Test_Util.createFund(s.Id);
        	
        	fList.add(f);
        }
        insert fList;
        
        Contact c = new Contact(LastName='tc');
        insert c;
        
        Sector_Review_Plan__c srp = Test_Util.createSRP();
        insert srp;
        
        Included_Sector__c is = Test_Util.createIS(srp.Id, s.Id);
        insert is;
        
        Covered_Fund__c cf = Test_Util.createCF(srp.Id, fList[0].Id);
        insert cf;
        
        Covered_Fund__c cf2 = Test_Util.createCF(srp.Id, fList[1].Id);
        insert cf2;
        
        //User u = new User(LastName='r@test.com',Email='r@test.com');
        //insert u;
        
        Resource__c r = Test_Util.createResource(cf.Id,UserInfo.getUserId());
        insert r;
        
        
        test.startTest();
        PageReference pageRef1 = Page.SRP_CalculateRating;
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('id',srp.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(srp); 
        SRP_CalculateRatingController controller = new SRP_CalculateRatingController(ctr);
        controller.runCalculations();
       	test.stopTest();
	}
}