public class FileNote_CreateController {

	public String debugString {get;set;}
	public Boolean isError {get;set;}
	public Integer indexToRemove {get;set;}
	public List<attendeeWrapper> attendeeWrapperList {get;set;}
	public List<fileNoteWrapper> fileNoteWrapperList {get;set;}
	//public List<keypointWrapper> keypointWrapperList {get;set;}
	
	public fileNoteWrapper filenote {get;set;} // General filenote - applicable value for all file notes
	public fileNoteWrapper activeFileNote {get;set;}
	public Integer activeIndex {get;set;}

	//String state = 'initial';

	//constructor
	public FileNote_CreateController(){
		isError = false;
		debugString = '';
		activeIndex = 0;
		indexToRemove = 0;
		filenote = new fileNoteWrapper(new File_Note__c(),0,true,null);
		//activeFileNote = new fileNoteWrapper(new File_Note__c(),0,true,null);
		fileNoteWrapperList = new List<fileNoteWrapper>();
		fileNoteWrapperList.add(new fileNoteWrapper(new File_Note__c(),0,true,null));
		attendeeWrapperList = new List<attendeeWrapper>();
		attendeeWrapperList.add(new attendeeWrapper(new Attendee__c(Attendee_Type__c='Contact'),0));
		filenote.fileNote.File_Note_Author__c = UserInfo.getUserId();
		reloadActiveFileNote();
	}
	
	public Boolean isValidData(){
		//check if there is atleast 1 product strategy
		Boolean hasPS = false;
		//if (fileNoteWrapperList.size()>1 && filenote.fileNote.For_Out_of_Sector_Review__c){
		if (fileNoteWrapperList.size()>1 && filenote.fileNote.Meeting_Type__c=='Out of cycle review'){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Only one fund is allowed for out of cycle review'));
			isError = true;
			return false;
		}
		for (fileNoteWrapper fn : fileNoteWrapperList){
			if (fn.fileNote.Product_Strategy__c!=null){
				hasPS = true;
			}
		}
		
		//check if there is atleast 1 attendee
		Boolean hasA = false;
		for (attendeeWrapper aw : attendeeWrapperList){
			// check if attendee name is not null
			if (aw.attendee.Attendee_Name__c!=null || aw.attendee.Attendee_User__c!=null){
				hasA = true;
			}
		}
		
		if (!hasPS){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'There must be at least 1 Product/Strategy that is not blank.'));
			isError = true;
		}
		if (!hasA){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'There must be at least 1 Attendee that is not blank.'));
			isError = true;
		}
		
		//check if added keypoints are filled up completely
		for (fileNoteWrapper fn : fileNoteWrapperList){
			for(keypointWrapper kpw :fn.kpWrapperList){
				if (kpw.keypoint.Name!=null && kpw.keypoint.Name!='' && (kpw.keypoint.Other_Key_Point_Notes__c==null || kpw.keypoint.Other_Key_Point_Notes__c=='')){
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Value for additional keypoint must both be filled up. Name and Notes.'));
					activeIndex=fn.Index;
					isError = true;
					return false;
				}
				if ((kpw.keypoint.Name==null || kpw.keypoint.Name=='') && kpw.keypoint.Other_Key_Point_Notes__c!=null && kpw.keypoint.Other_Key_Point_Notes__c!=''){
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Value for additional keypoint must both be filled up. Name and Notes.'));
					activeIndex=fn.Index;
					isError = true;
					return false;
				}
			}
		}
		if (!hasPS || !hasA)	return false;
		return true;
	}
	
	public PageReference save(){
		
		
			if (!isValidData())	return null;
			
			Integer fnCount = 0;
			Integer aCount = 0;
			Integer kpCount = 0;
			
			// save individual filenotes
			List<File_Note__c> fnToSave = new List<File_Note__c>();
			for (fileNoteWrapper fn : fileNoteWrapperList){
				// check if there is a selected fund
				if (fn.fileNote.Product_Strategy__c!=null){
					// copy all field values displayed to each filenote to be saved
					fn.fileNote.Meeting_Date__c 						= filenote.fileNote.Meeting_Date__c;
					//fn.fileNote.Fund_Manager__c 						= filenote.fileNote.Fund_Manager__c;
					fn.fileNote.Attending__c 							= filenote.fileNote.Attending__c;
					fn.fileNote.Meeting_Type__c 						= filenote.fileNote.Meeting_Type__c;
					fn.fileNote.Location__c 							= filenote.fileNote.Location__c;
					fn.fileNote.Other_Location__c 						= filenote.fileNote.Other_Location__c;
					fn.fileNote.File_Note_Author__c 					= filenote.fileNote.File_Note_Author__c;
					fn.fileNote.Corporate__c 							= filenote.fileNote.Corporate__c;
					fn.fileNote.People__c 								= filenote.fileNote.People__c;
					fn.fileNote.Process__c 								= filenote.fileNote.Process__c;
					fn.fileNote.Portfolio_Performance_and_Outlook__c 	= filenote.fileNote.Portfolio_Performance_and_Outlook__c;
					fn.fileNote.Fund_View_Overall__c 					= filenote.fileNote.Fund_View_Overall__c;
					fn.fileNote.Weaknesses__c 							= filenote.fileNote.Weaknesses__c;
					fn.fileNote.Strengths__c 							= filenote.fileNote.Strengths__c;
					
					fnToSave.add(fn.filenote);
					fnCount++;
				}
			}
			if (fnToSave.size()>0) insert fnToSave;
			
			List<Resource__c> rList = new List<Resource__c>();
			
			// save attendees for each filenote
			List<Attendee__c> attendeeToSave = new List<Attendee__c>();
			for (fileNoteWrapper fn : fileNoteWrapperList){
				for (attendeeWrapper aw : attendeeWrapperList){
					// check if attendee name is not null
					if (aw.attendee.Attendee_Name__c!=null || aw.attendee.Attendee_User__c!=null){
						Attendee__c tempAtt = aw.attendee;
						
						//tempAtt.Attendee_Type__c = aw.attendee.Attendee_Type__c;
						debugString += fn.filenote.Id;
						
						
						if (aw.attendee.Attendee_Type__c == 'Contact')	tempAtt.Attendee_Name__c = aw.attendee.Attendee_Name__c;
						if (aw.attendee.Attendee_Type__c == 'User')		tempAtt.Attendee_User__c = aw.attendee.Attendee_User__c;
						attendeeToSave.add(new Attendee__c(Attendee_Type__c = aw.attendee.Attendee_Type__c, Attendee_Name__c = aw.attendee.Attendee_Name__c, Attendee_User__c = aw.attendee.Attendee_User__c, File_Note__c = fn.filenote.Id));
						tempAtt.File_Note__c = fn.filenote.Id;
						//attendeeToSave.add(tempAtt);
						aCount++;
						
						// check if this is for out of sector review, if true, create resources related to the fund from each attendee
						if (filenote.fileNote.Meeting_Type__c=='Out of cycle review' && aw.attendee.Attendee_Type__c == 'User'){
							rList.add(new Resource__c(Fund__c = fn.fileNote.Product_Strategy__c, Resource_Name__c = tempAtt.Attendee_User__c, Role__c = 'Attendee', SRP_Function__c='Meeting'));
						}
					}
				}
				if (filenote.fileNote.Meeting_Type__c=='Out of cycle review'){
					rList.add(new Resource__c(Fund__c = fn.fileNote.Product_Strategy__c, Resource_Name__c = fn.fileNote.File_Note_Author__c, Role__c = 'File Note Author', SRP_Function__c='Meeting'));
				}
				
			}
		try{	if (attendeeToSave.size()>0) upsert attendeeToSave;
				if (rList.size()>0)	upsert rList;
			
			// save attributes for each filenote Key_Point__c object
			List<Key_Point__c> kpToSave = new List<Key_Point__c>();
			List<Key_Point__c> kpToDelete = new List<Key_Point__c>();
			for (fileNoteWrapper fn : fileNoteWrapperList){
				for(keypointWrapper kpw :fn.kpWrapperList){
					if (kpw.keypoint.Name!=null && kpw.keypoint.Name!='' && kpw.keypoint.Other_Key_Point_Notes__c!=null && kpw.keypoint.Other_Key_Point_Notes__c!=''){
						Key_Point__c tempKP = kpw.keypoint;
						tempKP.File_Note__c = fn.filenote.id;
						//kpToSave.add(new Key_Point__c(Name = kpw.keypoint.Name, Other_Key_Point_Notes__c = kpw.keypoint.Other_Key_Point_Notes__c, File_Note__c = fn.filenote.id));
						kpToSave.add(tempKP);
						kpCount++;
					}else if (kpw.keypoint.Id!=null){
						kpToDelete.add(kpw.keypoint);
					}
				}
			}
			if (kpToDelete.size()>0) 	delete kpToDelete;
			if (kpToSave.size()>0) 		upsert kpToSave;
			
			activeIndex = 0;
			indexToRemove = 0;
			filenote = new fileNoteWrapper(new File_Note__c(),0,true,null);
			//activeFileNote = new fileNoteWrapper(new File_Note__c(),0,true,null);
			fileNoteWrapperList = new List<fileNoteWrapper>();
			fileNoteWrapperList.add(new fileNoteWrapper(new File_Note__c(),0,true,null));
			attendeeWrapperList = new List<attendeeWrapper>();
			attendeeWrapperList.add(new attendeeWrapper(new Attendee__c(Attendee_Type__c='Contact'),0));
			
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO,'The following have been successfully saved: '+ fnCount + ' File Notes, '+aCount+ ' Attendees, '+kpCount + ' Additional KeyPoints, '+rList.size()+' Resources.'));
			isError = false;
			return null;
		}catch(Exception e){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,debugString+'This is for testing and debugging only: '+attendeeToSave + '========='+fileNoteWrapperList+ e.getMessage()));
			isError = true;
			return null;
		}
	}
	
	public void reloadActiveFileNote(){
		activeIndex = (ApexPages.currentPage().getParameters().get('activeIndex')!=null) ? Integer.valueOf(ApexPages.currentPage().getParameters().get('activeIndex')) : activeIndex;
		
		for (fileNoteWrapper fn : fileNoteWrapperList){
			if (activeIndex == fn.index){
				activeFileNote = fn;
				fn.isSelected = true;
			}
		}
		debugString = String.valueOf('activeIndex after reload: '+activeIndex + ' || activeFileNote index: '+activeFileNote.index + ' || activeFileNote.kpWrapperList: '+activeFileNote.kpWrapperList.size());
		//return null;
	}
	
	public void addKeyPoint(){
		activeIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('activeIndex'));
		
		for (fileNoteWrapper fn : fileNoteWrapperList){
			if (activeIndex == fn.index){
				List<keyPointWrapper> tempList = fn.kpWrapperList;
				if (tempList.size()>0)
					tempList.add(new keyPointWrapper(new Key_Point__c(),tempList[tempList.size()-1].index+1));
				else
					tempList.add(new keyPointWrapper(new Key_Point__c(),0));
				
				//tempList.add(new Key_Point__c());
				fn.kpWrapperList = tempList;
				debugString += String.valueOf('|| FOUND kpWrapperList.size: '+fn.kpWrapperList.size() + '  ||  index: '+fn.index);
			}
		}
		reloadActiveFileNote();
		//return null;
	}
	
	public PageReference removeKeyPoint(){
		indexToRemove = Integer.valueOf(ApexPages.currentPage().getParameters().get('indexToRemove'));
		//activeIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('activeIndex'));
		
		for (fileNoteWrapper fn : fileNoteWrapperList){
			if (activeIndex == fn.index){
				fn.kpWrapperList.remove(indexToRemove);
				//fn.kpWrapperList = tempList;
				//activeFileNote = fn;
				debugString += String.valueOf('|| FOUND kpWrapperList.size: '+fn.kpWrapperList.size() + '  ||  index: '+fn.index);
			}
		}
		return null;
	}
	
	// adds an attendee row to the page
	public PageReference addAttendee(){
		if (attendeeWrapperList.size()>0)
			attendeeWrapperList.add(new attendeeWrapper(new Attendee__c(Attendee_Type__c='Contact'),attendeeWrapperList[attendeeWrapperList.size()-1].index+1));
		else
			attendeeWrapperList.add(new attendeeWrapper(new Attendee__c(Attendee_Type__c='Contact'),0));
		return null;
	}
	
	// removes an attendee row from the page
	/*public PageReference removeAttendee(){
		indexToRemove = Integer.valueOf(ApexPages.currentPage().getParameters().get('indexToRemove'));
		//if (indexToRemove==0) return null;
		//attendeeWrapperList.remove(indexToRemove);
		List<attendeeWrapper> tempList = new List<attendeeWrapper>();
		//Integer ctr = 0;
		for (attendeeWrapper a : attendeeWrapperList){
			if (a.index == indexToRemove) System.debug('removed '+a.index);
			else		tempList.add(new attendeeWrapper(a.attendee,a.index));
			//tempList.add(new attendeeWrapper(a.attendee,ctr));
			//ctr++;
		}
		attendeeWrapperList = tempList;
		return null;
	}*/
	
	// adds a product/strategy row to the page
	public void addFund(){
		
		
		if (fileNoteWrapperList.size()>0){
			if (filenote.fileNote.Meeting_Type__c==null || filenote.fileNote.Meeting_Type__c==''){
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a Meeting Type first.'));
				isError = true;
				return;
			}
				
			if (fileNoteWrapperList.size()==1 && filenote.fileNote.Meeting_Type__c=='Out of cycle review'){
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Only one fund is allowed for out of cycle review'));
				isError = true;
				return;
			}
			fileNoteWrapperList.add(new fileNoteWrapper(new File_Note__c(),fileNoteWrapperList[fileNoteWrapperList.size()-1].index+1, false, null));
		}else
			fileNoteWrapperList.add(new fileNoteWrapper(new File_Note__c(),0, false, null));
		//return null;
	}
	
	// removes a product/strategy row from the page
	/*public PageReference removeFund(){
		indexToRemove = Integer.valueOf(ApexPages.currentPage().getParameters().get('indexToRemove'));
		if (indexToRemove==activeIndex) activeIndex = 0;
		if (fileNoteWrapperList.size()==1){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'There must be at least 1 Product/Strategy.'));
			return null;
		}

		fileNoteWrapperList.remove(indexToRemove);
		List<fileNoteWrapper> tempList = new List<fileNoteWrapper>();
		Integer ctr = 0;
		for (fileNoteWrapper fw : fileNoteWrapperList){
			tempList.add(new fileNoteWrapper(fw.fileNote,ctr,fw.isSelected,fw.kpWrapperList));
			ctr++;
		}
		fileNoteWrapperList = tempList;
		if (indexToRemove == activeIndex) activeIndex = 0;
		return null;
	}*/
	
	// used to wrap File_Note__c
	public class fileNoteWrapper{
		
		public Integer index {get;set;}
		public Boolean isSelected {get;set;}
		public File_Note__c fileNote {get;set;}
		public List<keyPointWrapper> kpWrapperList {get;set;}
		public fileNoteWrapper(File_Note__c pFileNote, Integer pIndex, Boolean pIsSelected, List<keyPointWrapper> pKpList){
			index 			= pIndex;
			fileNote 		= pFileNote;
			isSelected 		= pIsSelected;
			List<keyPointWrapper> tempList = new List<keyPointWrapper>();
			tempList.add(new keyPointWrapper(new Key_Point__c(),0));
			kpWrapperList		= (pKpList!=null) ? pKpList : tempList;
		}
	}
	
	// used to wrap Attendee__c
	public class attendeeWrapper{
		
		public Integer index {get;set;}
		public Attendee__c attendee {get;set;}
		public attendeeWrapper(Attendee__c pAttendee, Integer pIndex){
			index 		= pIndex;
			attendee 	= pAttendee;
		}
	}
	
	// used to wrap Key_Point__c
	public class keyPointWrapper{
		
		public Integer index {get;set;}
		public Key_Point__c keypoint {get;set;}
		public keyPointWrapper(Key_Point__c pKeypoint, Integer pIndex){
			index 		= pIndex;
			keypoint 	= pKeypoint;
		}
	}
	
	public PageReference dummyMethod(){
		return null;
	}
	
	static testMethod void test () {
        // Instantiate a new controller with all parameters in the page
        
        Sector__c s = Test_Util.createSector(null);
        insert s;
        
        Fund__c f = Test_Util.createFund(s.Id);
        insert f;
        
        Contact c = new Contact(LastName='tc');
        insert c;
        
        PageReference pageRef1 = Page.FileNote_Create;
        Test.setCurrentPage(pageRef1);
        FileNote_CreateController controller = new FileNote_CreateController ();
        ApexPages.currentPage().getParameters().put('activeIndex','0');
        controller.addKeyPoint();
        controller.addFund();
        controller.addAttendee();
        controller.dummyMethod();
        
        controller.activeIndex = 0;
        controller.indexToRemove = 1;
        
        ApexPages.currentPage().getParameters().put('indexToRemove','0');
        controller.removeKeyPoint();
        controller.save();
        controller.addKeyPoint();
        controller.addFund();
        controller.addAttendee();
        for (attendeeWrapper aw : controller.attendeeWrapperList){
  			aw.attendee.Attendee_Name__c = c.Id;
  		}
        
        for (fileNoteWrapper fn : controller.fileNoteWrapperList){
        	fn.isSelected = true;
        	fn.filenote.Product_Strategy__c = f.Id;
        	for(keypointWrapper kpw :fn.kpWrapperList){
        		kpw.keypoint.Name = 'a';
        	}
        }
  		controller.save();
  		
  		for (fileNoteWrapper fn : controller.fileNoteWrapperList){
        	for(keypointWrapper kpw :fn.kpWrapperList){
        		kpw.keypoint.Name = '';
        		kpw.keypoint.Other_Key_Point_Notes__c = 'a';
        	}
        }
        controller.save();
        
        for (fileNoteWrapper fn : controller.fileNoteWrapperList){
        	for(keypointWrapper kpw :fn.kpWrapperList){
        		kpw.keypoint.Name = 'a';
        		kpw.keypoint.Other_Key_Point_Notes__c = 'a';
        	}
        }
        
        controller.addKeyPoint();
        controller.addFund();
        controller.addAttendee();
        
  		controller.save();

    }    

}