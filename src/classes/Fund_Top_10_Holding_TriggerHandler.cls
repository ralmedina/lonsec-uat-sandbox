public class Fund_Top_10_Holding_TriggerHandler {
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	public Fund_Top_10_Holding_TriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	private void deactivateOldHoldings(Fund_Top_10_Holding__c[] pList){
		
		Set<Id> FundIdSet = new Set<Id>();
		for (Fund_Top_10_Holding__c t : pList){
			if (t.Main_Fund__c!=null){
				FundIdSet.add(t.Main_Fund__c);
			}
		}
		
		List<Fund_Top_10_Holding__c> fthList = [select Id, Fund__c
										from Fund_Top_10_Holding__c 
										where Main_Fund__c IN :FundIdSet];
		
		
		for (Fund_Top_10_Holding__c oldFTH : fthList){
			oldFTH.Active__c = false;
		}
		
		if (fthList.size()>0)	update fthList;
		
	}
	
	public void OnBeforeInsert(Fund_Top_10_Holding__c[] pList){
		deactivateOldHoldings(pList);
		//checkAssetAllocationWeightForEachFundProfile(pAAList);
	}
}