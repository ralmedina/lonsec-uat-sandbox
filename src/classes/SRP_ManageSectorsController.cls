public with sharing class SRP_ManageSectorsController {

	public String srpID {get;set;}
	public String searchStr {get;set;}
	public Sector_Review_Plan__c srp {get;set;}
	public List<sectorWrapper> sectorWrapperList {get;set;}
	Map<Id, Included_Sector__c> sectorISMap = new Map<Id, Included_Sector__c>();
	
	public class sectorWrapper{
		public Integer rowNum {get;set;}
		public String sectorName {get;set;}
		public Sector__c sector {get;set;}
		public Included_Sector__c is {get;set;}
		public Boolean isSelected {get;set;}
		
		public sectorWrapper(Sector__c pSector, Included_Sector__c pIS, Boolean pisSelected, String psrpId, Integer pRowNum){
			sectorName 							= pSector.Name;//pSector.Sector__r.Name + ' -> ' + pSector.Name;
			sector 								= pSector;
			isSelected 							= pisSelected;
			is	 								= pIS;
			is.Sector__c 						= (sector.Id!=null) ? sector.Id : null;
			is.Sector_Review_Plan_Record__c 	= psrpId;
			rowNum = pRowNum;
		}
	}
	
	public void addSectorRow(){
		/*string myParam = apexpages.currentpage().getparameters().get('sId');
		for (fundWrapper fw : fundList){
			if (fw.fund.Id == myParam){
				fw.cfResourceList.add(new Resource__c());
			}
		}*/
	}
	
	// Constructor
	public SRP_ManageSectorsController(ApexPages.StandardController controller){
		srp = (Sector_Review_Plan__c)controller.getRecord();
		srpID = ApexPages.currentPage().getParameters().get('id');
		searchStr = ''; 
		sectorWrapperList = new List<sectorWrapper>();
		getSectorList('initial');
	}
	
	public List<sectorWrapper> getSectorList(String stage){
		sectorISMap.clear();
		// get all existing sector records under the sector review plan
		for (Included_Sector__c tIS : [select Id, Sector__c  from Included_Sector__c 
										where Sector_Review_Plan_Record__c = :srpID ]){
			sectorISMap.put(tIS.Sector__c, tIS);
		}
		
		String qryStr = 'select Id, Name, Sector__r.Name from Sector__c';
		String condition = '';
		
		sectorWrapperList.clear();
		
		condition = ' where Sector__c!=null AND Name like \'%' + searchStr + '%\'';
		
		Integer ctr = 1;
		for (Sector__c s : database.query(qryStr+condition)){
			if (sectorISMap.containsKey(s.Id))		sectorWrapperList.add(new sectorWrapper(s,sectorISMap.get(s.Id),true,srpID,ctr));
			else if (stage!='initial')				sectorWrapperList.add(new sectorWrapper(s,new Included_Sector__c(),false,srpID,ctr));
			ctr++;
		}
		
		return sectorWrapperList;
	}
	
	public void goSearch(){
		
		if (validate()) return;
		getSectorList('');
	}
	
	public PageReference updateSRPSectors(){
	
		List<Included_Sector__c> isToInsert = new List<Included_Sector__c>();
		List<Included_Sector__c> isToDelete = new List<Included_Sector__c>();
		//List<Sector__c> sectorssToUpdate 		= new List<Sector__c>();
		
		for (sectorWrapper sw : sectorWrapperList){
			
			if (sw.isSelected){
				isToInsert.add(sw.is);
			}else{
				if (sw.sector.Id!=null)
					if (sectorISMap.containsKey(sw.sector.Id))	
						isToDelete.add(sw.is);
			}
		}
		
		if (isToDelete.size()>0)	delete isToDelete;
		if (isToInsert.size()>0)	upsert isToInsert;

		return new PageReference('/'+srpId);
		// insert logic that adds (converts funds to covered funds) and removes covered funds from the SRP record
	}
	
	public String classInputPostalCode {get;set;}
	public String errorMessage {get;set;}
	
	public Boolean validate() {
		if (searchStr==null || searchStr=='' || searchStr.length()<3){
        	classInputPostalCode = 'error';  // put the errorclass, red borders
        	errorMessage = 'Please input atleast 3 characters';
        	return true;
        } else {
        	classInputPostalCode = '';
        	errorMessage = '';
        	return false;
		}
	}
}