/*Description: This is class is written to 
                1)To update the parent(SRP) field with related child(SRF) count.
                2)Then on the SRF's related list of SRP's calculated Qualitative Rank and Quantitative Rank.
                3)Next on teh update of SRF to any of it's related SRP ,collected all the SRF's with respect to it's SRP
                  and compared it's style value and if found match then inserted new record in newly created object Related_Style__c
@author      :Rimali Gurav  
@date        :29th May 2013
 */

public  class Covered_Fund_Trigger_Class {
    
    public static void callTrigger(set<id> SRPId){
    
        list<Sector_Review_Plan__c>lstSRP=new list<Sector_Review_Plan__c>();
        
        for(Sector_Review_Plan__c objSRP:[SELECT Id,
                                                 Funds_Reviewed__c,
                                                 (SELECT Id 
                                                  FROM Covered_Funds__r) 
                                          FROM Sector_Review_Plan__c
                                          WHERE Id IN : SRPId]){
                                                
            objSRP.Funds_Reviewed__c = objSRP.Covered_Funds__r.size();
            
            system.debug('******objSRP.Funds_Reviewed__c******'+objSRP.Funds_Reviewed__c);
            
            lstSRP.add(objSRP);
        
        }
        if(!lstSRP.isEmpty()){
        
            update lstSRP; 
        } 
    }
    
    
    //This method is used to calculate the Quant_Rank__c and Qual_Rank__c 
    public static void CallQualityRank(set<id> SRPID){

        /************Declaration**********/
        
        //This Map is used to holds the list Sector Review Funds records against its related Sector Review Plan
        map<Id,List<Covered_Fund__c>> mapSRPwithSRF = new map<Id,List<Covered_Fund__c>>();
        
        //This Map is used to holds the list Sector Review Funds records against its related Total_Qualitative_Score__c
        map<decimal,List<Covered_Fund__c>> mapQualityRank = new map<decimal,List<Covered_Fund__c>>();
        
        //This Map is used to holds the list Sector Review Funds records against its related Total_Quantitative_Score__c
        map<decimal,List<Covered_Fund__c>> mapQuantityRank = new map<decimal,List<Covered_Fund__c>>();
        
        //This list is used to holds the Sector Review Funds records for sync purpose
        list<Covered_Fund__c>lstcoverFund = new list<Covered_Fund__c>();
        
        //This list is used to holds the Sector Review Funds records for updation.
        list<Covered_Fund__c> updatelstcoverFund = new list<Covered_Fund__c>();
        
        //This list is used to holds the value of Total_Qualitative_Score__c and Total_Quantitative_Score__c alternately
        list<decimal> lstwithKey = new list<decimal>();
        
        //This Map is used to holds the Sector Review Funds records against its Id.
        map<Id, Covered_Fund__c> IdwithCF = new map<Id, Covered_Fund__c> ();

        //Iterating on Sector Review Funds Records for getting the Sector Review Funds Records information and its Sector Review Plan Id
        for(Covered_Fund__c objCF:[SELECT Total_Quantitative_Score__c, 
                                          Total_Qualitative_Score__c,
                                          Sector_Review_Plan_Record__r.Id,
                                          Quant_Rank__c,
                                          Qual_Rank__c, 
                                          Id 
                                  FROM Covered_Fund__c
                                  WHERE Sector_Review_Plan_Record__r.Id IN : SRPID]){


            //Filling the map as one parent(Sector Review Plan Id) and its multiple childs(Sector Review Funds Records)
            if(mapSRPwithSRF.containsKey(objCF.Sector_Review_Plan_Record__r.Id)){
                
                //If key found then add the record against it.
                mapSRPwithSRF.get(objCF.Sector_Review_Plan_Record__r.Id).add(objCF);
            
            }else{
                
                //If key does  not exists then create new list against it and add record to it.
                mapSRPwithSRF.put(objCF.Sector_Review_Plan_Record__r.Id,new List<Covered_Fund__c>{objCF});
            
            }
        }

        //Actual logic for calculating the Ranks
        //Iterating on the Sector Review Plan Ids
        for(Id i : mapSRPwithSRF.keyset()){

            //Getting the total Rank by getting the associated Child size of Sector Review Plan Ids
            integer Rank = mapSRPwithSRF.get(i).size();
            
            //Iterating on the Child(Sector Review Funds) records
            for(Covered_Fund__c c : mapSRPwithSRF.get(i)){
                
                //Filling the map as one Total_Qualitative_Score__c and its multiple Sector Review Funds Records
                if(mapQualityRank.containsKey(c.Total_Qualitative_Score__c)){
                
                    mapQualityRank.get(c.Total_Qualitative_Score__c).add(c);
                
                }else{
                
                    mapQualityRank.put(c.Total_Qualitative_Score__c,new List<Covered_Fund__c>{c});
                
                }
                
                //Filling the map as one Total_Quantitative_Score__c and its multiple Sector Review Funds Records
                if(mapQuantityRank.containsKey(c.Total_Quantitative_Score__c)){
                
                    mapQuantityRank.get(c.Total_Quantitative_Score__c).add(c);
                
                }else{
                
                    mapQuantityRank.put(c.Total_Quantitative_Score__c,new List<Covered_Fund__c>{c});
                
                }
            }
            
            //Adding the Total_Qualitative_Score__c to lstwithKey list for sorting.
            lstwithKey.addall(mapQualityRank.keyset());
            
            //Sorting the list
            lstwithKey.sort();
            
            //this variable is used to give the rank to highest Total_Qualitative_Score__c as 1.
            integer Qual_Rank= 1;
            
            //Iterating on the lstwithKey list as highest value first.
            for(integer p = lstwithKey.size()-1; p>=0;p-- ){
                
                //Iterating on the Sector Review Funds Records having the p as Total_Qualitative_Score__c
                for(Covered_Fund__c c : mapQualityRank.get(lstwithKey[p])){
                    
                    //Assigning the Rank.
                    c.Qual_Rank__c = Qual_Rank+ '/' +Rank;
                    
                    //Adding the Sector Review Funds Records in list for next calculation.
                    lstcoverFund.add(c); 
                }
                
                //Incrementing the rank.
                Qual_Rank++;

            }
            
            //getting the Sector Review Funds Records with updated Qual_Rank__c in the form of map for Quant_Rank__c calculation
            for(Covered_Fund__c c : lstcoverFund){
                
                IdwithCF.put(c.Id, c);
            }
            
            //clearing the list for reuse.
            lstcoverFund.clear();
            lstwithKey.clear();
            
            //Adding the Total_Quantitative_Score__c to lstwithKey list for sorting.
            lstwithKey.addall(mapQuantityRank.keyset());
            
            //Sorting the list
            lstwithKey.sort();
            
            //this variable is used to give the rank to highest Total_Quantitative_Score__c as 1.
            integer Quant_Rank= 1;
            
            //Iterating on the lstwithKey list as highest value first.
            for(integer p = lstwithKey.size()-1; p>=0;p-- ){
                
                //Iterating on the Sector Review Funds Records having the p as Total_Quantitative_Score__c
                for(Covered_Fund__c c : mapQuantityRank.get(lstwithKey[p])){
                    
                    //Assigning the Rank.
                    c.Quant_Rank__c = Quant_Rank + '/' +Rank; 
                    
                    //Adding the Sector Review Funds Records in list for next calculation.
                    lstcoverFund.add(c); 
                }
                
                //Incrementing the rank.
                Quant_Rank++;
                
            }
            
            //collecting the Sector Review Funds Records information in single list.
            for(Covered_Fund__c c : lstcoverFund){
                
                c.Qual_Rank__c= IdwithCF.get(c.Id).Qual_Rank__c;
                updatelstcoverFund.add(c);
            }
            
            //updating the Sector Review Funds Records.
            if(!updatelstcoverFund.isEmpty()){
                update updatelstcoverFund; 
            }
        }
    } 
  
      
    public static void checkStyle(set<Id> SRPId, list<Covered_Fund__c> lstcovefund){
        
        //This Map is used to holds the list Sector Review Funds records against its related Sector Review Plan
        map<Id,List<Covered_Fund__c>> mapSRPwithSRF = new map<Id,List<Covered_Fund__c>>();
        
        //This list is used to insert the new records in Related_Style__c 
        list<Related_Style__c > lstRelatedStyle = new list<Related_Style__c >();
        
        map<Id,List<Covered_Fund__c>> mapSRPwithSRFExisting = new map<Id,List<Covered_Fund__c>>();
        
        for(Covered_Fund__c objCF:[SELECT Total_Quantitative_Score__c, 
                                          Total_Qualitative_Score__c,
                                          Sector_Review_Plan_Record__r.Id,
                                          Quant_Rank__c,
                                          Qual_Rank__c, 
                                          Id,
                                          Style_manager__c    
                                  FROM Covered_Fund__c
                                  WHERE Sector_Review_Plan_Record__r.Id IN : SRPID]){


            //Filling the map as one parent(Sector Review Plan Id) and its multiple childs(Sector Review Funds Records)
            if(mapSRPwithSRFExisting.containsKey(objCF.Sector_Review_Plan_Record__r.Id)){
                
                //If key found then add the record against it.
                mapSRPwithSRFExisting.get(objCF.Sector_Review_Plan_Record__r.Id).add(objCF);
            
            }else{
                
                //If key does  not exists then create new list against it and add record to it.
                mapSRPwithSRFExisting.put(objCF.Sector_Review_Plan_Record__r.Id,new List<Covered_Fund__c>{objCF});
            
            }
        }
        
        //Iterating on Sector Review Funds Records for getting the Sector Review Funds Records information and its Sector Review Plan Id
        for(Covered_Fund__c objCF: lstcovefund){

            //Filling the map as one parent(Sector Review Plan Id) and its multiple childs(Sector Review Funds Records)
            
            if(objCF.Style_manager__c == 'Active' || objCF.Style_manager__c == 'Enhanced Passive'){
                if(mapSRPwithSRF.containsKey(objCF.Sector_Review_Plan_Record__r.Id)){
                    
                    //If key found then add the record against it.
                    mapSRPwithSRF.get(objCF.Sector_Review_Plan_Record__r.Id).add(objCF);
                
                }else{
                    
                    //If key does  not exists then create new list against it and add record to it.
                    mapSRPwithSRF.put(objCF.Sector_Review_Plan_Record__r.Id,new List<Covered_Fund__c>{objCF});
                
                }
            }
        }
        
        //Iterating on the Sector Review Plan Ids
        for(Id i : mapSRPwithSRF.keyset()){

            //Getting the total Rank by getting the associated Child size of Sector Review Plan Ids
            integer Rank = mapSRPwithSRF.get(i).size();
            
            //Iterating on the Child(Sector Review Funds) records
            for(Covered_Fund__c c : mapSRPwithSRF.get(i)){
            
                Related_Style__c objRelatedStyle = new Related_Style__c();
                    objRelatedStyle.Sector_Review_Fund__c = c.id  ;
                    
                    
                    for(Covered_Fund__c CFExisting : mapSRPwithSRFExisting.get(c.Sector_Review_Plan_Record__c)){
                        if(c.Id != CFExisting.Id && c.Style_manager__c == CFExisting.Style_manager__c){
                            objRelatedStyle.Related_Sector_Review_Fund__c = CFExisting.id;
                        }
                    }
                lstRelatedStyle.add(objRelatedStyle);
            
            }
        }
        
        if(!lstRelatedStyle.isEmpty()){
                system.debug('***********lstRelatedStyle*****************'+lstRelatedStyle);
                insert lstRelatedStyle;
                system.debug('***********lstRelatedStyle*****************'+lstRelatedStyle);
            }  
    }
    
    public static void checkStyleUpdate(set<Id> SRPId, list<Covered_Fund__c> lstcovefund, map<Id, Covered_Fund__c> oldMapCF){
        //This Map is used to holds the list Sector Review Funds records against its related Sector Review Plan
        map<Id,List<Covered_Fund__c>> mapSRPwithSRF = new map<Id,List<Covered_Fund__c>>();
        
        //This list is used to update the new records in Related_Style__c 
        list<Related_Style__c > lstRelatedStyle = new list<Related_Style__c >();
        
        //This list is used to delete the new records in Related_Style__c 
        list<Related_Style__c > lstRelatedStyleDelete= new list<Related_Style__c >();
        
        
        map<Id,List<Covered_Fund__c>> mapSRPwithSRFExisting = new map<Id,List<Covered_Fund__c>>();
        
        for(Covered_Fund__c objCF:[SELECT Total_Quantitative_Score__c, 
                                          Total_Qualitative_Score__c,
                                          Sector_Review_Plan_Record__r.Id,
                                          Quant_Rank__c,
                                          Qual_Rank__c, 
                                          Id,
                                          Style_manager__c    
                                  FROM Covered_Fund__c
                                  WHERE Sector_Review_Plan_Record__r.Id IN : SRPID]){


            //Filling the map as one parent(Sector Review Plan Id) and its multiple childs(Sector Review Funds Records)
            if(mapSRPwithSRFExisting.containsKey(objCF.Sector_Review_Plan_Record__r.Id)){
                
                //If key found then add the record against it.
                mapSRPwithSRFExisting.get(objCF.Sector_Review_Plan_Record__r.Id).add(objCF);
            
            }else{
                
                //If key does  not exists then create new list against it and add record to it.
                mapSRPwithSRFExisting.put(objCF.Sector_Review_Plan_Record__r.Id,new List<Covered_Fund__c>{objCF});
            
            }
        }
        
        //Iterating on Sector Review Funds Records for getting the Sector Review Funds Records information and its Sector Review Plan Id
        for(Covered_Fund__c objCF: [SELECT Id, Sector_Review_Plan_Record__c, Style_manager__c,
                                        (SELECT Id, name FROM Related_Styles__r) FROM Covered_Fund__c Where Id In :lstcovefund]){

            //Filling the map as one parent(Sector Review Plan Id) and its multiple childs(Sector Review Funds Records)
            
            if(oldMapCF.get(objCF.Id).Style_manager__c != objCF.Style_manager__c && (objCF.Style_manager__c == 'Active' || objCF.Style_manager__c == 'Enhanced Passive')){
                for(Related_Style__c RS : objCF.Related_Styles__r){
                    for(Covered_Fund__c CFExisting : mapSRPwithSRFExisting.get(objCF.Sector_Review_Plan_Record__c)){
                        if(objCF.Id != CFExisting.Id && objCF.Style_manager__c == CFExisting.Style_manager__c){
                            RS.Related_Sector_Review_Fund__c = CFExisting.id;
                        }
                    }
                     lstRelatedStyle.add(RS);
                }
                
            }
            
            if(oldMapCF.get(objCF.Id).Style_manager__c != objCF.Style_manager__c && objCF.Style_manager__c == 'Multi-manager' ){
                for(Related_Style__c RS : objCF.Related_Styles__r){
                    lstRelatedStyleDelete.add(RS);
                }
            }
        }
        
        
        if(!lstRelatedStyle.isEmpty()){
                system.debug('***********lstRelatedStyle*****************'+lstRelatedStyle);
                update lstRelatedStyle;
                system.debug('***********lstRelatedStyle*****************'+lstRelatedStyle);
            }  
        if(!lstRelatedStyleDelete.isEmpty()){
                system.debug('***********lstRelatedStyleDelete*****************'+lstRelatedStyleDelete);
                delete lstRelatedStyleDelete;
                system.debug('***********lstRelatedStyleDelete*****************'+lstRelatedStyleDelete);
            }  
            
        
    }
    
    public static void checkStyledelete(list<Covered_Fund__c> lstcovefund){
        
        //This list is used to delete the new records in Related_Style__c 
        list<Related_Style__c > lstRelatedStyleDelete= new list<Related_Style__c >();
        
        for(Covered_Fund__c objCF: [SELECT Id, Sector_Review_Plan_Record__c, Style_manager__c,
                                        (SELECT Id, name FROM Related_Styles__r) FROM Covered_Fund__c Where Id In :lstcovefund]){
              
              system.debug('******indelete1*****');
              for (Related_Style__c rs : objCF.Related_Styles__r)  {                        
                  lstRelatedStyleDelete.add(rs);    
                  system.debug('******indelete12*****');                  
              }
        }
        
        if(!lstRelatedStyleDelete.isEmpty()){
                system.debug('***********lstRelatedStyleDelete*****************'+lstRelatedStyleDelete);
                delete lstRelatedStyleDelete;
                system.debug('***********lstRelatedStyleDelete*****************'+lstRelatedStyleDelete);
            }  
    }
    
    public static void checkStyleManager(List<Covered_Fund__c> cList){
        Set<Id> cfIds = new Set<Id>();
        Set<Id> cfSRP = new Set<Id>();
        List<Related_Style__c> r = new List<Related_Style__c>();
        Map<Id,List<Related_Style__c>> cfWithrsMap = new Map<Id,List<Related_Style__c>>();
        
        Set<Id> rsIds = new Set<Id>();
        
        for(Covered_Fund__c cf: cList){
            cfIds.add(cf.Id);
            cfSRP.add(cf.Sector_Review_Plan_Record__c);
        }
        
        
        List<Related_Style__c> rs= [select Id, Related_Sector_Review_Fund__c, Sector_Review_Fund__c, Related_Sector_Review_Fund__r.Sector_Review_Plan_Record__c from Related_Style__c where Related_Sector_Review_Fund__c IN :cfIds or Related_Sector_Review_Fund__r.Sector_Review_Plan_Record__c IN :cfSRP];
        
        
        if(!rs.isEmpty()) delete rs;
        
        Map<String,List<Covered_Fund__c>> cfMap = new Map<String,List<Covered_Fund__c>>();
        
        List<Covered_Fund__c> cfList = [select Id, Sector_Review_Plan_Record__c, (select id from Related_Styles__r) from Covered_Fund__c where Sector_Review_Plan_Record__c IN :cfSRP and (Style_manager__c = 'Active' or Style_manager__c = 'Enhanced Passive')];
        
        
            for(Covered_Fund__c cf: cfList){
                if(cfMap.containsKey(cf.Sector_Review_Plan_Record__c)){
                    cfMap.get(cf.Sector_Review_Plan_Record__c).add(cf);
                }else{
                    cfMap.put(cf.Sector_Review_Plan_Record__c,new List<Covered_Fund__c>{cf});
                }
            }
            
            
            List<Related_Style__c> rsToInsert = new List<Related_Style__c>();
            for(String keySet: cfMap.keySet()){
                for(Covered_Fund__c cf: cfMap.get(keySet)){
                    for(Covered_Fund__c cf2: cfMap.get(keySet)){
                        Related_Style__c tempRS = new Related_Style__c();
                        tempRS.Related_Sector_Review_Fund__c = cf2.Id;
                        tempRS.Sector_Review_Fund__c = cf.Id;
                        rsToInsert.add(tempRS);
                    }
                }
            }
            system.debug('!!!rsToInsert' + rsToInsert);
            insert rsToInsert;
    }
    
    public static void deleteRelatedRating(list<Covered_Fund__c> lstcovefund){
        
        //This list is used to delete the new records in Related_Style__c 
        list<Related_Rating__c > lstRelatedRatingDelete= new list<Related_Rating__c >();
        
        for(Covered_Fund__c objCF: [SELECT Id, Sector_Review_Plan_Record__c, Style_manager__c,
                                        (SELECT Id, name FROM Related_Rating__r) FROM Covered_Fund__c Where Id In :lstcovefund]){
              
              system.debug('******indelete1*****');
              for (Related_Rating__c rr : objCF.Related_Rating__r)  {                        
                  lstRelatedRatingDelete.add(rr);    
                  system.debug('******indelete12*****');                  
              }
        }
        
        if(!lstRelatedRatingDelete.isEmpty()){
                system.debug('***********lstRelatedStyleDelete*****************'+lstRelatedRatingDelete);
                delete lstRelatedRatingDelete;
                system.debug('***********lstRelatedStyleDelete*****************'+lstRelatedRatingDelete);
            }  
    }
    
    public static void createRelatedRating(List<Covered_Fund__c> cList){
        Set<Id> cfIds = new Set<Id>();
        Set<Id> cfSRP = new Set<Id>();
        List<Related_Rating__c> r = new List<Related_Rating__c>();
        
        Set<Id> rsIds = new Set<Id>();
        
        for(Covered_Fund__c cf: cList){
            cfIds.add(cf.Id);
            cfSRP.add(cf.Sector_Review_Plan_Record__c);
        }
        
        
        List<Related_Rating__c> rr= [select Id, Related_Sector_Review_Funds__c, Sector_Review_Funds__c, Related_Sector_Review_Funds__r.Sector_Review_Plan_Record__c from Related_Rating__c 
        							where Related_Sector_Review_Funds__c IN :cfIds or Related_Sector_Review_Funds__r.Sector_Review_Plan_Record__c IN :cfSRP];
        
        
        if(!rr.isEmpty()) delete rr;
        
        Map<String,List<Covered_Fund__c>> srpIdCFMap = new Map<String,List<Covered_Fund__c>>();
        
        List<Covered_Fund__c> cfList = [select Id, Sector_Review_Plan_Record__c, Existing_Rating__c from Covered_Fund__c 
        								where Sector_Review_Plan_Record__c IN :cfSRP AND Existing_Rating__c != null];
        
        Map<String, Map<String,List<Covered_Fund__c>>> srpIdRatingCFListMap = new Map<String, Map<String,List<Covered_Fund__c>>>();
        
        for(Covered_Fund__c cf: cfList){
        
        	// check if there is the map already has the current srp
        	if (srpIdRatingCFListMap.containsKey(cf.Sector_Review_Plan_Record__c)){
        		// check if the srp rating map already have the current rating
        		if ( srpIdRatingCFListMap.get(cf.Sector_Review_Plan_Record__c).containsKey(cf.Existing_Rating__c) ){
        			srpIdRatingCFListMap.get(cf.Sector_Review_Plan_Record__c).get(cf.Existing_Rating__c).add(cf);
        		}else{
        			srpIdRatingCFListMap.get(cf.Sector_Review_Plan_Record__c).put(cf.Existing_Rating__c, new List<Covered_Fund__c>{cf});
        		}
        	}else{
        		Map<String, List<Covered_Fund__c>> tempRatingCFListMap = new Map<String, List<Covered_Fund__c>>();
        		tempRatingCFListMap.put(cf.Existing_Rating__c, new List<Covered_Fund__c>{cf});
        		srpIdRatingCFListMap.put(cf.Sector_Review_Plan_Record__c, tempRatingCFListMap);
        	}
        
        	//Map<String, List<Covered_Fund__c>> ratingCFListMap = new Map<String, List<Covered_Fund__c>>();
        
            /*if(srpIdCFMap.containsKey(cf.Sector_Review_Plan_Record__c)){
                srpIdCFMap.get(cf.Sector_Review_Plan_Record__c).add(cf);
            }else{
                srpIdCFMap.put(cf.Sector_Review_Plan_Record__c,new List<Covered_Fund__c>{cf});
            }*/
        }
            
        List<Related_Rating__c> rrToInsert = new List<Related_Rating__c>();
        for (String srpId : srpIdRatingCFListMap.keySet()){
        	for (String rating : srpIdRatingCFListMap.get(srpId).keySet()){
        		// loop each fund grouped per rating
        		for (Covered_Fund__c tempCF : srpIdRatingCFListMap.get(srpId).get(rating)){
        			
        			// loop each fund grouped per rating to determine the related rating records to be created, exclude the current fund from it which is the tempCF
        			for (Covered_Fund__c tempCF2 : srpIdRatingCFListMap.get(srpId).get(rating)){
        				if (tempCF.Id!=tempCF2.Id){
        					Related_Rating__c tempRR = new Related_Rating__c();
	                        tempRR.Related_Sector_Review_Funds__c = tempCF2.Id;
	                        tempRR.Sector_Review_Funds__c = tempCF.Id;
	                        rrToInsert.add(tempRR);
        				}
        			}
        		}
        	}
        }
        
        system.debug('!!!rrToInsert' + rrToInsert);
        insert rrToInsert;
    }
    
    
    
}