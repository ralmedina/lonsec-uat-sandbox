public class DocumentRepositoryListController {
    //document reporitory
    public List<Document_Repository__c> docList = new List<Document_Repository__c>();
    public String docRepDocTypeString{get;set;}
    public String docRepFundString{get;set;}
    public String docRepSectorString{get;set;}
    public String docRepAuthorString{get;set;}
    
    public List<id> metaDataList = new List<id>();
    
    public Id docDownloadId { get; set;}
    public class TempDocument {
    
     public TempDocument(String name, String docType, String docFund, String docSector, String docAuthor, String id){
         this.name = name;
         this.docType = docType;
         this.docFund = docFund;
         this.docSector = docSector;
         this.docAuthor = docAuthor;
         this.id = id;
     }
      public String name { get; set; }
      public String docType { get; set; }
      public String docFund { get; set; }
      public String docSector { get; set; }
      public String docAuthor { get; set; }
      public String id { get; set;}
    }

    public List<TempDocument> documents { get; set; }
    public List<TempDocument> tempDoc = new List<TempDocument>();
    User u;
    public DocumentRepositoryListController() {
       docList = [SELECT Id, Document_Type__c, Fund__r.Name, Sector__r.Name, Author__c FROM Document_Repository__c ORDER BY Document_Type__c ASC];
       
       documents = new List<TempDocument>();
       
       Id userId = UserInfo.getUserId();
       u = [select contactid from user where id = :userid limit 1];
       
       
       List<Contact_Package__c> cprList = [select Package__c from Contact_Package__c where Contact__c = :u.contactid];
       
       List<Id> idList = new List<Id>();
       
       for (Contact_Package__c c : cprList) {
          idList.add(c.Package__c);
       }
       
       //get related account from user's contact
       List<Contact> accListFromCon = [SELECT Id, AccountId FROM Contact WHERE Id = :u.contactid];
       List<Id> accIdList = new List<Id>();
       
       for(Contact c: accListFromCon){
            accIdList.add(c.AccountId);
       }
       
       List<Account_Package__c> capList = [SELECT Package__c FROM Account_Package__c WHERE Client__c IN :accIdList];
       for(Account_Package__c ap: capList){
            idList.add(ap.Package__c);
       }
       
       List<Package_Document__c> docList = [select Document__c from Package_Document__c where package_repository__c in :idList];
      
       for(Package_Document__c d : docList)
       {
          metaDataList.add(d.Document__c);
       }
       
       Map<Id,Document_Repository__c> docs = new Map<Id,Document_Repository__c>([select id, name, Document_Type__c, Fund__r.Name, Sector__r.Name, Author__c from Document_Repository__c where id in :metadataList]);
       
       List<Attachment> attachments = [Select id,parentId from Attachment where parentId in  :metadataList];
       
       system.debug('!!!attachments '+attachments );

       for(Attachment a : attachments) {
          documents.add(new TempDocument(docs.get(a.parentId).name, docs.get(a.parentId).Document_Type__c, docs.get(a.parentId).Fund__r.Name, docs.get(a.parentId).Sector__r.Name, docs.get(a.parentId).Author__c, a.id));
       }
    }
    
    public PageReference kickDownload() {
      
      Attachment attachment = [Select id,parentId,body,name from Attachment where id = :docDownloadId];
      
      document_access__c ticket = new document_access__c();
      ticket.Contact__c = u.contactId;
      insert ticket;
      
      Attachment newAttachment = attachment.clone();
      newAttachment.parentid = ticket.id;
      insert newAttachment;
      
      return new PageReference('/servlet/servlet.FileDownload?file='+newAttachment.id);
     
    }
    /*************************************************
    ******get document repository - document type*****
    *************************************************/
    public List<selectOption> getDocRep(){
        Set<String> docType = new Set<String>();
        List<selectOption> docTypeOptions = new List<selectOption>();
        docList = [SELECT Id, Document_Type__c FROM Document_Repository__c ORDER BY Document_Type__c ASC];
        for(Document_Repository__c d: docList){
            if(d.Document_Type__c != null){
                docType.add(d.Document_Type__c);
            }
        }
        docTypeOptions.add(new SelectOption('--Choose Document Type--','--Choose Document Type--'));
        for(String s: docType){
            docTypeOptions.add(new SelectOption(s,s));
        }
        return docTypeOptions;
    }
    /*************************************************
    **********get document repository - funds*********
    *************************************************/
    public List<selectOption> getdocRepFunds(){
        Set<String> docFunds = new Set<String>();
        List<selectOption> docFundsOptions = new List<selectOption>();
        docList = [SELECT Id, Fund__r.Name FROM Document_Repository__c ORDER BY Fund__r.Name ASC];
        for(Document_Repository__c d: docList){
            if(d.Fund__c != null){
                docFunds.add(d.Fund__r.Name);
            }
        }
        docFundsOptions.add(new SelectOption('--Choose Funds--','--Choose Funds--'));
        for(String s: docFunds){
            docFundsOptions.add(new SelectOption(s,s));
        }
        return docFundsOptions;
    }
    /*************************************************
    *********get document repository - Sectors********
    *************************************************/
    public List<selectOption> getdocRepSector(){
        Set<String> docSector = new Set<String>();
        List<selectOption> docSectorOptions = new List<selectOption>();
        docList = [SELECT Id, Sector__r.Name FROM Document_Repository__c ORDER BY Sector__r.Name ASC];
        for(Document_Repository__c d: docList){
            if(d.Sector__c != null){
                docSector.add(d.Sector__r.Name);
            }
        }
        docSectorOptions.add(new SelectOption('--Choose Sector--','--Choose Sector--'));
        for(String s: docSector){
            docSectorOptions.add(new SelectOption(s,s));
        }
        return docSectorOptions;
    }
    /*************************************************
    *********get document repository - Author********
    *************************************************/
    public List<selectOption> getdocRepAuthor(){
        Set<String> docAuthor = new Set<String>();
        List<selectOption> docAuthorOptions = new List<selectOption>();
        docList = [SELECT Id, Author__c FROM Document_Repository__c ORDER BY Author__c ASC];
        for(Document_Repository__c d: docList){
            if(d.Author__c != null){
                docAuthor.add(d.Author__c);
            }
        }
        docAuthorOptions.add(new SelectOption('--Choose Author--','--Choose Author--'));
        for(String s: docAuthor){
            docAuthorOptions.add(new SelectOption(s,s));
        }
        return docAuthorOptions;
    }
    /*************************************************
    *************FILTERING LIST DOCUMENT**************
    *************************************************/
    public void getDocumentList(){
        documents.clear();
        tempDoc.clear();
        Map<Id,Document_Repository__c> docs = new Map<Id,Document_Repository__c>([select id, name, Document_Type__c, Fund__r.Name, Sector__r.Name, Author__c from Document_Repository__c where id in :metadataList]);
        
        List<Attachment> attachments = [Select id, parentId from Attachment where parentId in  :metadataList];
        
        system.debug('!!!attachments '+attachments );
        
        for(Attachment a : attachments) {
          documents.add(new TempDocument(docs.get(a.parentId).name, docs.get(a.parentId).Document_Type__c, docs.get(a.parentId).Fund__r.Name, docs.get(a.parentId).Sector__r.Name, docs.get(a.parentId).Author__c, a.id));
        }
        /************* FIRST COMBI (1000,1100,1110,1111,1010)*************/
        if(docRepDocTypeString != '--Choose Document Type--' && docRepFundString == '--Choose Funds--' && docRepSectorString == '--Choose Sector--' && docRepAuthorString == '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docType == docRepDocTypeString){
                    tempDoc.add(td);
                }
            }
        }
        if(docRepDocTypeString != '--Choose Document Type--' && docRepFundString != '--Choose Funds--' && docRepSectorString == '--Choose Sector--' && docRepAuthorString == '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docType == docRepDocTypeString && td.docFund == docRepFundString){
                    tempDoc.add(td);
                }
            }
        }
        if(docRepDocTypeString != '--Choose Document Type--' && docRepFundString != '--Choose Funds--' && docRepSectorString != '--Choose Sector--' && docRepAuthorString == '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docType == docRepDocTypeString && td.docFund == docRepFundString && td.docSector == docRepSectorString){
                    tempDoc.add(td);
                }
            }
        }
        if(docRepDocTypeString != '--Choose Document Type--' && docRepFundString != '--Choose Funds--' && docRepSectorString != '--Choose Sector--' && docRepAuthorString != '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docType == docRepDocTypeString && td.docFund == docRepFundString && td.docSector == docRepSectorString && td.docAuthor == docRepAuthorString){
                    tempDoc.add(td);
                }
            }
        }
        if(docRepDocTypeString != '--Choose Document Type--' && docRepFundString == '--Choose Funds--' && docRepSectorString != '--Choose Sector--' && docRepAuthorString == '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docType == docRepDocTypeString && td.docSector == docRepSectorString){
                    tempDoc.add(td);
                }
            }
        }
        /************* SECOND COMBI (0100,0110,0111,0101)*************/
        if(docRepDocTypeString == '--Choose Document Type--' && docRepFundString != '--Choose Funds--' && docRepSectorString == '--Choose Sector--' && docRepAuthorString == '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docFund == docRepFundString){
                    tempDoc.add(td);
                }
            }
        }
        if(docRepDocTypeString == '--Choose Document Type--' && docRepFundString != '--Choose Funds--' && docRepSectorString != '--Choose Sector--' && docRepAuthorString == '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docFund == docRepFundString && td.docSector == docRepSectorString){
                    tempDoc.add(td);
                }
            }
        }
        if(docRepDocTypeString == '--Choose Document Type--' && docRepFundString != '--Choose Funds--' && docRepSectorString != '--Choose Sector--' && docRepAuthorString != '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docFund == docRepFundString && td.docSector == docRepSectorString && td.docAuthor == docRepAuthorString){
                    tempDoc.add(td);
                }
            }
        }
        if(docRepDocTypeString == '--Choose Document Type--' && docRepFundString != '--Choose Funds--' && docRepSectorString == '--Choose Sector--' && docRepAuthorString != '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docFund == docRepFundString && td.docAuthor == docRepAuthorString){
                    tempDoc.add(td);
                }
            }
        }
        /************* THIRD COMBI (0010,0011,1011)*************/
        if(docRepDocTypeString == '--Choose Document Type--' && docRepFundString == '--Choose Funds--' && docRepSectorString != '--Choose Sector--' && docRepAuthorString == '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docSector == docRepSectorString){
                    tempDoc.add(td);
                }
            }
        }
        if(docRepDocTypeString == '--Choose Document Type--' && docRepFundString == '--Choose Funds--' && docRepSectorString != '--Choose Sector--' && docRepAuthorString != '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docSector == docRepSectorString && td.docAuthor == docRepAuthorString){
                    tempDoc.add(td);
                }
            }
        }
        if(docRepDocTypeString != '--Choose Document Type--' && docRepFundString == '--Choose Funds--' && docRepSectorString != '--Choose Sector--' && docRepAuthorString != '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docSector == docRepSectorString && td.docAuthor == docRepAuthorString && td.docType == docRepDocTypeString){
                    tempDoc.add(td);
                }
            }
        }
        /************* FOURTH COMBI (0001,1001)*************/
        if(docRepDocTypeString == '--Choose Document Type--' && docRepFundString == '--Choose Funds--' && docRepSectorString == '--Choose Sector--' && docRepAuthorString != '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docAuthor == docRepAuthorString){
                    tempDoc.add(td);
                }
            }
        }
        if(docRepDocTypeString != '--Choose Document Type--' && docRepFundString == '--Choose Funds--' && docRepSectorString == '--Choose Sector--' && docRepAuthorString != '--Choose Author--'){
            for(TempDocument td: documents){
                if(td.docType == docRepDocTypeString && td.docAuthor == docRepAuthorString){
                    tempDoc.add(td);
                }
            }
        }
        
        documents.clear();
        documents.addAll(tempDoc);
    }
    /***************** CLEAR FILTER *****************/
    public PageReference clearFilter(){
        /*documents.clear();
        Map<Id,Document_Repository__c> docs = new Map<Id,Document_Repository__c>([select id, name, Document_Type__c, Fund__r.Name, Sector__r.Name, Author__c from Document_Repository__c where id in :metadataList]);
        
        List<Attachment> attachments = [Select id, parentId from Attachment where parentId in  :metadataList];
        
        system.debug('!!!attachments '+attachments );
        
        for(Attachment a : attachments) {
          documents.add(new TempDocument(docs.get(a.parentId).name, docs.get(a.parentId).Document_Type__c, docs.get(a.parentId).Fund__r.Name, docs.get(a.parentId).Sector__r.Name, docs.get(a.parentId).Author__c, a.id));
        }*/
        PageReference pageRef = page.ListDocuments;
        pageref.setRedirect(true);
        return pageRef;
    }
}