public class Fund_Recommendation_TriggerHandler {
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	public Fund_Recommendation_TriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	/*private void checkFundRec(Fund_Recommendation__c[] pFRList){
		
		Set<Id> fundIdSet 	= new Set<Id>();
		for (Fund_Recommendation__c t : pFRList){
			if (t.Fund__c!=null){
				fundIdSet.add(t.Fund__c);
			}
		}
		
		List<Fund_Recommendation__c> frList = [select Id, Fund__c
										from Fund_Recommendation__c 
										where Fund__c IN :fundIdSet];
		
		
		for (Fund_Recommendation__c newFR : pFRList){
			for (Fund_Recommendation__c oldFR : frList){
				if (newFR.Fund__c == oldFR.Fund__c)	newFR.addError('There can only be one Fund Recommendation per Fund.');
			}
		}
		
	}*/
	
	private void updateRatingChange(Fund_Recommendation__c[] pFRList, Map<Id,Fund_Recommendation__c> oldMap, Map<Id,Fund_Recommendation__c> newMap){
		Map<String,Integer> valueLevelMap = new Map<String,Integer>();
		valueLevelMap.put('Highly Recommended',0);
		valueLevelMap.put('Highly Recommended (Index)',1);
		valueLevelMap.put('Recommended',2);
		valueLevelMap.put('Recommended (Index)',3);
		valueLevelMap.put('Investment Grade',4);
		valueLevelMap.put('Investment Grade (Index)',5);
		valueLevelMap.put('Approved',6);
		valueLevelMap.put('Fund Watch',7);
		valueLevelMap.put('Non Investment Grade',8);
		valueLevelMap.put('Accept Merger',9);
		valueLevelMap.put('Redeem',10);
		valueLevelMap.put('Under Review',11);
		valueLevelMap.put('ClosedWind up',12);
		valueLevelMap.put('Ceased Coverage',13);
		valueLevelMap.put('Not Rated',14);
		 
		for (Fund_Recommendation__c fr : pFRList){
			Fund_Recommendation__c oldFR = oldMap.get(fr.Id);
			Fund_Recommendation__c newFR = newMap.get(fr.Id);
			
			if (oldFR.Recommendation_picklist__c != fr.Recommendation_picklist__c){ // && newFR is rejected
				
				Integer oldVal = valueLevelMap.get(oldFR.Recommendation_Picklist__c);
				Integer newVal = valueLevelMap.get(fr.Recommendation_Picklist__c);
				
				if (oldVal > newVal) // lower integer value means higher level
					fr.Rating_Change__c = 'Upgrade';
				else
					fr.Rating_Change__c = 'Downgrade';
			}
			else{
				fr.Rating_Change__c = 'No Change';
			}
		}
	}
	
	private void createBulletinRecords(Fund_Recommendation__c[] pFRList, Map<Id,Fund_Recommendation__c> oldMap){
		Set<Id> fundRecIdSet = oldMap.keySet();
		Set<Id> fundIdSet 	= new Set<Id>();
		for (Fund_Recommendation__c t : pFRList){
			if (t.Fund__c!=null){
				fundIdSet.add(t.Fund__c);
			}
		}
		/*Map<Id,String> frIdCommentMap = new Map<Id,String>();
		
		//get all related Approval history comments for each. Only the latest
		List<ProcessInstance> piList = [select Id, TargetObjectId, (select Comments from Steps order by SystemModstamp DESC limit 1) from ProcessInstance where TargetObjectId IN :fundRecIdSet Order by LastModifiedDate DESC];
		for(ProcessInstance pi : piList){
			for(ProcessInstanceStep piStep : pi.Steps){
				if (!frIdCommentMap.containskey(pi.TargetObjectId))
					frIdCommentMap.put(pi.TargetObjectId, piStep.Comments);
			}
		}*/
		
		Map<Id,String> fIdAPIRMap = new Map<Id,String>();
		//get all APIR related to the Fund
		for(Tax_Structure__c ts : [Select Fund__c, APIR_Code__c from Tax_Structure__c where Fund__c IN: fundIdSet]){
			if (ts.APIR_Code__c != null && ts.APIR_Code__c != ''){
				String tempAPIR;
				if (fIdAPIRMap.containsKey(ts.Fund__c)){
					tempAPIR = fIdAPIRMap.get(ts.Fund__c);
					tempAPIR += ', ' + ts.APIR_Code__c;
				}else{
					tempAPIR = ts.APIR_Code__c;
				}
				fIdAPIRMap.put(ts.Fund__c, tempAPIR);
			}
		}
		
		Research_Bulletin_Main__c rbToday;
		for (Research_Bulletin_Main__c rb : [select Id, Date__c from Research_Bulletin_Main__c where Date__c = TODAY]){
			rbToday = rb;
		}
		if (rbToday == null){
			rbToday = new Research_Bulletin_Main__c(Date__c = date.today());
			insert rbToday;
		}
		
		List<Research_Bulletin__c> rbList = new List<Research_Bulletin__c>();
		for (Fund_Recommendation__c fr : pFRList){
			Fund_Recommendation__c oldFR = oldMap.get(fr.Id);
			if (fr.Approved_Date__c != null){
				//if (oldFR.Approved_Date__c != fr.Approved_Date__c){
					Research_Bulletin__c rb = new Research_Bulletin__c();
					rb.APIR__c						= fIdAPIRMap.get(fr.Fund__c); // get this from fund's tax struct (more than one)
					//rb.Comments__c					= frIdCommentMap.get(fr.Id); // is this a separate object or should there be a recommendation textarea in fund
					rb.Comments__c					= fr.Description__c; // is this a separate object or should there be a recommendation textarea in fund
					rb.Fund_Name__c					= fr.Fund__c;
					rb.Recommendation_Change__c		= fr.Rating_Change__c;
					rb.Research_Bulletin_Main__c	= rbToday.Id;
					rb.Section__c					= fr.Section__c;
					
					rbList.add(rb);
				//}
			}
		}
		
		if (rbList.size()>0)	insert rbList;
		
	}
	
	public void OnBeforeInsert(Fund_Recommendation__c[] pFRList){
		//checkFundRec(pFRList);
	}
	
	public void OnBeforeUpdate(Fund_Recommendation__c[] pFRList, Map<Id,Fund_Recommendation__c> oldMap, Map<Id,Fund_Recommendation__c> newMap){
		updateRatingChange(pFRList, oldMap, newMap);
	}
	
	public void OnAfterUpdate(Fund_Recommendation__c[] pFRList, Map<Id,Fund_Recommendation__c> oldMap){
		createBulletinRecords(pFRList, oldMap);
	}
	
	static testMethod void test () {
        Sector__c s = Test_Util.createSector(null);
        insert s;
        
        List<Fund__c> fList = new List<Fund__c>();
        for (Integer i=0 ; i<20 ; i++){
        	Fund__c f = Test_Util.createFund(s.Id);
        	
        	fList.add(f);
        }
        insert fList;
        
        Fund_Recommendation__c fr = Test_Util.createFR(fList[0].Id);
        insert fr;
        //Fund_Recommendation__c fr2 = Test_Util.createFR(fList[0].Id);
        //insert fr2;
        
        fr.Approved_Date__c = date.today();
        update fr;
        
        // Start next approval process
        Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
        request.setObjectId(fr.Id);
        request.setComments('test');
        List<String> approverList = new List<String>();
        approverList.add(UserInfo.getuserId());
        request.setNextApproverIds(approverList);
        Approval.ProcessResult requestResult = Approval.process(request);
        
	}
}