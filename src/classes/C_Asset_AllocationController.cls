public class C_Asset_AllocationController{
    
    public String fpId {get;set;}
    public Fund_Profile__c fp { get; set; }
    public List<Asset_Allocation__c> aaList { get; set; }
    public List<aaWrapper> newAssetAllocationWrapper { get; set; }
    
    
    public class aaWrapper{
        public Asset_Allocation__c aa {get;set;}
        public Boolean isDeleted {get;set;}
        public Integer index {get;set;}
        
        public aaWrapper(Asset_Allocation__c pAa, Boolean pIsDeleted, Integer pIndex){
            aa = pAa;
            isDeleted = pIsDeleted;
            index = pIndex;
        }
    }
    
    public C_Asset_AllocationController(ApexPages.standardcontroller controller){
        initialize();
    }
    
    public C_Asset_AllocationController(){
        initialize();
    }
    
    public void initialize(){ 
        try{
            fpId = Apexpages.currentPage().getParameters().get('fpId');
            
            newAssetAllocationWrapper = new List<aaWrapper>();
            
            //getRelatedBenchmarkRiskProfileDetais();
            if (fpId!=null && fpId!=''){
                fp = [select Id, (select Id, Name, Fund_Profile__c, Asset_Code__c , Weight__c, Active__c from Asset_Allocation__r) from Fund_Profile__c where Id = :fpId];
                aaList = fp.Asset_Allocation__r;
            }
        
        }catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
            System.debug('!!!' + e.getMessage());
        }
    }
    
    public PageReference addAssetAllocation(){
        try{
        List<aaWrapper> tempList = new List<aaWrapper>();
        
        if (newAssetAllocationWrapper.size()>0)
            newAssetAllocationWrapper.add(new aaWrapper(new Asset_Allocation__c(Fund_Profile__c=fp.Id,Active__c=true),false,newAssetAllocationWrapper[newAssetAllocationWrapper.size()-1].index+1));
        else
            newAssetAllocationWrapper.add(new aaWrapper(new Asset_Allocation__c(Fund_Profile__c=fp.Id,Active__c=true),false,0));
            
        }catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }
        return null;
    }
    
    public PageReference save(){
        // All validations done on the page level
        List<Asset_Allocation__c> aaToInsert = new List<Asset_Allocation__c>();
        List<Asset_Allocation__c> brpdToDelete = new List<Asset_Allocation__c>();
        Decimal weight = 0;
        //Set<Id> fundProfileIds = new Set<Id>();
        
        Asset_Type__c ac = [select Id, Asset_Name__c from Asset_Type__c where Asset_Name__c = 'Cash'];
        
        for(aaWrapper aaw : newAssetAllocationWrapper){
            if (!aaw.isDeleted){
                if (aaw.aa.Weight__c!=null){
                    if (aaw.aa.Weight__c<=0){
                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Current Weight should be greater than 0.'));
                        return null;
                    }
                    //fundProfileIds.add(aaw.aa.Fund_Profile__c);
                    aaToInsert.add(aaw.aa);
                    weight += aaw.aa.Weight__c;
                }
                else{
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Current Weight should not be blank.'));
                    return null;
                }       
            }
        }
        
        if (weight>100){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Total of all asset allocation weights must not exceed to 100'));
            return null;
        }
        if(aaToInsert.size()>0 && weight<100){
            //totalWeight = totalWeight-weight;
            Asset_Allocation__c aa = new Asset_Allocation__c();
            aa.Fund_Profile__c = fp.Id;
            aa.Asset_Code__c = ac.Id;
            aa.Weight__c = 100 - weight;
            aa.Active__c = true;
            aaToInsert.add(aa);
        }
        
        if (aaToInsert.size()>0)    insert aaToInsert;
        if (brpdToDelete.size()>0)    delete brpdToDelete;
        return new PageReference('/'+fp.Id);
    }
    
    public PageReference cancel(){
        return new PageReference('/'+fp.Id);
    }
}