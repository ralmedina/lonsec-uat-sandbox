/*Description: This class acts as controller for the PC_GrowthSeries page. It gets growth series data related to a fund or benchmark
@author      :Robert Andrew Almedina
@date        :May 20, 2013
 */

public with sharing class PC_GrowthSeriesController {
	
	public String mainJSONDebug {get;set;}
	
	// for debugging only
	public List<Growth_Series__c> aa {get;set;} 
	public String debugStr {get;set;}
	public String debugStrStat {get;set;}
	public String debugQry {get;set;}
	// for debugging only

	public list<wrapper> lstWrapper{get;set;} // this is displayed in the growth series tab

	public Boolean isCompositeBenchmark {get;set;}

    public id recordId; 
	
	/* Constructor */
	public PC_GrowthSeriesController(){
		debugStr = '';
		mainJSONDebug = '';
		isCompositeBenchmark = false;
		
		datePick = new Growth_Series__c();
		datePick.Month_End__c = date.today();
		// convert the selected date to the last day of the selected month
		datePick.Month_End__c = datePick.Month_End__c.addDays( (Date.daysInMonth( datePick.Month_End__c.year(),datePick.Month_End__c.month()) - datePick.Month_End__c.day() ) );
		getPageParameters();
		initialize();
	}
	
	List<Growth_Series__c> gsList = new List<Growth_Series__c>();
	//method to populate the growth series data
	public void Initialize(){
		try{
			lstWrapper= new list<wrapper>();
			
			// on initial load, lead the growth series tab
			initializeGrowthSeriesTABFromTaxStructure();
			
		}catch(Exception e){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'An error has occurred. Please contact your System Administrator and send the following error message: '+e.getMessage() + +e.getStackTraceString()));
        	debugStr = e.getMessage() + +e.getStackTraceString();
        }
	}
	
	/*public List<Growth_Series__c> getBenchmarkGrowthSeries(){
		
		List<Growth_Series__c> finalGSList = new List<Growth_Series__c>(); // this variable contains the computed value of the growth series
		
		Map<Id,Double> bIdWeightMap = new Map<Id,Double>();
		Map<Id,List<Growth_Series__c>> benchmarkIdGrowthSeriesListMap = new Map<Id,List<Growth_Series__c>>();
		Map<Id,Date> benchmarkIdStartDateMap = new Map<Id,Date>(); // this contains the benchmark growthseries start date
		Map<Id,Date> benchmarkIdEndDateMap = new Map<Id,Date>(); // this contains the benchmark growthseries end date
		List<List<Growth_Series__c>> bmGrowthSeriesListList = new List<List<Growth_Series__c>>();
		
		List<Benchmark_Component__c> bcList = [	select Name, Composite_Benchmark__c, Component_Benchmark__c, Id, Benchmark_Weight__c 
												from Benchmark_Component__c where Composite_Benchmark__c = :recordId];
		// if true, it is a composite benchmark
		if (bcList.size()>0){
			isCompositeBenchmark = true;
			
			// get all benchmark components whose composite benchmark = mainBenchmarkId
			for (Benchmark_Component__c bc : bcList){
				
				// get weight of each benchmark component
				bIdWeightMap.put(bc.Component_Benchmark__c, bc.Benchmark_Weight__c/100);
				
				// putting query here using getGrowthSeries() since maximum related record is 5
				List<Growth_Series__c> tempGSList = getBGrowthSeries('', bc.Component_Benchmark__c);
				benchmarkIdGrowthSeriesListMap.put(bc.Component_Benchmark__c, tempGSList);
				
				// minimum size of growth series data should be 2 since it cannot be computed if it has only one row of data
				if (tempGSList.size()>1){
					// get last record of the list since it is in DESC order.
					benchmarkIdStartDateMap.put(bc.Component_Benchmark__c, tempGSList[tempGSList.size()-1].Month_End__c);
					
					// get first record of the list since it is in DESC order.
					benchmarkIdEndDateMap.put(bc.Component_Benchmark__c, tempGSList[0].Month_End__c);
				}
				debugStr += ' 1= ';
			}
			Date startDate = getCommonDate(benchmarkIdStartDateMap, 'start');
			Date endDate = getCommonDate(benchmarkIdStartDateMap, 'end');
			
			for (String key : benchmarkIdGrowthSeriesListMap.keySet()){
				List<Growth_Series__c> tempGSList = new List<Growth_Series__c>();
				for (Growth_Series__c gs : benchmarkIdGrowthSeriesListMap.get(key)){
					if (gs.Month_End__c >= startDate && gs.Month_End__c <= endDate)
						tempGSList.add(gs);
				}
				bmGrowthSeriesListList.add(new List<Growth_Series__c>(tempGSList));
				debugStr += '  ||'+key + ' size of tempGSList is : '+tempGSList.size();
			}
			debugStr += ' bmGrowthSeriesListList size is : '+ bmGrowthSeriesListList.size();
			debugStr += ' bmGrowthSeriesListList[0] size is : '+ bmGrowthSeriesListList[0].size();
			// loop the total number of overlapping growth series data for all the benchmark components
			for (Integer gsCtr=0 ; gsCtr<bmGrowthSeriesListList[0].size() ; gsCtr++){
				
				Date monthEnd 		= date.today();
				Double lonsecValue 	= 0;
				Double valueFE 		= 0;
				Double lonsecExDev 	= 0;
				Double exDevFE 		= 0;
				// loop the total number of benchmark components
				for (Integer bCtr=0 ; bCtr<bmGrowthSeriesListList.size() ; bCtr++){
					Growth_Series__c tempGS = bmGrowthSeriesListList[bCtr][gsCtr];
					Double weight 	= bIdWeightMap.get(tempGS.Benchmark__c);
					lonsecValue 	+= (tempGS.Lonsec_Value__c * weight);
					valueFE 		+= (tempGS.Value_FE__c * weight);
					lonsecExDev 	+= (tempGS.Lonsec_Ex_Dev__c * weight);
					exDevFE 		+= (tempGS.Ex_Dev_FE__c * weight);
					monthEnd 		= tempGS.Month_End__c;
				}
				finalGSList.add(new Growth_Series__c(	Month_End__c 		= monthEnd,
														Lonsec_Value__c 	= lonsecValue,
														Value_FE__c			= valueFE,
														Lonsec_Ex_Dev__c	= lonsecExDev,
														Ex_Dev_FE__c		= exDevFE
													));
													debugStr += 'ako= ';
			}
			
		}else{
			finalGSList = getBGrowthSeries('', recordId);
		}
		return finalGSList;
	}*/
	
	public List<Growth_Series__c> getBGrowthSeries(String addedCondition, String benchmarkCodeId){
		
		String qryString = 'Select CreatedDate, Benchmark_Code__c, Is_Lonsec_Data__c, Is_Validated_Data__c, Lonsec_Value__c, Month_End__c, Tax_Structure_Investment_Code__c, Value_FE__c, LastModifiedDate, Lonsec_Ex_Dev__c, Ex_Dev_FE__c From Growth_Series__c ';
		String condition = '';
		
		// this is to remove time from the date field to prevent error when querying
		addedCondition = addedCondition.replaceAll('00:00:00', '');
		condition = ' Where Benchmark_Code__c =\'' 	+ benchmarkCodeId + '\'';
		
		debugQry = qryString + condition + addedCondition + ' Order by Month_End__c DESC';
		
		return database.query(qryString + condition + addedCondition + ' Order by Month_End__c DESC limit 1000');
	}
	
	public List<Growth_Series__c> getGrowthSeries(String addedCondition, String referenceId){
		
		String qryString = 'Select CreatedDate, Benchmark_Code__c, Is_Lonsec_Data__c, Is_Validated_Data__c, Lonsec_Value__c, Month_End__c, Tax_Structure_Investment_Code__c, Value_FE__c, LastModifiedDate, Lonsec_Ex_Dev__c, Ex_Dev_FE__c From Growth_Series__c ';
		String condition = '';
		
		// this is to remove time from the date field to prevent error when querying
		addedCondition = addedCondition.replaceAll('00:00:00', '');
		condition = ' Where Tax_Structure_Investment_Code__c =\'' 	+ referenceId + '\'';
		debugQry = qryString + condition + addedCondition + ' Order by Month_End__c DESC';
		
		return database.query(qryString + condition + addedCondition + ' Order by Month_End__c DESC limit 1000');
	}
	
	public PageReference save(){
		List<Growth_Series__c> gsToUpdate = new List<Growth_Series__c>();
		for (wrapper w : lstWrapper){
			gsToUpdate.add(w.objGrowthSeries);
		}
		update gsToUpdate;
		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, 'Records have been successfully updated.'));
		return null;
	}
	
	
	
	// This is for the Performance Tab
	
	public Growth_Series__c datePick {get;set;}
	public list<PC_MatlabAPICall.wrapperclass> wlist {get;set;}
	
	public void initializePerformanceTab(){
		datePick = new Growth_Series__c();
		datePick.Month_End__c = date.today();
		// convert the selected date to the last day of the selected month
		datePick.Month_End__c = datePick.Month_End__c.addDays( (Date.daysInMonth( datePick.Month_End__c.year(),datePick.Month_End__c.month()) - datePick.Month_End__c.day() ) );
		//instantiating all the wrapperclass lists
		wlist = new list<PC_MatlabAPICall.wrapperclass>();
		displayWrapperValue();
	}
	
	public Map<Id,List<Growth_Series__c>> processedBenchmarkIdGrowthSeriesListMap = new Map<Id,List<Growth_Series__c>>();
	
	
	// get benchmark growthseries (composite or not) - used for the Performance TAB only
	Map<String,String> benchmarkIdBenchmarkProviderMap = new Map<String,String>();
	Map<String,String> benchmarkIdBenchmarkCodeIdMap = new Map<String,String>();
	public String createBenchmarkJSONString(String mainBenchmarkId, String addedCondition){
		benchmarkIdBenchmarkProviderMap = new Map<String,String>();
		benchmarkIdBenchmarkCodeIdMap 	= new Map<String,String>();
		
		processedBenchmarkIdGrowthSeriesListMap.clear();
		String benchmarkString = ''; // this contains the consolidated benchmark JSON string
		
		//List<Benchmark__c> bList = new List<Benchmark__c>();
		Map<Id,Double> bIdWeightMap = new Map<Id,Double>();
		Map<Id,List<Growth_Series__c>> benchmarkIdGrowthSeriesListMap = new Map<Id,List<Growth_Series__c>>();
		Map<Id,Date> benchmarkIdStartDateMap = new Map<Id,Date>(); // this contains the benchmark growthseries start date
		Map<Id,Date> benchmarkIdEndDateMap = new Map<Id,Date>(); // this contains the benchmark growthseries end date
		Benchmark__c mainBenchmark;
		//if (pageUsedFrom == 'Benchmark'){
			List<Benchmark_Component__c> bcList = [	select Name, Composite_Benchmark__c, Component_Benchmark__c, Id, Benchmark_Weight__c,
													Component_Benchmark__r.Benchmark_Code_Used_For_Growth_Series__c,
													Component_Benchmark__r.Benchmark_Code_Used_For_Growth_Series__r.Data_Provider__c 
													from Benchmark_Component__c where Composite_Benchmark__c = :mainBenchmarkId];
			if (bcList.size()>0){
				isCompositeBenchmark = true;
				for (Benchmark_Component__c tempBC : bcList){
					benchmarkIdBenchmarkProviderMap.put(tempBC.Component_Benchmark__c, tempBC.Component_Benchmark__r.Benchmark_Code_Used_For_Growth_Series__r.Data_Provider__c);
					benchmarkIdBenchmarkCodeIdMap.put(tempBC.Component_Benchmark__c, tempBC.Component_Benchmark__r.Benchmark_Code_Used_For_Growth_Series__c);
				}
			}else{
				mainBenchmark = [select Id, Benchmark_Code_Used_For_Growth_Series__c, Benchmark_Code_Used_For_Growth_Series__r.Data_Provider__c from Benchmark__c where Id = :mainBenchmarkId];
			}
			// if true, it is a composite benchmark
			if (bcList.size()>0){
				// get all benchmark components whose composite benchmark = mainBenchmarkId
				for (Benchmark_Component__c bc : bcList){
					
					// get weight of each benchmark component
					bIdWeightMap.put(bc.Component_Benchmark__c, bc.Benchmark_Weight__c/100);
					
					// putting query here using getGrowthSeries() since maximum related record is 5
					List<Growth_Series__c> tempGSList = getBGrowthSeries(addedCondition, bc.Component_Benchmark__r.Benchmark_Code_Used_For_Growth_Series__c);
					benchmarkIdGrowthSeriesListMap.put(bc.Component_Benchmark__c, tempGSList);
					
					// minimum size of growth series data should be 2 since it cannot be computed if it has only one row of data
					if (tempGSList.size()>1){
						// get last record of the list since it is in DESC order.
						benchmarkIdStartDateMap.put(bc.Component_Benchmark__c, tempGSList[tempGSList.size()-1].Month_End__c);
						
						// get first record of the list since it is in DESC order.
						benchmarkIdEndDateMap.put(bc.Component_Benchmark__c, tempGSList[0].Month_End__c);
					}
				}
				Date startDate = getCommonDate(benchmarkIdStartDateMap, 'start');
				Date endDate = getCommonDate(benchmarkIdStartDateMap, 'end');
				
				for (String key : benchmarkIdGrowthSeriesListMap.keySet()){
					List<Growth_Series__c> tempGSList = new List<Growth_Series__c>();
					for (Growth_Series__c gs : benchmarkIdGrowthSeriesListMap.get(key)){
						if (gs.Month_End__c >= startDate && gs.Month_End__c <= endDate)
							tempGSList.add(gs);
					}
					
					processedBenchmarkIdGrowthSeriesListMap.put(key, new List<Growth_Series__c>(tempGSList));
					
					benchmarkString += (benchmarkString == '') ? '' : ',';
					benchmarkString += 	'{"Weight": ' + 
										bIdWeightMap.get(key) + ', ' + 
										PC_MatlabAPICall.convertToJSON(tempGSList, 'Benchmark', benchmarkIdBenchmarkProviderMap.get(key))
										+ '}';
				}
			}else{
				List<Growth_Series__c> gsList = getBGrowthSeries(addedCondition, mainBenchmark.Benchmark_Code_Used_For_Growth_Series__c);
				benchmarkString = '{"Weight": 1, ' + PC_MatlabAPICall.convertToJSON(gsList, 'Benchmark', mainBenchmark.Benchmark_Code_Used_For_Growth_Series__r.Data_Provider__c) + '}';
			}
			return '"BenchmarkName": [ ' +benchmarkString + ' ]';
		//}
		//else{ // for Tax Structure Benchmark Data
			// get all benchmark component whose composite benchmark = mainBenchmarkId AND fund = tax structure's fund
		//}
		return null;
	}
	
	
	
	
	//method to diplay all table values using wrapperclass
	public void displayWrapperValue(){
		wlist.clear();
		if(datePick.Month_End__c == null)	datePick.Month_End__c = date.today();
		
		if(datePick.Month_End__c != null){
			try{
				// convert the selected date to the last day of the selected month
				datePick.Month_End__c = datePick.Month_End__c.addDays( (Date.daysInMonth( datePick.Month_End__c.year(),datePick.Month_End__c.month()) - datePick.Month_End__c.day() ) );
				
				// this is used to filter the date range which is upto 10yrs before date today
				Date dt = datePick.Month_End__c.addYears(-10);
				
				String dateCondition = ' AND Month_End__c <= ' + (Date)datePick.Month_End__c + ' AND Month_End__c >= ' + (Date)dt;
				String mainJSON = '';
			
				// this means the page is called from Tax structure page's View growth series link
				// get tax structure growthseries
				List<Growth_Series__c> tgsList = getGrowthSeries(dateCondition, recordId);
				
				// get the tax structure's fund's benchmark growth series
				Investment_Code__c ic = [select Data_Provider__c, Tax_Structure__c from Investment_Code__c where Id = :recordId];
				Tax_Structure__c tempTS = [select Fund__r.Cash_Benchmark_Code__c, Fund__r.Cash_Benchmark_Code__r.Data_Provider__c, Fund__r.Override_Fund_Benchmark__c, Fund__r.Sector__r.Benchmark__c 
											from Tax_Structure__c where Id =:ic.Tax_Structure__c];
				String benchmarkId = '';
				if (tempTS.Fund__r.Override_Fund_Benchmark__c!=null)	benchmarkId = tempTS.Fund__r.Override_Fund_Benchmark__c;
				else													benchmarkId = tempTS.Fund__r.Sector__r.Benchmark__c;
				
				// get benchmark growthseries
				//List<Growth_Series__c> bgsList = getGrowthSeries(dateCondition, benchmarkId);
				
				// convert the growth series data to JSON string format
				String tgsJSON = PC_MatlabAPICall.convertToJSON(tgsList, 'Fund', ic.Data_Provider__c);
				tgsJSON = '"MyFund1": { "Benchmark": "BenchmarkName", ' + tgsJSON + '}';
				
				// create the Benchmark GrowthSeries JSON String
				// loop this for multiple benchmarks
				//String bgsJSON = '';
				//String bgsJSON = '"BenchmarkName": [';
				String benchmarkWeights = '';
				
				/*//check if benchmark is a composite benchmark
				List<Benchmark_Component__c> bcList = [select Name, Composite_Benchmark__c, Component_Benchmark__c, Id, Benchmark_Weight__c from Benchmark_Component__c where Composite_Benchmark__c = :recordId];
				if (bcList.size()>1){
					for (Benchmark_Component__c bc : bcList){
						
						// loop this for composite benchmark weights
						String tempBGS = PC_MatlabAPICall.convertToJSON(bgsList, 'Benchmark', 'Lonsec');
						
						//benchmarkWeights = PC_MatlabAPICall.convertToJSON(bgsList, 'Benchmark', 'Lonsec');
						benchmarkWeights += (benchmarkWeights == '') ? '' : ',';
						benchmarkWeights += '{"Weight": ' + bc.Benchmark_Weight__c + ', ' + benchmarkWeights + '}';
					}
				}else{
					bgsList = getGrowthSeries(dateCondition, benchmarkId);
					// loop this for composite benchmark weights
					benchmarkWeights = PC_MatlabAPICall.convertToJSON(bgsList, 'Benchmark', 'Lonsec');
					Double weight = 1;
					benchmarkWeights = '{"Weight": ' + weight + ', ' + benchmarkWeights + '}';
					
				}*/
				
				String bgsJSON = createBenchmarkJSONString(benchmarkId, dateCondition);
				
				//bgsJSON = '"BenchmarkName": [' + benchmarkWeights + ']'; // this is for wrapping up one benchmark
				
				String cgsJSON = '';
				// get the (Cash is also the benchmark growthseries) or DEFAULT cash reference which is "UBS Bank Bill Index"  
				if (tempTS.Fund__r.Cash_Benchmark_Code__c != null)
					cgsJSON = PC_MatlabAPICall.convertToJSON(getBGrowthSeries(dateCondition, tempTS.Fund__r.Cash_Benchmark_Code__c), 'Cash', TempTS.Fund__r.Cash_Benchmark_Code__r.Data_Provider__c);
				else
					cgsJSON = PC_MatlabAPICall.getDefaultCashBasis();
				
				//String cgsJSON = PC_MatlabAPICall.convertToJSON(bgsList, 'Cash', 'Lonsec');
				//cgsJSON = cgsJSON;
				
				debugStrStat += ' |||| JSON TGS |||| ' + tgsJSON;
				debugStrStat += ' |||| JSON BGS |||| ' + bgsJSON;
				//debugStrStat += ' |||| JSON CASH |||| ' + cgsJSON;
				
				// wrap JSON string to before passing to API
				//mainJSON = PC_MatlabAPICall.wrapJSONString(tgsJSON +','+ bgsJSON +','+PC_MatlabAPICall.getDefaultCashBasis(), 'Tax Structure Performance Growth Series');
				//mainJSON = PC_MatlabAPICall.wrapJSONString(tgsJSON, bgsJSON, cgsJSON, 'Tax Structure Performance Growth Series');
				mainJSON = PC_MatlabAPICall.wrapJSONString(tgsJSON, bgsJSON, cgsJSON, 'Tax Structure Performance Growth Series');
			
				mainJSONDebug = mainJSON;
				debugStr = mainJSON;//tgsJSON +','+ bgsJSON +','+PC_MatlabAPICall.getDefaultCashBasis();
				
				JSONParser parser = PC_MatlabAPICall.parseJSONResponse(mainJSON); // this returns the response of the matlab API fed to a JSONParser
				// start parsing the json string by each section
				//debugStrStat = string.valueOf(parser);
				while (parser.nextToken() != null) {
		        	
		        	if (parser.getCurrentName()!=null){
		        		
		        		if (parser.getCurrentName() == 'TaxStructurePerformance'){
		        			System.debug('===== TaxStructurePerformance Text: '+parser.getText());
		        			debugStrStat+= ' parser.getText ========' + parser.getText();
		        			parser.nextToken();
		        			wlist = PC_MatlabAPICall.parseTaxStructurePerformance(parser.getText());
		        			//debugStr = parser.getText();
		        		}
		        		else if (parser.getCurrentName() == 'TaxStructurePerformanceGrowth'){
		        			System.debug('===== TaxStructurePerformance Text: '+parser.getText());
		        			debugStrStat+= ' parser.getText ========' + parser.getText();
		        			parser.nextToken();
		        			wlist = PC_MatlabAPICall.parseTaxStructurePerformanceGrowth(parser.getText());
		        			//debugStr = parser.getText();
		        		}	
		        		else if (parser.getCurrentName() == 'BenchmarkPerformance'){
		        			System.debug('===== TaxStructurePerformance Text: '+parser.getText());
		        			debugStrStat+= ' parser.getText ========' + parser.getText();
		        			parser.nextToken();
		        			wlist = PC_MatlabAPICall.parseBenchmarkPerformance(parser.getText());
		        			//debugStr = parser.getText();
		        		}	
		        	}
		        }
			}catch(Exception e){
	        	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage() + +e.getStackTraceString()));
	        	debugStrStat += 'EXCEPTION:: '+e.getMessage() + +e.getStackTraceString();
	        }
			
		}
	
	}
	
	///////////////////////////////////////////////// FINISHED METHODS

	/*	this method gets all the parameters passed to this page	*/
	public void getPageParameters(){
		recordId 		= ApexPages.currentPage().getParameters().get('Id');//to get current record id
	} 
	
	/* 	This method returns the common date of the date map
	   	@pDateMap contains map of benchmarkId and Dates (can be start date or end date)
	   	@typeOfDate may be = start or end	*/
	public Date getCommonDate(Map<Id,Date> pDateMap, String typeOfDate){
		// convert the selected date to the last day of the selected month
		datePick.Month_End__c = datePick.Month_End__c.addDays( (Date.daysInMonth( datePick.Month_End__c.year(),datePick.Month_End__c.month()) - datePick.Month_End__c.day() ) );
		
		// this is used to filter the date range which is upto 10yrs before date today
		Date dt = (typeOfDate == 'start') ? datePick.Month_End__c.addYears(-10) : datePick.Month_End__c;
		
		// initial start date is 10 yrs before the date selected in the date picker
		// initial end date is last day of current month
		Date finalDate = dt;
		
		for (Date d : pDateMap.values()){
			if (typeOfDate == 'start')	
				if (finalDate < d)	
					finalDate = d;
			else 
				if (finalDate > d)	
					finalDate = d;
		}
		return finalDate;
	}
	
	/* Wrapper class for growth series tab */
	public class wrapper{
		//wrapper class variables declaration
		public string objDate{get;set;}
		public string objLastModifiedDate{get;set;}
		public Growth_Series__c objGrowthSeries{get;set;}
		
	} 
	
	
	// this method initializes the growth series TAB when coming from a benchmark page
	public void initializeGrowthSeriesTABFromBenchmark(){
		// get growth series data related to the benchmark
		gsList = getGrowthSeries('', recordId);//getBenchmarkGrowthSeries();
		aa = gsList;
		
		//List<Growth_Series__c> gsList = getGrowthSeries('', recordId);
						
		for(Growth_Series__c gs:	gsList){
			//to convert date to MM/DD/YYYY format
			//string	LastDate= gs.CreatedDate.month() + '/' +  gs.CreatedDate.day() +'/'+ gs.CreatedDate.year();
			string	dateToDisplay= gs.Month_End__c.month() + '/' +  gs.Month_End__c.day()+'/'+ gs.Month_End__c.year();	
			
		   //wrapper class instance created	
			wrapper w = new wrapper();
			w.objGrowthSeries =gs;
			//w.objLastModifiedDate= LastDate;
			w.objDate = dateToDisplay;
			lstWrapper.add(w);
		}
	}
	
	// this method initializes the growth series TAB when coming from a tax structure page
	public void initializeGrowthSeriesTABFromTaxStructure(){
		// get growth series related to fund tax structure
		gsList = getGrowthSeries('', recordId);
		aa = gsList;
						
		for(Growth_Series__c gs:	gsList){
			//to convert date to MM/DD/YYYY format
			//string	LastDate= gs.CreatedDate.month() + '/' +  gs.CreatedDate.day() +'/'+ gs.CreatedDate.year();
			string	dateToDisplay= gs.Month_End__c.month() + '/' +  gs.Month_End__c.day()+'/'+ gs.Month_End__c.year();	
			
		   //wrapper class instance created	
			wrapper w = new wrapper();
			w.objGrowthSeries =gs;
			//w.objLastModifiedDate= LastDate;
			w.objDate = dateToDisplay;
			lstWrapper.add(w);
		}
		initializePerformanceTab();
	}
	
}