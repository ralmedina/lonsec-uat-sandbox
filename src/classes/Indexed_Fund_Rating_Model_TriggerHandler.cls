public class Indexed_Fund_Rating_Model_TriggerHandler {
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	public Indexed_Fund_Rating_Model_TriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	private void createIndexedFundScoreRecords(Indexed_Fund_Rating_Model__c[] pIFRMList, Set<Id> ifrmIdSet){
		Map<Id,Id> sectorIdIFRMIdMap = new Map<Id,Id>();
		for (Indexed_Fund_Rating_Model__c temp : pIFRMList){
			if (temp.Sector__c!=null){
				sectorIdIFRMIdMap.put(temp.Sector__c,temp.Id);
			}
		}
		//get old fund scores from old rating model
		Map<Id,Map<Id,Indexed_Fund_Rating_Score__c>> sectorIdFundIdOldScoreMap = new Map<Id,Map<Id,Indexed_Fund_Rating_Score__c>>();
		//List<Indexed_Fund_Rating_Model__c> tempList = [select Id from Indexed_Fund_Rating_Model__c where Id NOT IN :ifrmIdSet AND Sector__c IN :sectorIdSet Order by CreatedDate DESC];
		for (Indexed_Fund_Rating_Model__c tempifrm : [select Sector__c, (select Model_Rating__c, Fund__c from Indexed_Fund_Rating_Scores__r) from Indexed_Fund_Rating_Model__c where Id NOT IN :ifrmIdSet AND Sector__c IN :sectorIdIFRMIdMap.keySet() AND Status__c = 'Approved' Order by CreatedDate DESC]){
			
			Map<Id,Indexed_Fund_Rating_Score__c> tempMap = new Map<Id,Indexed_Fund_Rating_Score__c>();
			for (Indexed_Fund_Rating_Score__c tempifrs : tempifrm.Indexed_Fund_Rating_Scores__r){
				tempMap.put(tempifrs.Fund__c,tempifrs);
			}
			
			sectorIdFundIdOldScoreMap.put(tempifrm.Sector__c,tempMap);
		}
		
		List<Indexed_Fund_Rating_Score__c> ifrsToInsert = new List<Indexed_Fund_Rating_Score__c>();
		for (Fund__c f : [select Id, Sector__c from Fund__c where Sector__c IN :sectorIdIFRMIdMap.keySet() AND Indexed__c = true]){
			String oldRating = 'New'; // default value is New
			if (sectorIdFundIdOldScoreMap.containsKey(f.Sector__c)){
				if (sectorIdFundIdOldScoreMap.get(f.Sector__c).containsKey(f.Id)){
					oldRating = sectorIdFundIdOldScoreMap.get(f.Sector__c).get(f.Id).Model_Rating__c;
				}
			}
			ifrsToInsert.add(new Indexed_Fund_Rating_Score__c(Indexed_Fund_Rating_Model__c=sectorIdIFRMIdMap.get(f.Sector__c), Fund__c=f.Id,Existing_Rating__c=oldRating));
		}
		
		if (ifrsToInsert.size()>0)	insert ifrsToInsert;
	}
	
	public void OnAfterInsert(Indexed_Fund_Rating_Model__c[] pIFRMList, Set<Id> pIfrmIdSet){
		createIndexedFundScoreRecords(pIFRMList, pIfrmIdSet);
	}
	
	static testMethod void test () {
        Sector__c s = Test_Util.createSector(null);
        insert s;
        
        List<Fund__c> fList = new List<Fund__c>();
        for (Integer i=0 ; i<20 ; i++){
        	Fund__c f = Test_Util.createFund(s.Id);
        	f.Indexed__c = true;
        	fList.add(f);
        }
        insert fList;
        
        Indexed_Fund_Rating_Model__c ifrm = new Indexed_Fund_Rating_Model__c(Sector__c=s.Id);
        insert ifrm;
	}
}