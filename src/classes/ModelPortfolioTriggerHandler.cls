public class ModelPortfolioTriggerHandler {

    public static void archiveModelPortfolios(Set<Id> modelPortfolioIds) {
        
        Map<Id, List<Fund_Risk_Profile__c>> frpPerMP = new Map<Id, List<Fund_Risk_Profile__c>>();
        Map<Id, List<Benchmark_Portfolio__c>> bpPerMP = new Map<Id, List<Benchmark_Portfolio__c>>(); 
        Map<Id, List<Analysis__c>> anPerMP = new Map<Id, List<Analysis__c>>();
        Map<Id, List<Benchmark_Risk_Profile__c>> brpPerBP = new Map<Id, List<Benchmark_Risk_Profile__c>>(); 
        
        Set<Id> bpToQuery = new Set<Id>();
        
        Map<Id, IC_Model_Portfolio__c> mpToInsert = new Map<Id, IC_Model_Portfolio__c>();
        Map<Id, IC_Benchmark_Portfolio_Repository__c> bpToInsert = new Map<Id, IC_Benchmark_Portfolio_Repository__c>();
        Map<Id, IC_Benchmark_Risk_Profile_Repository__c> brpToInsert = new Map<Id, IC_Benchmark_Risk_Profile_Repository__c>();
        Map<Id, IC_Fund_Risk_Profile_Repository__c> frpToInsert = new Map<Id, IC_Fund_Risk_Profile_Repository__c>();
        Map<Id, IC_Analysis_Repository__c> anToInsert = new Map<Id, IC_Analysis_Repository__c>();
        
        List<IC_Benchmark_in_Risk_Profile_Repository__c> brpdToInsert = new List<IC_Benchmark_in_Risk_Profile_Repository__c>();
        List<IC_Fund_in_Risk_Profile_Repository__c> frpdToInsert = new List<IC_Fund_in_Risk_Profile_Repository__c>();
        List<IC_Fund_Commentary_Repository__c> fcToInsert = new List<IC_Fund_Commentary_Repository__c>();
        
        holdsModelIdSet(modelPortfolioIds);
        
        for(Model_Portfolio__c mp: [Select Id, 
                                           Overview_of_the_Quarter__c, 
                                           Overview_of_the_Year__c,
                                           Inception_Date__c 
                                    from   Model_Portfolio__c 
                                    where  ID IN: modelPortfolioIds]) {
        
            mpToInsert.put(mp.Id, new IC_Model_Portfolio__c(Date_of_Creation__c = Date.Today(), 
                                                           Model_Portfolio__c = mp.Id, 
                                                           Overview_of_the_Quarter__c = mp.Overview_of_the_Quarter__c,
                                                           Overview_of_the_Year__c = mp.Overview_of_the_Year__c,
                                                           Inception_Date__c = mp.Inception_Date__c));
        
        }
        
        if(mpToInsert.size()>0) {
            insert mpToInsert.values();
        }
        
        for(Fund_Risk_Profile__c frp: [Select Id,
                                              Model_Portfolio__c,
                                              Benchmark_Risk_Profile__c,
                                              (Select Id,
                                                      Current_Weight__c,
                                                      Date__c,
                                                      Fund__c,
                                                      Included_in_Perf_Calc__c,
                                                      Investment_Code__c 
                                               from   Fund_Risk_Profile_Details__r)
                                       From   Fund_Risk_Profile__c
                                       Where  Model_Portfolio__c IN: modelPortfolioIds] ) {
                                       
            if(frpPerMP.containsKey(frp.Model_Portfolio__c)) {
                frpPerMP.get(frp.Model_Portfolio__c).add(frp);
            } else {
                frpPerMP.put(frp.Model_Portfolio__c, new List<Fund_Risk_Profile__c>{frp});
            }
            
        }
        
        for(Benchmark_Portfolio__c bp: [Select  Id, 
                                                Name,
                                                Model_Portfolio__c,
                                                Default_Benchmark_Portfolio__c
                                        from    Benchmark_Portfolio__c
                                        where   Model_Portfolio__c IN: modelPortfolioIds]) {
            
            bpToQuery.add(bp.Id);
            if(bpPerMP.containsKey(bp.Model_Portfolio__c)) {
                bpPerMP.get(bp.Model_Portfolio__c).add(bp);
            } else {
                bpPerMP.put(bp.Model_Portfolio__c, new List<Benchmark_Portfolio__c>{bp});
            }
            
        }
        
        for(Benchmark_Risk_Profile__c brp: [Select  Id, 
                                                    Benchmark_Portfolio__c,
                                                    Model_Portfolio__c,
                                                    (Select Id,
                                                            Benchmark__c,
                                                            Benchmark_Code__c,
                                                            Current_Weight__c,
                                                            Date__c,
                                                            Included_in_Perf_Calc__c,
                                                            Sector__c
                                                      From  Benchmark_Risk_Profile_Details__r)
                                            from    Benchmark_Risk_Profile__c
                                            where   Benchmark_Portfolio__c IN: bpToQuery]) {
            
            if(brpPerBP.containsKey(brp.Benchmark_Portfolio__c)) {
                brpPerBP.get(brp.Benchmark_Portfolio__c).add(brp);
            } else {
                brpPerBP.put(brp.Benchmark_Portfolio__c, new List<Benchmark_Risk_Profile__c>{brp});
            }
            
        }
        
        for(Analysis__c an: [Select Id,
                                    Comment__c,
                                    Sector__c,
                                    Model_Portfolio__c,
                                    (Select Id,
                                            Commentary__c,
                                            Fund__c
                                     from   Fund_Commentaries1__r)
                             From   Analysis__c
                             Where  Model_Portfolio__c IN: modelPortfolioIds] ) {
                                       
            if(anPerMP.containsKey(an.Model_Portfolio__c)) {
                anPerMP.get(an.Model_Portfolio__c).add(an);
            } else {
                anPerMP.put(an.Model_Portfolio__c, new List<Analysis__c>{an});
            }
            
        }

        for(Id mpId: bpPerMP.keySet()) {
            for(Benchmark_Portfolio__c bp: bpPerMP.get(mpId)) {
                if(mpToInsert.containsKey(mpId)) {
                    bpToInsert.put(bp.Id, new IC_Benchmark_Portfolio_Repository__c(Name=bp.Name, Default_Benchmark_Portfolio__c = bp.Default_Benchmark_Portfolio__c, IC_Model_Portfolio__c = mpToInsert.get(mpId).Id));
                }
            }
        }
        
        if(bpToInsert.size()>0) {
            insert bpToInsert.values();
        }
        
        for(Id bpId: brpPerBP.keySet()) {
            for(Benchmark_Risk_Profile__c brp: brpPerBP.get(bpId)) {
                if(bpToInsert.containsKey(bpId)) {
                    brpToInsert.put(brp.Id, new IC_Benchmark_Risk_Profile_Repository__c(IC_Benchmark_Portfolio_Repository__c = bpToInsert.get(bpId).Id, Model_Portfolio__c = brp.Model_Portfolio__c));
                }
            }
        }
        
        if(brpToInsert.size()>0) {
            insert brpToInsert.values();
        }
        
        for(Id bpId: brpPerBP.keySet()) {
            for(Benchmark_Risk_Profile__c brp: brpPerBP.get(bpId)) {
                for(Benchmark_Risk_Profile_Detail__c brpd: brp.Benchmark_Risk_Profile_Details__r) {
                    brpdToInsert.add(new IC_Benchmark_in_Risk_Profile_Repository__c(Benchmark__c = brpd.Benchmark__c,
                                                                                    Benchmark_Code__c = brpd.Benchmark_Code__c,
                                                                                    Current_Weight__c = brpd.Current_Weight__c,
                                                                                    Date__c = brpd.Date__c,
                                                                                    IC_Benchmark_Risk_Profile_Repository__c = brpToInsert.get(brp.Id).Id,
                                                                                    Included_in_Perf_Calc__c = brpd.Included_in_Perf_Calc__c,
                                                                                    Sector__c = brpd.Sector__c));
                }
            }
        }
        
        if(brpdToInsert.size()>0) {
            insert brpdToInsert;
        }
        
        for(Id mpId: frpPerMP.keySet()) {
            for(Fund_Risk_Profile__c frp: frpPerMP.get(mpId)) {
                if(mpToInsert.containsKey(mpId) && brpToInsert.containsKey(frp.Benchmark_Risk_Profile__c)) {
                    frpToInsert.put(frp.Id, new IC_Fund_Risk_Profile_Repository__c(IC_Model_Portfolio__c = mpToInsert.get(mpId).Id,
                                                                                   IC_Benchmark_Risk_Profile_Repository__c = brpToInsert.get(frp.Benchmark_Risk_Profile__c).Id));
                }
            }
        }
        
        if(frpToInsert.size()>0) {
            insert frpToInsert.values();
        }
        
        for(Id mpId: frpPerMP.keySet()) {
            for(Fund_Risk_Profile__c frp: frpPerMP.get(mpId)) {
                for(Fund_Risk_Profile_Detail__c frpd: frp.Fund_Risk_Profile_Details__r) {
                    frpdToInsert.add(new IC_Fund_in_Risk_Profile_Repository__c(Current_Weight__c = frpd.Current_Weight__c,
                                                                                    Date__c = frpd.Date__c,
                                                                                    Fund__c = frpd.Fund__c,
                                                                                    Included_in_Perf_Calc__c = frpd.Included_in_Perf_Calc__c,
                                                                                    Investment_Code__c = frpd.Investment_Code__c,
                                                                                    IC_Fund_Risk_Profile_Repository__c = frpToInsert.get(frp.Id).Id));
                }
            }
        }
        
        if(frpdToInsert.size()>0) {
            insert frpdToInsert;
        }
        
        for(Id mpId: anPerMP.keySet()) {
            for(Analysis__c an: anPerMP.get(mpId)) {
                if(anPerMP.containsKey(mpId)) {
                    anToInsert.put(an.Id, new IC_Analysis_Repository__c(IC_Model_Portfolio__c = mpToInsert.get(mpId).Id,
                                                                        Sector__c = an.Sector__c,
                                                                        Comment__c = an.Comment__c));
                }
            }
        }
        
        if(anToInsert.size()>0) {
            insert anToInsert.values();
        }
        
        for(Id mpId: anPerMP.keySet()) {
            for(Analysis__c an: anPerMP.get(mpId)) {
                for(Fund_Commentaries__c fc: an.Fund_Commentaries1__r) {
                    fcToInsert.add(new IC_Fund_Commentary_Repository__c(IC_Analysis_Repository__c = anToInsert.get(an.Id).Id,
                                                                        Comment__c = fc.Commentary__c,
                                                                        Fund__c = fc.Fund__c));
                }
            }
        }
        
        if(fcToInsert.size()>0) {
            insert fcToInsert;
        }
        
    }
    
    public static void holdsModelIdSet(Set<Id> mpIds){

        List<IC_Model_Portfolio__c> icMportfolioToExisting = new List<IC_Model_Portfolio__c>();
        for(IC_Model_Portfolio__c icMport: [select  Id,
                                                    Model_Portfolio__c,
                                                    Date_of_Creation__c
                                            from    IC_Model_Portfolio__c
                                            where   Model_Portfolio__c IN :mpIds
                                                    AND Date_of_Creation__c >= TODAY ]){
    
            icMportfolioToExisting.add(icMport);
        }
        
        if(icMportfolioToExisting.size()>0) {
            delete icMportfolioToExisting;
        }

    }

}