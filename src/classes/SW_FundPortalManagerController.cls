public with sharing class SW_FundPortalManagerController {
	
	String thisID = '';
	
	public String savedFundName {get;set;}
	public String isSaved {get;set;}
	public String isComplete {get;set;}
	Boolean isUpToDateQuarterUpdate {get;set;}		// this is true if the latest quarter update is for the current quarter
	Quarter_Update__c latestQuarterUpdate {get;set;}// this contains the latest saved Quarter update of the fund
	String originalCommentary {get;set;}
	
	Date cutOffDate {get;set;}
	public Boolean hasPassed {get; set;}
	
	public String selectedFund {get;set;}
	//public String selectedFundsHeader {get;set;}
	public String selectedQuarterYear {get; set;}
	public List<Fund__c> fundList {get;set;}
	Map<Id,Id> fundIDSectorIDMap = new Map<Id,Id>();
	Map<Id,Template_Header__c> fundIdTHMap = new Map<Id,Template_Header__c>();
	public List<FundWrapper> fwList {get;set;}
	//public Map<String,TemplateDetailWrapper> templateDetailNameWrapperMap {get;set;}
	Map<Id,fundWrapper> fundIDFundWrapperMap = new Map<Id,fundWrapper>();
	public FundWrapper fw {get;set;}
	public String currentQuarter {get;set;}
	public Quarter_Update__c qu {get;set;}
	public List<TemplateDetailWrapper> templateDetailWrapperList {get;set;}
	public Map<Id, Quarter_Update__c> fundIdLatestQuarterUpdateMap {get;set;}
	List<Quarter_Update__c> quarterUpdateList = new List<Quarter_Update__c>();
	Map<Id,Quarter_Update__c> quIdQuarterUpdateMap = new  Map<Id,Quarter_Update__c>();
	List<Quarter_Update__c> fundAllQuarterUpdatelist = new List<Quarter_Update__c>();
	Id loggedInAccountId;
	
	public List<SelectOption> QuarterYearPickListOpts {get;set;}
	public Boolean renderQuarterYearOpts {get; set;}
	public Boolean renderNoQuarterRcrds {get; set;}
	// START FOR PAGINATION
	final static Integer MAX_REC = 20;
	public String selectedPageNum {get;set;}
	public Integer totalPageNum {get;set;}
	public Map<Integer, List<fundWrapper>> mainList {get;set;}
	public List<fundWrapper> listToDisplay {get;set;}
	public List<SelectOption> pageNumOptions {get;set;}
	
	public PageReference getRefreshList(){
		listToDisplay = mainList.get(Integer.valueOf(selectedPageNum));
		return null;
	}
	
	public List<SelectOption> getPageNumbers(){
		pageNumOptions = new List<SelectOption>();
		for (Integer i=1 ; i<=totalPageNum ; i++){
			pageNumOptions.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
		}
		return pageNumOptions;
	}
	// END FOR PAGINATION
	
	public void retrieveUserAccountId(){
		//User currUser = [select AccountId, Id from User WHERE Id = '005N0000000GSzW'];
		User currUser = [select AccountId, Id from User WHERE Id = : UserInfo.getUserId()];
		loggedInAccountId = currUser.AccountId;
	}
	public void retrieveClientCutoffDate(){
		if(loggedInAccountId!=null){
			Account currAcct = [select Cut_Off_Date__c, Id from Account WHERE Id = : loggedInAccountId];
			cutOffDate = currAcct.Cut_Off_Date__c;
		}
		else{
			cutOffDate = Date.today().addDays(1);
		}
	}
	
	
	public SW_FundPortalManagerController(){
		initialize();
		savedFundName = (ApexPages.currentPage().getParameters().get('fName')!=null) ? ApexPages.currentPage().getParameters().get('fName') : '';
		isSaved = (ApexPages.currentPage().getParameters().get('isSaved')!=null) ? ApexPages.currentPage().getParameters().get('isSaved') : 'false';
		isComplete = (ApexPages.currentPage().getParameters().get('isComplete')!=null) ? ApexPages.currentPage().getParameters().get('isComplete') : 'false';
		
	}
	
	public void initialize(){
		QuarterYearPickListOpts = new List<SelectOption>();
	  
		
		mainList = new Map<Integer, List<fundWrapper>>();
		pageNumOptions = new List<SelectOption>();
		selectedPageNum = '1';
		totalPageNum = 1;
		
		retrieveUserAccountId();
		retrieveClientCutoffDate();
		isUpToDateQuarterUpdate = false;
		latestQuarterUpdate = new Quarter_Update__c();
		originalCommentary = '';
		
		fundIdLatestQuarterUpdateMap = new Map<Id, Quarter_Update__c>();
		selectedFund = (ApexPages.currentPage().getParameters().get('firstParam')!=null) ? ApexPages.currentPage().getParameters().get('firstParam') : '';
		//selectedFundsHeader = '';
		qu = new Quarter_Update__c();
		currentQuarter = getCurrentQuarterString();
		fw = new FundWrapper('','','','','','');
		fwList = new List<FundWrapper>();
		//templateDetailNameWrapperMap = new Map<String,TemplateDetailWrapper>();
		templateDetailWrapperList = new List<templateDetailWrapper>();
		
		//query fund fields and set to map/list
		fundList = [SELECT Id, Name, Manager__c, Sector__c, Sector__r.Name, Model_Portfolio__c, APIR_Code__c, Active_Benchmark_Name__c,
					(Select Fund__c, APIR_Code__c from Tax_Structures__r),
					(SELECT Id, Commentary__c, Name, Status__c, Fund__c, Quarter__c, Template_Detail_IDs__c, Template_Detail_Value__c, Template_Header__c, Year__c FROM Quarter_Updates__r Order by Quarter__c DESC, Year__c DESC) 
					//FROM Fund__c Order by Name];
					FROM Fund__c Where Manager__c = :loggedInAccountId Order by Name];
		
		Map<Id, Set<Id>> sectorIDFundIdSetMap = new Map<Id, Set<Id>>();
		
		if(cutOffDate >= Date.today())hasPassed = false;
		else hasPassed = true;
		
		for(Fund__c fund: fundList){	
			
			// create the fund wrapper for each fund
			//get the APIR codes from tax structure
			String tempAPIR = '';
			for(Tax_Structure__c ts : fund.Tax_Structures__r){
				if (ts.APIR_Code__c != null && ts.APIR_Code__c != ''){
					if (tempAPIR!=''){
						tempAPIR += ', ' + ts.APIR_Code__c;
					}else{
						tempAPIR = ts.APIR_Code__c;
					}
				}
			}
			
			// get Status from Last Quarter update record
			Boolean isCurrent = false;
			String tempStatus = '';
			for (Quarter_Update__c qu : fund.Quarter_Updates__r){
				
				// add checking to verify if it is for the current quarter
				
				if(qu.Quarter__c == getCurrentQuarterString() && qu.Year__c == (String.valueOf(SW_FundPortalManagerController.currentYear()))){
					 isCurrent = true;
					 System.Debug('qu.Quarter__c: ' + qu.Quarter__c);
					 System.Debug('qu.Year__c: ' + qu.Year__c);
				}
				else isCurrent = false;  
								
				if (isCurrent){
					fundIdLatestQuarterUpdateMap.put(fund.Id,qu);
					tempStatus = qu.Status__c;
					//break;
				}
				else quarterUpdateList.add(qu); //get all quarter updates except current
			}
			fundWrapper tempFW = new FundWrapper(fund.Sector__r.Name +' -> '+ fund.Name, fund.Id, tempAPIR, tempStatus, fund.Model_Portfolio__c, fund.Active_Benchmark_Name__c);
			fwList.add(tempFW);
			fundIDFundWrapperMap.put(fund.Id,tempFW);
			
			// get templates for each fund depending on their sector
			fundIDSectorIDMap.put(fund.Id,fund.Sector__c);	
			
			Set<Id> tempFundIdSet = new Set<Id>();
			if (sectorIDFundIdSetMap.containsKey(fund.Sector__c)){
				tempFundIdSet = sectorIDFundIdSetMap.get(fund.Sector__c);
			}
			tempFundIdSet.add(fund.Id);
			sectorIDFundIdSetMap.put(fund.Sector__c,tempFundIdSet);
		}
		
		// SET THE MAP FOR THE PAGINATION
		Integer pageNum = 1;
		list<Fundwrapper> tempList = new List<fundWrapper>();
		for (fundWrapper fw : fwList){
			if (tempList.size() == MAX_REC){
				pageNum++;
				tempList = new list<Fundwrapper>();
				tempList.add(fw);
			}else{
				tempList.add(fw);
			}
			mainList.put(pageNum,tempList);
		}
		totalPageNum = pageNum;
		listToDisplay = mainList.get(1);
		// END SETTING MAP FOR THE PAGINATION
		
		//Map<Id, Template_Header__c> fundIDTHMap = new Map<Id,Template_Header__c>();
		//query templateHeader and template details
		List<Template_Header__c>templateHeaderList = [select Name, Active__c, Id, Sector__c, Commentary__c, (select Id, Name, Sequence__c, Template_Header__c from Template_Managers__r order by Sequence__c ASC) from Template_Header__c where Sector__c IN :fundIDSectorIDMap.values() AND Active__c = 'Yes' order by Sector__c];
		for (Template_Header__c th : templateHeaderList){
			for(Id fundId : sectorIDFundIdSetMap.get(th.Sector__c)){
				fundIDTHMap.put(fundId,th);
			}
		}
	}
	
	//action Link
	public PageReference showDetails(){
		if (selectedFund!=''){
			savedFundName = '';
			isSaved = 'false';
			isComplete = 'false';
		}
		fw = new FundWrapper('','','','','','');
		selectedFund = ApexPages.currentPage().getParameters().get('firstParam');
		renderQuarterYearOpts = false;
	    renderNoQuarterRcrds  = false;
	    
		templateDetailWrapperList.clear();
		quIdQuarterUpdateMap.clear();
		fundAllQuarterUpdatelist.clear();
		getLastQuarterUpdate();
		if(!quarterUpdateList.isEmpty()){
			for (Quarter_Update__c tempQU : quarterUpdateList){
				if(tempQU.Fund__c == selectedFund){
					quIdQuarterUpdateMap.put(tempQU.Id,tempQU);
					fundAllQuarterUpdatelist.add(tempQu);	
				}
			}
		}
		if(fundAllQuarterUpdatelist.size()>0) renderQuarterYearOpts = true;
		else renderNoQuarterRcrds  = true;
		
		if (fundIdLatestQuarterUpdateMap.containsKey(selectedFund)){
			Quarter_Update__c tempQU = fundIdLatestQuarterUpdateMap.get(selectedFund);
			if (tempQU.Quarter__c == getCurrentQuarterString() && tempQU.Year__c == String.valueOf(date.today().year())){
				isUpToDateQuarterUpdate = true;
				qu = tempQU;
				qu.Commentary__c = tempQU.Commentary__c;
				qu.Template_Detail_IDs__c = tempQU.Template_Detail_IDs__c;
				qu.Template_Detail_Value__c = tempQU.Template_Detail_Value__c;
			}else{
				qu.Template_Detail_IDs__c = '';//tempQU.Template_Detail_IDs__c;
				qu.Template_Detail_Value__c = '';//tempQU.Template_Detail_Value__c;
			}
		}else{
			qu = new Quarter_Update__c();
			templateDetailWrapperList = new List<templateDetailWrapper>();
		}
		if(fundIDTHMap.containsKey(selectedFund)){
			Template_Header__c th = fundIDTHMap.get(selectedFund);
			if (isUpToDateQuarterUpdate){
				
				if(qu.Status__c != 'Pending'){
					if (qu.Template_Detail_IDs__c==null)	qu.Template_Detail_IDs__c = '';
					if (qu.Template_Detail_Value__c==null)	qu.Template_Detail_Value__c = '';
					String [] tempDetailNames = qu.Template_Detail_IDs__c.split(';');
					String [] tempDetailValues = qu.Template_Detail_Value__c.split(';');
					System.debug('===tempDetailNames: ' + tempDetailNames);
					
					for(Integer i=0; i<tempDetailNames.size(); i++){
						String val = ''; 
						if (tempDetailValues.size()>i)
							val = tempDetailValues[i].trim();
						else val = '';
						//if (tempDetailValues[i]==null)	tempDetailValues[i] = '';
						templateDetailWrapperList.add(new templateDetailWrapper(tempDetailNames[i].trim(), val));
					}
				}else{// create fields since status is Pending
					if(th.Template_Managers__r.size()>0){
						for (Template_Detail__c td : th.Template_Managers__r){
							templateDetailWrapperList.add(new templateDetailWrapper(td.Name.trim(), ''));
						}
					}else{
						templateDetailWrapperList = new List<templateDetailWrapper>();
					}
				}
			}else{
				if(th.Template_Managers__r.size()>0){
					for (Template_Detail__c td : th.Template_Managers__r){
						//templateDetailNameWrapperMap.put(td.Name,new templateDetailWrapper(td.Name));
						templateDetailWrapperList.add(new templateDetailWrapper(td.Name.trim(), ''));
					}
				}else{
					templateDetailWrapperList = new List<templateDetailWrapper>();
				}
			}
			originalCommentary = th.Commentary__c;
			if (qu.Commentary__c==null) qu.Commentary__c = th.Commentary__c;
		}else{
			qu = new Quarter_Update__c();
			templateDetailWrapperList = new List<templateDetailWrapper>();
		}
		
		fw = fundIDFundWrapperMap.get(selectedFund);

		return null;
	}
	
	
	
	public List<SelectOption> getQuarterYearPickListOptions(){
		QuarterYearPickListOpts = new List<SelectOption>();
		if(fundAllQuarterUpdatelist.size() > 0){
			for(Quarter_Update__c qu : fundAllQuarterUpdatelist ){
      			QuarterYearPickListOpts.add(new SelectOption(qu.Id, qu.Quarter__c + ' ' + qu.Year__c));
			}
		}
   	
		return QuarterYearPickListOpts;  
		
	}
	
	public PageReference copyFromQuarterYear(){
		
		if(selectedQuarterYear !='' && selectedQuarterYear != null){
			Quarter_Update__c tempQU = new Quarter_Update__c();
			tempQU=quIdQuarterUpdateMap.get(selectedQuarterYear);
			Template_Header__c th = fundIDTHMap.get(selectedFund);
			//List<templateDetailWrapper> tempTemplateDetailWrapperList = new List<templateDetailWrapper>(); //delete 
		
			//if(tempQU.Template_Header__c == th.Id){ // commented out to remove restriction of copying from different template headers
				String [] tempDetailNames = tempQU.Template_Detail_IDs__c.split('; ');
				String [] tempDetailValues = tempQU.Template_Detail_Value__c.split('; ');
				Map<String,String> dNamesDvaluesMap = new Map<String,String>();
				
				for(Integer i=0;i<tempDetailNames.size();i++){
					dNamesDvaluesMap.put(tempDetailNames[i],tempDetailValues[i]);
				}
				
				for(Integer i=0;i<templateDetailWrapperList.size();i++){
					if(dNamesDvaluesMap.containsKey(templateDetailWrapperList[i].detailName)){
						templateDetailWrapperList[i].detailValue = dNamesDvaluesMap.get(templateDetailWrapperList[i].detailName);
					}
				}
			/*	
			}
			else{
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Cannot copy from selected Quarter and Year. Template is not the same.'));
			}*/
				
		}
		
		return null;
		
	}
	
	public PageReference save(){
		
		if (selectedFund == ''){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Select a fund first.'));
			return null;
		}
		if (!fundIDTHMap.containsKey(selectedFund)){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'There is no Template for this fund yet.'));
			return null;
		}
		if (templateDetailWrapperList.size()<1){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'There is no Field for the template of this fund yet.'));
			return null;
		}
		
		String templateDetailFieldNames = '';
		String templateDetailFieldValues = '';
		String commentary = originalCommentary;
		String status = '';
		String quarter = getcurrentQuarterString();
		String year = String.valueOf(date.today().year());
		
		Boolean isComplete 	= true;
		Boolean isNull		= true;
		
		if (String.isNotBlank(commentary)){
			commentary = commentary.replaceAll('\\[FundName\\]',fw.fundname);
			commentary = commentary.replaceAll('\\[Benchmark\\]',fw.benchmark);
			commentary = commentary.replaceAll('\\[Quarter\\]',quarter + ' ' + year);
		}else	commentary = '';
		
		
		for (templateDetailWrapper tdw : templateDetailWrapperList){
			
			if (tdw.detailValue!=''){
				isNull = false;
				commentary = commentary.replaceAll('\\['+tdw.detailName+'\\]',''+tdw.detailValue+'');
				templateDetailFieldValues += (templateDetailFieldValues!='') 	? '; ' + tdw.detailValue : tdw.detailValue;
			}else{
				isComplete = false;
				commentary = commentary.replaceAll('\\['+tdw.detailName+'\\]','');
				templateDetailFieldValues += (templateDetailFieldValues!='') 	? '; ' : ' ';
			}
			templateDetailFieldNames += (templateDetailFieldNames!='') 		? '; ' + tdw.detailName 		: tdw.detailName;
			
		}
		
		if (!isNull && !isComplete)		status = 'Partially Completed';
		else if (isNull)				status = 'Pending';
		else if (isComplete)			status = 'Complete';
		
		qu.Fund__c = selectedFund;
		qu.Status__c = status;
		qu.Template_Detail_IDs__c = templateDetailFieldNames;
		qu.Template_Detail_Value__c = templateDetailFieldValues;
		qu.Commentary__c = commentary;
		qu.Template_Header__c = fundIDTHMap.get(selectedFund).Id;
		qu.Quarter__c = quarter;
		qu.Year__c = year;
		
		upsert qu;
		/*thisID = selectedFund;
		initialize();
		ApexPages.currentPage().getParameters().put('firstParam',thisID);
		showDetails();
		getQuarterYearPickListOptions();
		ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO,'Record was successfully saved.'));
		*/
		
		PageReference newRef = Page.SW_FundPortalManager;
		newRef.getParameters().put('fName', fw.fundname);
		newRef.getParameters().put('isSaved', 'true');
		if (status=='Complete')	newRef.getParameters().put('isComplete', 'true');
		newRef.setRedirect(true);
		return newRef;
		//return null;
	}

	public PageReference submitForApproval(){
		try{
		
		// Start next approval process
		Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
		request.setObjectId(qu.Id);
		request.setComments(qu.Commentary__c);
		
		// make all research analysts be approvers of the quarter update record
		/*Profile researchAnalystProfile = [select Id from Profile where Name = 'Research Analyst'];
		List<String> approverList = new List<String>();
		for(User researchAnalyst : [select Id from User where ProfileId = :researchAnalystProfile.Id and isActive = true]){
			approverList.add(researchAnalyst.Id);
		}
		
		approverList.add(UserInfo.getuserId()); // this should be removed (for testing only)
		request.setNextApproverIds(approverList);
		*/
		Approval.ProcessResult requestResult = Approval.process(request);
		initialize();
		ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO,'Record was successfully submitted for approval. Please refresh the page for updates.'));
		}catch(Exception e){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage() + e.getCause() + e.getLineNumber()));
		}
		return null;
	}
	
	public String lquName {get;set;}
	public String lquId {get;set;}
	
	public Quarter_Update__c getLastQuarterUpdate(){
		Quarter_Update__c latestQU = new Quarter_Update__c();
		if (fundIdLatestQuarterUpdateMap.containsKey(selectedFund)){
			latestQU = fundIdLatestQuarterUpdateMap.get(selectedFund);
			lquName = (latestQU.Name!=null) ? latestQU.Name : '';
			lquId = (latestQU.Id!=null) ? latestQU.Id : '';
		}else{
			lquName = '';
			lquId = '';
		}
		return latestQU;
	}
	
	public String getCurrentQuarterString(){
		Integer currentMonth = Date.today().month();
		if (currentMonth < 4)								currentQuarter = 'Jan-Mar';
		else if (currentMonth >= 4 && currentMonth < 7)		currentQuarter = 'Apr-Jun';
		else if (currentMonth >= 7 && currentMonth < 10)	currentQuarter = 'Jul-Sep';
		else												currentQuarter = 'Oct-Dec';
		return currentQuarter;
	}
	
	public static Integer currentYear(){
		Date currentDate = Date.today();
		return currentDate.year();
	}
	
	public class FundWrapper{
		
		public String fundName {get; set;}
		public String APIR_Code {get; set;}
		public String fundId {get;set;}  
		public String status {get;set;}
		public String isModel {get;set;}
		public String benchmark {get;set;}
		
		public QuarterUpdateWrapper fundQuarterUpdateWrapper {get; set;}
		
		public FundWrapper(String pFundName, String pFundId, String pAPIRCode, String pStatus, String pModel, String pBenchmark){
			fundName = pFundName;
			APIR_Code = pAPIRCode;
			fundId = pFundId;
			status = pStatus;
			isModel = pModel;
			benchmark = (pBenchmark!=null) ? pBenchmark : 'no benchmark';
		}
	}
	
	public class QuarterUpdateWrapper{
		public Quarter_Update__c  qu {get; set;}
		
		public QuarterUpdateWrapper(Quarter_Update__c  qu ){
			this.qu = qu;
			 
		}
	}
	
	public class TemplateHeaderWrapper{
		public Template_Header__c th {get; set;}
		
		public List<templateDetailWrapper> templateDetailWrapperList {get;set;}
		public TemplateHeaderWrapper(Template_Header__c th, List<templateDetailWrapper> pTemplateDetailWrapperList){
			this.th = th;
			templateDetailWrapperList = pTemplateDetailWrapperList;
		}
	}
	
	public class TemplateDetailWrapper{
		public String detailName {get;set;}
		public String detailValue {get;set;}
		
		public TemplateDetailWrapper(String pDetailName, String pDetailValue){
			detailName = pDetailName;
			detailValue = pDetailValue;
		}
		
	}

}