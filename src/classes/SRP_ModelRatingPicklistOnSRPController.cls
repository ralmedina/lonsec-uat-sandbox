public class SRP_ModelRatingPicklistOnSRPController {
	
	public String srpId {get;set;}
	//public Sector_Review_Plan__c srp {get;set;}
	public String debugString {get;set;}

	public SRP_ModelRatingPicklistOnSRPController(ApexPages.StandardController controller){
		srpId = ApexPages.currentPage().getParameters().get('Id');
	}
	
	public PageReference runAutoFill(){
		List<Covered_Fund__c> cfList = 	[select Id, Normalized_Fund_Score__c, Model_Rating__c
										from Covered_Fund__c 
										where Sector_Review_Plan_Record__c = :srpId];
		
		for (Covered_Fund__c cf : cfList){
			if (cf.Normalized_Fund_Score__c > 85 )												cf.Model_Rating__c = 'HR';
			else if (cf.Normalized_Fund_Score__c <= 85 	&& cf.Normalized_Fund_Score__c > 75)	cf.Model_Rating__c = 'R';
			else if (cf.Normalized_Fund_Score__c <= 75 	&& cf.Normalized_Fund_Score__c > 65)	cf.Model_Rating__c = 'IG';
			else if (cf.Normalized_Fund_Score__c <= 65 	&& cf.Normalized_Fund_Score__c > 55)	cf.Model_Rating__c = 'H';
			else if (cf.Normalized_Fund_Score__c <= 55 )										cf.Model_Rating__c = 'Rd';
		}
		if (cfList.size()>0)	update cfList; // this updates the SRP funds with the correct model rating
		
		return redirectPage();
	}
	
	public PageReference redirectPage(){
		return new PageReference('/../'+srpId);
	}
	
}