@isTest(seeAllData=true)
Public class DocumentRepositoryListControllerTest{
   public static testMethod void testRunAs() {
      // Setup test data
      // This code runs as the system user
       List<User> userTest = new List<User>([SELECT Id, ContactId, AccountId, Name FROM User WHERE Name =: 'High Volume']);

      System.runAs(userTest[0]) {
        Test.StartTest();
        Sector__c sector = new Sector__c();
        sector.Name = 'Sample Sector';
        insert sector;
        
        Fund__c fund = new Fund__c();
        fund.Name = 'Sample Fund';
        fund.Sector__c = sector.Id;
        insert fund;
        
        Package_Repository__c PackageRep = new Package_Repository__c();
        packageRep.Name = 'Sample Package Repository';
        insert PackageRep;
        
        Document_Repository__c Document = new Document_Repository__c();
        Document.Name = 'Sample Document';
        Document.Sector__c = sector.Id;
        Document.Package_Repository__c = packageRep.Id;
        Document.Fund__c = fund.Id;
        Document.Document_Type__c = 'Fund Review';
        Document.Author__c = 'Sample Author';
        insert Document;

        Account_package__c accpack = new Account_Package__c();
        accpack.Client__c = userTest[0].accountId;
        accpack.Package__c = packagerep.Id;
        insert accpack;
        
        Contact_Package__c contactPackage = new Contact_Package__c();
        contactPackage.Contact__c = userTest[0].contactId;
        contactPackage.Package__c = PackageRep.Id;
        insert contactPackage;
        
        Attachment t = new Attachment();
        t.parentId = Document.Id;
        t.Body = Blob.valueOf('Test Attachment');
        t.Name = 'TestName';
        insert t;
        
        Package_Document__c PackageDocument = new Package_Document__c();
        PackageDocument.Package_Repository__c = PackageRep.Id;
        PackageDocument.Document__c = Document.Id;
        insert PackageDocument;
        
        DocumentRepositoryListController drlc = new DocumentRepositoryListController();
        drlc.clearFilter();
        drlc.getDocRep();
        drlc.getdocRepFunds();
        drlc.getdocRepSector();
        drlc.getdocRepAuthor();
        drlc.docDownloadId = t.Id;
        drlc.kickDownload();
        
        drlc.docRepFundString = '--Choose Funds--';
        drlc.docRepSectorString = '--Choose Sector--';
        drlc.docRepAuthorString = '--Choose Author--';
        drlc.docRepDocTypeString ='Fund Review';
        
        drlc.getDocumentList();

        drlc.docRepFundString = 'Sample Fund';
        drlc.docRepSectorString = '--Choose Sector--';
        drlc.docRepAuthorString = '--Choose Author--';
        drlc.docRepDocTypeString ='Fund Review';        
        
        drlc.getDocumentList();
        
        drlc.docRepFundString = 'Sample Fund';
        drlc.docRepSectorString = 'Sample Sector';
        drlc.docRepAuthorString = '--Choose Author--';
        drlc.docRepDocTypeString ='Fund Review';        
        
        drlc.getDocumentList();
        
        drlc.docRepFundString = 'Sample Fund';
        drlc.docRepSectorString = 'Sample Sector';
        drlc.docRepAuthorString = 'Sample Author';
        drlc.docRepDocTypeString ='Fund Review';        
        
        drlc.getDocumentList();
        
        drlc.docRepFundString = '--Choose Funds--';
        drlc.docRepSectorString = 'Sample Sector';
        drlc.docRepAuthorString = '--Choose Author--';
        drlc.docRepDocTypeString ='Fund Review';        
        
        drlc.getDocumentList();
        
        drlc.docRepFundString = 'Sample Fund';
        drlc.docRepSectorString = '--Choose Sector--';
        drlc.docRepAuthorString = '--Choose Author--';
        drlc.docRepDocTypeString ='--Choose Document Type--';
        
        drlc.getDocumentList();
        
        drlc.docRepFundString = 'Sample Fund';
        drlc.docRepSectorString = 'Sample Sector';
        drlc.docRepAuthorString = '--Choose Author--';
        drlc.docRepDocTypeString ='--Choose Document Type--';
        
        drlc.getDocumentList();
        
        drlc.docRepFundString = 'Sample Fund';
        drlc.docRepSectorString = 'Sample Sector';
        drlc.docRepAuthorString = 'Sample Author';
        drlc.docRepDocTypeString ='--Choose Document Type--';
        
        drlc.getDocumentList();
        
        drlc.docRepFundString = 'Sample Fund';
        drlc.docRepSectorString = '--Choose Sector--';
        drlc.docRepAuthorString = 'Sample Author';
        drlc.docRepDocTypeString ='--Choose Document Type--';
        
        drlc.getDocumentList();
        
        drlc.docRepFundString = '--Choose Funds--';
        drlc.docRepSectorString = 'Sample Sector';
        drlc.docRepAuthorString = '--Choose Author--';
        drlc.docRepDocTypeString ='--Choose Document Type--';
        
        drlc.getDocumentList();
        
        drlc.docRepFundString = '--Choose Funds--';
        drlc.docRepSectorString = 'Sample Sector';
        drlc.docRepAuthorString = 'Sample Author';
        drlc.docRepDocTypeString ='--Choose Document Type--';
        
        drlc.getDocumentList();
        
        drlc.docRepFundString = '--Choose Funds--';
        drlc.docRepSectorString = 'Sample Sector';
        drlc.docRepAuthorString = 'Sample Author';
        drlc.docRepDocTypeString = 'Fund Review';
        
        drlc.getDocumentList();
        
        drlc.docRepFundString = '--Choose Funds--';
        drlc.docRepSectorString = '--Choose Sector--';
        drlc.docRepAuthorString = 'Sample Author';
        drlc.docRepDocTypeString ='--Choose Document Type--';
        
        drlc.getDocumentList();
        
        drlc.docRepFundString = '--Choose Funds--';
        drlc.docRepSectorString = '--Choose Sector--';
        drlc.docRepAuthorString = 'Sample Author';
        drlc.docRepDocTypeString ='Fund Review';
        
        drlc.getDocumentList();
        
        Test.StopTest();
      }
   }
}