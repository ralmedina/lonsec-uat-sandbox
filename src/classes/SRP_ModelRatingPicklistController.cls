public class SRP_ModelRatingPicklistController {
	
	public String fId {get;set;}
	public Fund__c f {get;set;}
	public String debugString {get;set;}

	public SRP_ModelRatingPicklistController(ApexPages.StandardController controller){
		fId = ApexPages.currentPage().getParameters().get('Id');
	}
	
	public PageReference runAutoFill(){
		f = 	[select Id, Normalized_Fund_Score__c, Model_Rating__c
				from Fund__c 
				where id = :fId];
		
		if (f.Normalized_Fund_Score__c > 85 )												f.Model_Rating__c = 'HR';
		else if (f.Normalized_Fund_Score__c <= 85 	&& f.Normalized_Fund_Score__c > 75)	f.Model_Rating__c = 'R';
		else if (f.Normalized_Fund_Score__c <= 75 	&& f.Normalized_Fund_Score__c > 65)	f.Model_Rating__c = 'IG';
		else if (f.Normalized_Fund_Score__c <= 65 	&& f.Normalized_Fund_Score__c > 55)	f.Model_Rating__c = 'H';
		else if (f.Normalized_Fund_Score__c <= 55 )										f.Model_Rating__c = 'Rd';
		
		update f; // this updates the fund with the correct model rating
		
		return redirectPage();
	}
	
	public PageReference redirectPage(){
		return new PageReference('/../'+fId);
	}
	
	static testMethod void test () {
        // Instantiate a new controller with all parameters in the page
        
        Sector__c s = Test_Util.createSector(null);
        insert s;
        
        List<Fund__c> fList = new List<Fund__c>();
        for (Integer i=0 ; i<20 ; i++){
        	Fund__c f = Test_Util.createFund(s.Id);
        	
        	fList.add(f);
        }
        insert fList;
        
        Contact c = new Contact(LastName='tc');
        insert c;
        
        Sector_Review_Plan__c srp = Test_Util.createSRP();
        insert srp;
        
        Included_Sector__c is = Test_Util.createIS(srp.Id, s.Id);
        insert is;
        
        Covered_Fund__c cf = Test_Util.createCF(srp.Id, fList[0].Id);
        insert cf;
        
        //User u = new User(LastName='r@test.com',Email='r@test.com');
        //insert u;
        
        Resource__c r = Test_Util.createResource(cf.Id,UserInfo.getUserId());
        insert r;
        
        
        test.startTest();
        PageReference pageRef1 = Page.SRP_ModelRatingPicklist;
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('id',fList[0].Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(fList[0]); 
        SRP_ModelRatingPicklistController controller = new SRP_ModelRatingPicklistController(ctr);
        controller.runAutoFill();
       	test.stopTest();
    }    
}