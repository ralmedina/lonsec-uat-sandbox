public class SRP_ManageFundsController {

	final static Integer MAX_REC = 5;
	public Integer indexToRemove {get;set;}
	public String selectedPageNum {get;set;}
	public Integer totalPageNum {get;set;}
	public Map<Integer, List<fundWrapper>> mainList {get;set;}
	public List<fundWrapper> listToDisplay {get;set;}
	public List<SelectOption> pageNumOptions {get;set;}

	public String srpID {get;set;}
	public Boolean showRated {get;set;}
	public String searchStr {get;set;}
	public String searchFilter {get;set;}
	public String searchFilterView {get;set;}
	public List<fundWrapper> fundList {get;set;}
	public String fundIdforResource {get;set;}
	public Sector_Review_Plan__c srp {get;set;}
	public Boolean selectAllFunds {get;set;}
	public Map<Id,Resource__c> resourceToDeleteMap = new Map<Id,Resource__c>();
	
	public PageReference getRefreshList(){
		listToDisplay = mainList.get(Integer.valueOf(selectedPageNum));
		return null;
	}
	
	public List<SelectOption> getPageNumbers(){
		pageNumOptions = new List<SelectOption>();
		for (Integer i=1 ; i<=totalPageNum ; i++){
			pageNumOptions.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
		}
		return pageNumOptions;
	}
	
	String isIDsStr  = '';
	
	Map<Id, Covered_Fund__c> cfFundMap = new Map<Id, Covered_Fund__c>();
	Map<Id, Covered_Fund__c> cfFundMap2 = new Map<Id, Covered_Fund__c>();
	List<Resource__c> oldResource {get;set;}
	
	
	Map<Id, List<resourceWrapper>> cfRWListMap = new Map<Id, List<resourceWrapper>>();
	//Map<Id, Covered_Fund__c> cfFundMap = new Map<Id, Covered_Fund__c>();
	
	
	public class resourceWrapper{
		public Integer index {get;set;}
		public Boolean isSelected {get;set;}
		public Resource__c r {get;set;}
		
		public resourceWrapper(Resource__c pR, Boolean pIsSelected, Integer pIndex){
			isSelected = pIsSelected;
			r = pR;
			index = pIndex;
		}
	}
	
	public class fundWrapper{
		transient public Integer rowNum {get;set;}
		transient public String fundName {get;set;}
		public Fund__c fund {get;set;}
		public Covered_Fund__c cfund {get;set;}
		//public List<Resource__c> cfResourceList {get;set;}
		public List<resourceWrapper> rwList {get;set;}
		//public Boolean isSelected {get;set;}
		
		public fundWrapper(Fund__c pFund, Covered_Fund__c pCFund, String psrpId, Integer pRowNum, List<resourceWrapper> pRWList){
			fundName 							= pFund.Sector__r.Name + ' -> ' + pFund.Name;
			fund 								= pFund;
			//isSelected 							= selected;
			cfund 								= pCFund;
			cfund.Fund__c 						= (fund.Id!=null) ? fund.Id : null;
			cfund.Sector_Review_Plan_Record__c 	= psrpId;
			rowNum 								= pRowNum;
			rwList								= pRWList;
			/*List<Resource__c> tempR = new List<Resource__c>();
			tempR.add(new Resource__c());
			cfResourceList						= (pCFund.Resources__r.size()>0) ? pCFund.Resources__r : tempR;*/
		}
	}
	
	public void addResource(){
		string myParam = apexpages.currentpage().getparameters().get('fId');
		for (fundWrapper fw : fundList){
			if (fw.fund.Id == myParam){
				//fw.rwList.add(new resourceWrapper(new Resource__c(),false));
				//fw.cfResourceList.add(new Resource__c());
				
				List<resourceWrapper> tempList = fw.rwList;
				if (tempList.size()>0)
					tempList.add(new resourceWrapper(new Resource__c(),false,tempList[tempList.size()-1].index+1));
				else
					tempList.add(new resourceWrapper(new Resource__c(),false,0));
				
				//tempList.add(new Key_Point__c());
				fw.rwList = tempList;
			}
		}
	}
	
	public PageReference removeResource(){
		string myParam = apexpages.currentpage().getparameters().get('fId');
		indexToRemove = Integer.valueOf(ApexPages.currentPage().getParameters().get('indexToRemove'));
		for (fundWrapper fw : fundList){
			if (fw.fund.Id == myParam){
				Integer i = 0;
				for (resourceWrapper rw : fw.rwList){
					if (rw.index == indexToRemove){
						
						break;
					}
					i++;
				}
				fw.rwList.remove(i);
				
				//Resource__c r = fw.rwList.get(indexToRemove).r;
				//if (r.Id!=null)	resourceToDeleteMap.put(r.Id,r);
				//fw.rwList.remove(indexToRemove);
				
				//fn.kpWrapperList = tempList;
				//activeFileNote = fn;
			}
		}
		return null;
	}
	
	public void selectAll(){
		if (selectAllFunds){
			for (fundWrapper fw : fundList){
				fw.fund.Included_in_SRP__c = true;
			}
		}
		else{
			for (fundWrapper fw : fundList){
				fw.fund.Included_in_SRP__c = false;
			}
		}
	}
	
	// Constructor
	public SRP_ManageFundsController(ApexPages.StandardController controller){
		
		pageNumOptions = new List<SelectOption>();
		selectedPageNum = '1';
		totalPageNum = 1;
		srp = (Sector_Review_Plan__c)controller.getRecord();
		srpID = ApexPages.currentPage().getParameters().get('id');
		searchStr = '';
		searchFilter = 'Sector';
		fundList = new List<fundWrapper>();
		oldResource = new List<ResourcE__c>();
		fundIdforResource = '';
		selectAllFunds = false;
		
		for (Included_Sector__c tIS : [select Sector__c  from Included_Sector__c 
										where Sector_Review_Plan_Record__c = :srpID ]){
			isIDsStr += (isIDsStr!='') ? ', \''+tIS.Sector__c+'\'' : '\''+tIS.Sector__c+'\'';
		}
		
		getFundList('initial');
	}
	
	public List<fundWrapper> getFundList(String stage){
		cfFundMap.clear();
		cfFundMap2.clear();
		String exisitingCFFunds = '';
		mainList = new Map<Integer, List<fundWrapper>>();
		// get all existing covered fund records under the sector
		for (Covered_Fund__c cf : [select Id, Fund__c, (select Id, SRP_Function__c, Covered_Fund__c, Resource_Name__c, Role__c from Resources__r) from Covered_Fund__c 
									where Sector_Review_Plan_Record__c = :srpID ]){
			cfFundMap.put(cf.Fund__c, cf);
			if (cf.Fund__c!=null) exisitingCFFunds += (exisitingCFFunds!='') ? ', \''+cf.Fund__c+'\'' : '\''+cf.Fund__c+'\'';
		}
		
		for (Covered_Fund__c cf : [select Id, Fund__c from Covered_Fund__c 
									where Sector_Review_Plan_Record__c = :srpID ]){
			cfFundMap2.put(cf.Fund__c, cf);
			if (cf.Fund__c!=null) exisitingCFFunds += (exisitingCFFunds!='') ? ', \''+cf.Fund__c+'\'' : '\''+cf.Fund__c+'\'';
		}
		
		String qryStr = 'select Included_in_SRP__c, Id, Name, Sector__r.Name, Manager__c, Current_Recommendation__c, Sector_Asset_Class__c from Fund__c';
		String condition = '';
		
		fundList = new List<fundWrapper>();
		
		if (stage=='initial') 	condition = ' where Sector__r.Name like \'%!@#!%\' OR Sector__c = \'' + srpID + '\'';
		else					condition = ' where Sector__r.Name like \'%' + searchStr + '%\' OR Sector__c = \'' + srpID + '\'';
			
		if (exisitingCFFunds!='')	condition += ' OR ID IN (' + exisitingCFFunds + ')';
		
		if (isIDsStr!=''){
			condition += (condition!='') ? ' OR ' : '';
			condition += ' Sector__r.Id IN (' + isIDsStr + ')';
		}		
		
		Map<Id, List<resourceWrapper>> cfRWListMap = new Map<Id, List<resourceWrapper>>();
		Integer ctr = 1;
		for (Fund__c f : database.query(qryStr+condition+' limit 1000')){
			if (cfFundMap.containsKey(f.Id)){
				List<resourceWrapper> tempRW = new List<resourceWrapper>();
				Integer rCtr = 0;
				for (Resource__c tr : cfFundMap.get(f.Id).Resources__r){
					tempRW.add(new resourceWrapper(tr,false,rCtr));
					rCtr++;
				}
				fundList.add(new fundWrapper(f,cfFundMap2.get(f.Id),srpID,ctr,tempRW));
			}
			else{// if (stage!='initial'){
				fundList.add(new fundWrapper(f,new Covered_Fund__c(),srpID,ctr,new List<resourceWrapper>()));
			}
			ctr++;
		}
		
		Integer pageNum = 1;
		list<Fundwrapper> tempList = new List<fundWrapper>();
		for (fundWrapper fw : fundList){
			if (tempList.size() == MAX_REC){
				pageNum++;
				tempList = new list<Fundwrapper>();
				tempList.add(fw);
			}else{
				tempList.add(fw);
			}
			mainList.put(pageNum,tempList);
		}
		totalPageNum = pageNum;
		listToDisplay = mainList.get(1);
		return fundList;
	}
	
	public void goSearch(){
		
		if (validate()) return;
		getFundList('');
	}
	
	public PageReference updateSRPFunds(){
	
		for (fundWrapper fw : fundList){
			if (fw.fund.Included_in_SRP__c){
				for (resourceWrapper r : fw.rwList)	{
					if (!r.isSelected){ 
						if (r.r.Resource_Name__c!=null){
							if (r.r.Role__c==null || r.r.Role__c==''){
								ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'All added resources must each have a role.'));
								return null;
							}
							if (r.r.SRP_Function__c==null || r.r.SRP_Function__c==''){
								ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'All added resources must each have a Fund Responsibility.'));
								return null;
							}
						}
					}
				}
			}
			else{
				for (resourceWrapper r : fw.rwList){
					if (r.r.Id!=null)	oldResource.add(r.r);
					
					if (!r.isSelected){ 
						if (r.r.Resource_Name__c!=null){
							//if (r.r.Role__c!=null && r.r.Role__c!='' && r.r.SRP_Function__c!=null && r.r.SRP_Function__c!=''){
								ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Error,'There are still resources allocated to fund/s not included in the SRP. Please remove them first.'));
								return null;
							//}
						}
					}
				}
			}
		}
	
		List<Covered_Fund__c> cfToInsert = new List<Covered_Fund__c>();
		List<Covered_Fund__c> cfToDelete = new List<Covered_Fund__c>();
		List<Fund__c> fundsToUpdate = new List<Fund__c>();
		
		for (fundWrapper fw : fundList){
			
			if (fw.fund.Included_in_SRP__c){
				cfToInsert.add(fw.cfund);
			}else{
				if (fw.fund.Id!=null)
					if (cfFundMap.containsKey(fw.fund.Id))	
						cfToDelete.add(fw.cfund);
			}
			fundsToUpdate.add(fw.fund);
		}
		
		if (cfToDelete.size()>0)	delete cfToDelete;
		if (cfToInsert.size()>0)	upsert cfToInsert;
		if (fundsToUpdate.size()>0)	update fundsToUpdate;
		
		// loop again for the resource
		
		List<Resource__c> cfrToInsert = new List<Resource__c>();
		for (fundWrapper fw : fundList){
			
			if (fw.fund.Included_in_SRP__c){
				cfToInsert.add(fw.cfund);
				
				for (resourceWrapper r : fw.rwList)	{
					if (!r.isSelected){ 
						if (r.r.Resource_Name__c!=null){
							r.r.Covered_Fund__c = fw.cfund.Id;
							cfrToInsert.add(r.r);
						}else {
							if (r.r.Id!=null) resourceToDeleteMap.put(r.r.Id,r.r);//oldResource.add(r.r);
						}
					}else {
						if (r.r.Id!=null) resourceToDeleteMap.put(r.r.Id,r.r);//oldResource.add(r.r);
					}
				}
			}
			else{
				for (resourceWrapper r : fw.rwList)	if (r.r.Id!=null)	resourceToDeleteMap.put(r.r.Id,r.r);//oldResource.add(r.r);
			}
		}
		//if (oldResource.size()>0) delete oldResource; // delete all existing Resources before creating new ones
		if (resourceToDeleteMap.size()>0)	delete resourceToDeleteMap.values();
		if (cfrToInsert.size()>0) upsert cfrToInsert;
		
		return new PageReference('/'+srpId);
		// insert logic that adds (converts funds to covered funds) and removes covered funds from the SRP record
	}
	
	
	
	public String classInputPostalCode {get;set;}
	public String errorMessage {get;set;}
	
	public Boolean validate() {
		if (searchStr==null || searchStr=='' || searchStr.length()<3){
        	classInputPostalCode = 'error';  // put the errorclass, red borders
        	errorMessage = 'Please input atleast 3 characters';
        	return true;
        } else {
        	classInputPostalCode = '';
        	errorMessage = '';
        	return false;
		}
	}
	
	static testMethod void test () {
        // Instantiate a new controller with all parameters in the page
        
        Sector__c s = Test_Util.createSector(null);
        insert s;
        
        List<Fund__c> fList = new List<Fund__c>();
        for (Integer i=0 ; i<20 ; i++){
        	Fund__c f = Test_Util.createFund(s.Id);
        	
        	fList.add(f);
        }
        insert fList;
        
        Contact c = new Contact(LastName='tc');
        insert c;
        
        Sector_Review_Plan__c srp = Test_Util.createSRP();
        insert srp;
        
        Included_Sector__c is = Test_Util.createIS(srp.Id, s.Id);
        insert is;
        
        Covered_Fund__c cf = Test_Util.createCF(srp.Id, fList[0].Id);
        insert cf;
        
        //User u = new User(LastName='r@test.com',Email='r@test.com');
        //insert u;
        
        Resource__c r = Test_Util.createResource(cf.Id,UserInfo.getUserId());
        insert r;
        
        
        test.startTest();
        PageReference pageRef1 = Page.SRP_ManageFunds;
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('id',srp.Id);
        ApexPages.StandardController ctr = new ApexPages.StandardController(srp); 
        SRP_ManageFundsController controller = new SRP_ManageFundsController(ctr);
        ApexPages.currentPage().getParameters().put('indexToRemove','0');
        controller.searchStr = 'test';
        controller.goSearch();
        controller.selectAll();
        controller.selectAllFunds = true;
        controller.selectAll();
        
        controller.indexToRemove = 1;
        
        ApexPages.currentPage().getParameters().put('fid',fList[0].Id);
        controller.addResource();
        
        controller.updateSRPFunds();
        for (fundWrapper fw : controller.fundList){
  			fw.fund.Included_in_SRP__c = true;
  			List<ResourceWrapper> rwList = new List<ResourceWrapper>();
  			rwList.add(new ResourceWrapper(r,true,1));
  			fw.rwList = rwList;
  		}
  		controller.updateSRPFunds();
  		controller.getPageNumbers();
  		controller.getRefreshList();
  		ApexPages.currentPage().getParameters().put('indexToRemove','0');
        controller.removeResource();
        
  		test.stopTest();
    }    
}