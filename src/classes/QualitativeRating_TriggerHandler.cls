public class QualitativeRating_TriggerHandler {

	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	public QualitativeRating_TriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	private void validateResource(Qualitative_Rating__c[] pQRList){
		
		Set<Id> cfIdSet = new Set<Id>();
		Map<Id,Covered_Fund__c> cfIdCFMap = new Map<Id,Covered_Fund__c>();
		
		for (Qualitative_Rating__c qr : pQRList){
			if (qr.Sector_Review_Fund__c!=null){
				cfIdSet.add(qr.Sector_Review_Fund__c);
			}
		}
		
		List<Covered_Fund__c> cfList = [select Id, Fund__c, (select Role__c, SRP_Function__c, Resource_Name__c from Resources__r) from Covered_Fund__c where Id in :cfIdSet];
		for (Covered_Fund__c cf : cfList){
			cfIdCFMap.put(cf.Id, cf);
		}
		
		for (Qualitative_Rating__c qr : pQRList){
			if (qr.Sector_Review_Fund__c!=null){
				Boolean isValid = false;
				Covered_Fund__c cf = cfIdCFMap.get(qr.Sector_Review_Fund__c);
				for (Resource__c r : cf.Resources__r){
					if (qr.OwnerId == r.Resource_Name__c && r.Role__c == 'Lead Analyst'){//.contains('Report Responsible')){
						isValid = true;
					}
				}
				//qr.Fund__c = cfIdCFMap.get(qr.Sector_Review_Fund__c).Fund__c;
				if (!isValid)	qr.Sector_Review_Fund__c.addError('Only Lead Analyst Resources are allowed to create Qualitative Ratings');
			}
		}
	}
	
	public void OnBeforeInsertUpdate(Qualitative_Rating__c[] pQRList){
		validateResource(pQRList);
	}
	
	static testMethod void test() {
        Sector__c s = Test_Util.createSector(null);
        insert s;
        
        List<Fund__c> fList = new List<Fund__c>();
        for (Integer i=0 ; i<20 ; i++){
        	Fund__c f = Test_Util.createFund(s.Id);
        	
        	fList.add(f);
        }
        insert fList;
        
        Contact c = new Contact(LastName='tc');
        insert c;
        
        Sector_Review_Plan__c srp = Test_Util.createSRP();
        insert srp;
        
        Included_Sector__c is = Test_Util.createIS(srp.Id, s.Id);
        insert is;
        
        Covered_Fund__c cf = Test_Util.createCF(srp.Id, fList[0].Id);
        insert cf;
        
        Covered_Fund__c cf2 = Test_Util.createCF(srp.Id, fList[1].Id);
        insert cf2;
        
        //User u = new User(LastName='r@test.com',Email='r@test.com');
        //insert u;
        
        Resource__c r = Test_Util.createResource(cf.Id,UserInfo.getUserId());
        r.SRP_Function__c = 'Report Responsible';
        r.Role__c = 'Lead Analyst';
        insert r;
        
        Qualitative_Rating__c qr = Test_Util.createQR(cf);
        insert qr;
	}
}