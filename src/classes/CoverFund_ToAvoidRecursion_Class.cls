/*
@Name        :CoverFund_ToAvoidRecursion_Class
@Description :This is class written to set the flag true or false that is being used in trigger Update SRP count of SRF.
@author      :Rimali Gurav  
@date        :29th May 2013
*/

public with sharing class CoverFund_ToAvoidRecursion_Class {
	   // Static variables are local to the context of a Web request  
    // (or testMethod during a runTests call)  
    // Therefore, this variable will be initialized as false  
    // at the beginning of each Web request which accesses it.  

    private static boolean executedflag = false;


    public static boolean hasExecutedflag() {
        return executedflag;
    }

    // By setting the variable to true, it maintains this  
    // new value throughout the duration of the request  
    // (or testMethod)  
    
    public static void setExecutedflag() {
        executedflag = true;
    }
    
    public static void setExecutedflagFalse() {
        executedflag = false;
    }

}