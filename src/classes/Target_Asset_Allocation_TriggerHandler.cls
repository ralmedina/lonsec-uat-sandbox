public class Target_Asset_Allocation_TriggerHandler {
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	public Target_Asset_Allocation_TriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	private void deactivateOldTargetAssetAllocation(Target_Asset_Allocation__c[] pTAList){
		
		Set<Id> FundProfileIdSet = new Set<Id>();
		for (Target_Asset_Allocation__c t : pTAList){
			if (t.Fund_Profile__c!=null){
				FundProfileIdSet.add(t.Fund_Profile__c);
			}
		}
		
		List<Target_Asset_Allocation__c> taList = [select Id, Fund_Profile__c
										from Target_Asset_Allocation__c 
										where Fund_Profile__c IN :FundProfileIdSet];
		
		
		for (Target_Asset_Allocation__c oldTA : taList){
			oldTA.Active__c = false;
		}
		
		if (taList.size()>0)	update taList;
		
	}
	
	public void OnBeforeInsert(Target_Asset_Allocation__c[] pTAList){
		deactivateOldTargetAssetAllocation(pTAList);
	}
	
	
	/*static testMethod void test () {
        Sector__c s = Test_Util.createSector(null);
        insert s;
        
        List<Fund__c> fList = new List<Fund__c>();
        for (Integer i=0 ; i<20 ; i++){
        	Fund__c f = Test_Util.createFund(s.Id);
        	
        	fList.add(f);
        }
        insert fList;
        
        Fund_Recommendation__c fr = Test_Util.createFR(fList[0].Id);
        insert fr;
        //Fund_Recommendation__c fr2 = Test_Util.createFR(fList[0].Id);
        //insert fr2;
        
        fr.Approved_Date__c = date.today();
        update fr;
        
        // Start next approval process
        Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
        request.setObjectId(fr.Id);
        request.setComments('test');
        List<String> approverList = new List<String>();
        approverList.add(UserInfo.getuserId());
        request.setNextApproverIds(approverList);
        Approval.ProcessResult requestResult = Approval.process(request);
        
	}*/
}