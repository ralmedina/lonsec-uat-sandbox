/*
@Name       :TriggerHandler_UtilityData
@Description: This utility class is developed to create test data for test class 
			   and can be reused.
@author     :Rimali Gurav  
@date       :30th May 2013
 */
 @isTest(seealldata=true)
	
public with sharing class TriggerHandler_UtilityData {
	//declaration
	static List<Sector_Review_Plan__c> mListOfSectorReviewPlan;
	static List<Covered_Fund__c> mListOfCoverFund;
	static Fund__c  mfund;//to insert single record of fund
	static Sector__c  mSector;//to insert single record of sector
	
	//this method is written to insert the parent that is SRP
	public static List<Sector_Review_Plan__c> getSRP(integer MAX_BATCH_SIZE) {
        if (mListOfSectorReviewPlan==null) {
            mListOfSectorReviewPlan= new List<Sector_Review_Plan__c>();
            for (integer i = 0; i < MAX_BATCH_SIZE; i++) {
           	 	mListOfSectorReviewPlan.add(new Sector_Review_Plan__c(Name='Test SRPt' + i));
            }
            insert (mListOfSectorReviewPlan);
        }
        return mListOfSectorReviewPlan;
    } 
    
    //this method is written to insert the sector record 
    public static Sector__c getSector() {
        if (mSector ==null) {
           mSector = new Sector__c(Name='TestSector');
            insert (mSector);
        } 
        return mSector;
    }
    //this method is written to insert the fund record
     public static Fund__c getFund() {
        if (mfund ==null) {
           mfund = new Fund__c(Name='TestFund' ,Sector__c=getSector().id);
            insert (mfund);
        } 
        return mfund;
    }
    
    //this method is written to insert SRF related to SRP's
	public static list<Covered_Fund__c> getCoverFundData(integer MAX_BATCH_SIZE){
		
		 if (mListOfCoverFund==null) {
		 	List<Sector_Review_Plan__c> lstCallparent = getSRP(MAX_BATCH_SIZE); 
		 	 mListOfCoverFund = new list<Covered_Fund__c>();
			for(integer i=0; i < MAX_BATCH_SIZE; i++ ){
            mListOfCoverFund.add(new Covered_Fund__c(Writeup__c ='Test'+i ,Fund__c=getFund().id,Sector_Review_Plan_Record__c = lstCallparent[0].id,Style_manager__c ='Active' ));
			}
           insert (mListOfCoverFund);
		 }
        return mListOfCoverFund;
	}

}