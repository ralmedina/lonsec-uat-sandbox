public class Asset_Allocation_TriggerHandler {
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	public Asset_Allocation_TriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	private void deactivateOldAssetAllocation(Asset_Allocation__c[] pAAList){
		
		Set<Id> FundProfileIdSet = new Set<Id>();
		for (Asset_Allocation__c t : pAAList){
			if (t.Fund_Profile__c!=null){
				FundProfileIdSet.add(t.Fund_Profile__c);
			}
		}
		
		List<Asset_Allocation__c> aaList = [select Id, Fund_Profile__c
										from Asset_Allocation__c 
										where Fund_Profile__c IN :FundProfileIdSet];
		
		
		for (Asset_Allocation__c oldAA : aaList){
			oldAA.Active__c = false;
		}
		
		if (aaList.size()>0)	update aaList;
		
	}
	
	private void checkAssetAllocationWeightForEachFundProfile(Asset_Allocation__c[] pAAList){
		
		Set<Id> FundProfileIdSet = new Set<Id>();
		for (Asset_Allocation__c t : pAAList){
			if (t.Fund_Profile__c!=null){
				FundProfileIdSet.add(t.Fund_Profile__c);
			}
		}
		Map<Id,Double> fpIdFPTotalAllocationWeightOLD = new Map<Id,Double>();
		List<Asset_Allocation__c> aaList = [select Id, Fund_Profile__c, Fund_Profile__r.Id, Fund_Profile__r.Asset_Allocation_Total_Weight__c
										from Asset_Allocation__c 
										where Fund_Profile__c IN :FundProfileIdSet];
		
		// get current weights
		for (Asset_Allocation__c oldAA : aaList){
			if (oldAA.Fund_Profile__r.Asset_Allocation_Total_Weight__c!=0 && oldAA.Fund_Profile__r.Asset_Allocation_Total_Weight__c!=null)
				fpIdFPTotalAllocationWeightOLD.put(oldAA.Fund_Profile__r.Id, oldAA.Fund_Profile__r.Asset_Allocation_Total_Weight__c);
			else
				fpIdFPTotalAllocationWeightOLD.put(oldAA.Fund_Profile__r.Id, 0);
		}
		
		Map<Id,Double> fpIdFPTotalAllocationWeightNEW = new Map<Id,Double>();
		for (Asset_Allocation__c t : pAAList){
			Double additionalWeight = 0;
			if (t.Weight__c!=null){
				if (fpIdFPTotalAllocationWeightNEW.containsKey(t.Fund_Profile__c)){
					additionalWeight = fpIdFPTotalAllocationWeightNEW.get(t.Fund_Profile__c) + t.Weight__c;
					fpIdFPTotalAllocationWeightNEW.put(t.Fund_Profile__c, additionalWeight);
				}else{
					fpIdFPTotalAllocationWeightNEW.put(t.Fund_Profile__c, t.Weight__c);
				}
			}
		}
		
		Asset_Type__c cashAssetType = [select Id from Asset_Type__c where Name = 'CASH'];
		
		List<Asset_Allocation__c> aaToInsertList = new List<Asset_Allocation__c>();
		
		Set<Id> fundProfileWithNewAssetAllocationCashSet = new Set<Id>();
		for (Asset_Allocation__c t : pAAList){
			//Double currentWeight = fpIdFPTotalAllocationWeight.get(t.Fund_Profile__r.Id);
			Double additionalWeight = fpIdFPTotalAllocationWeightNEW.get(t.Fund_Profile__r.Id);
			// check if the new total weight will exceed 100
			//Double totalWeight = currentWeight + additionalWeight;
			if (additionalWeight > 100)	t.Weight__c.addError('The total weight for a fund profile should not exceed 100.');	
			else if (additionalWeight < 100){
				// create a new Asset Allocation with Cash to Complete the 100 weighting rule
				if (!fundProfileWithNewAssetAllocationCashSet.contains(t.Fund_Profile__c)){
					Asset_Allocation__c aa = new Asset_Allocation__c();
					aa.Fund_Profile__c = t.Fund_Profile__c;
					aa.Asset_Code__c = cashAssetType.Id;
					aa.Weight__c = 100 - additionalWeight;
					
					aaToInsertList.add(aa);
					
					fundProfileWithNewAssetAllocationCashSet.add(t.Fund_Profile__c);
				}
			}
		}
		
		if (aaToInsertList.size()>0)	insert aaToInsertList;
		
	}
	
	public void OnBeforeInsert(Asset_Allocation__c[] pAAList){
		deactivateOldAssetAllocation(pAAList);
		checkAssetAllocationWeightForEachFundProfile(pAAList);
	}
	
	
	/*static testMethod void test () {
        Sector__c s = Test_Util.createSector(null);
        insert s;
        
        List<Fund__c> fList = new List<Fund__c>();
        for (Integer i=0 ; i<20 ; i++){
        	Fund__c f = Test_Util.createFund(s.Id);
        	
        	fList.add(f);
        }
        insert fList;
        
        Fund_Recommendation__c fr = Test_Util.createFR(fList[0].Id);
        insert fr;
        //Fund_Recommendation__c fr2 = Test_Util.createFR(fList[0].Id);
        //insert fr2;
        
        fr.Approved_Date__c = date.today();
        update fr;
        
        // Start next approval process
        Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
        request.setObjectId(fr.Id);
        request.setComments('test');
        List<String> approverList = new List<String>();
        approverList.add(UserInfo.getuserId());
        request.setNextApproverIds(approverList);
        Approval.ProcessResult requestResult = Approval.process(request);
        
	}*/
}