/*
@name        :Test_Update_SRP_CountOf_SRF
@Description : This test class written to cover the trigger Update SRP count of SRF.
@author      :Rimali Gurav  
@date        :29th May 2013
 */



/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_Update_SRP_CountOf_SRF {
	 
	 
    static testMethod void myUnitTest() {
    	
        // TO DO: implement unit test
        list <Sector_Review_Plan__c> lstSRP = TriggerHandler_UtilityData.getSRP(50);//inserting the SRF with style ='Active'
        list <Covered_Fund__c> lstCFund = TriggerHandler_UtilityData.getCoverFundData(100);
        //list <Covered_Fund__c> lstWithStyle =TriggerHandler_UtilityData.getCoverFundDatawithStyle(100);
        list<Covered_Fund__c> lstCovfund = new list<Covered_Fund__c>();
        
        //updating the SRF with style=Enhanced Passive
     	for(Covered_Fund__c objcf:lstCFund)
     	{
     		objcf.Writeup__c = 'test';
     		objcf.Style_manager__c ='Enhanced Passive';
     		lstCovfund.add(objcf);
     	}
     	CoverFund_ToAvoidRecursion_Class.setExecutedflagFalse();
     	update lstCovfund;
     	
     	
     	
     	
     	//updating the SRF with style= Multimanager
     	for(Covered_Fund__c objcf:lstCovfund)
     	{
     		objcf.Writeup__c = 'test';
     		objcf.Style_manager__c ='Multi-manager';
     		
     	}
     	//updating the list
     	CoverFund_ToAvoidRecursion_Class.setExecutedflagFalse();
     	update lstCovfund;
     	//deleteing the list
     	list <Covered_Fund__c> lstCFundNew = TriggerHandler_UtilityData.getCoverFundData(100);
     	CoverFund_ToAvoidRecursion_Class.setExecutedflagFalse();
     	delete lstCFundNew;
     	//undeleting the list
     	CoverFund_ToAvoidRecursion_Class.setExecutedflagFalse();
     	undelete lstCovfund;
     	     	
  }
}