public class SW_TemplateCreateController {

	public String keyword {get; set;}
	public String sectorName {get; set;}
	public String selectedSector {get;set;}
	public String selectedHeader {get;set;}
	public String selectedStatus {get; set;}
	public templateHeaderWrapper headerWrapper {get;set;}
	public Boolean renderHeaderPicklist {get; set;}
	public Integer lastIndex {get;set;}
	
	public String getSelectedSector(){
		return selectedSector;
	}
	public void setSelectedSector(String s){
		this.selectedSector = s;
	}
	

	// create internal class as wrapper for Sector and Header
	public class templateHeaderWrapper{
		
		public Template_Header__c th {get;set;}
		public List<templateDetailWrapper> templateDetailWrapperList {get;set;}
		
		public templateHeaderWrapper(Template_Header__c pTH, List<templateDetailWrapper> pTemplateDetailWrapperList){
			templateDetailWrapperList = pTemplateDetailWrapperList;
			th = pTH;
		}
	}

	// create internal class as wrapper for Header Detail
	public class templateDetailWrapper{
		
		public Template_Detail__c td {get;set;}
		public Boolean isDeleted {get;set;}
		public Integer index {get; set;}
		public templateDetailWrapper(Template_Detail__c pTD){
			td = pTD;
			isDeleted = false;
		}
	}
	public class sectorWrapper {
		public String name {get;set;}
		public String id	{get; set;}
		
		public sectorWrapper(String pId, String pName){
			this.name = pName;
			this.id = pId;
		}
	}
	
	
	public List<sectorWrapper> sectorList {get; set;}
	List<Sector__c> sectorListCont = new List<Sector__c>();
	List<Template_Header__c> templateHeaderList = new List<Template_Header__c>(); // this is used for template header picklist
	Map<Id, List<Template_Header__c>> sectorIdTHListMap = new Map<Id, List<Template_Header__c>>(); // this is used for sector picklist, used in getting the appropriate templates for a sector
	Map<Id, List<templateDetailWrapper>> thIdTHWrapperMap = new Map<Id, List<templateDetailWrapper>>(); // this is used for the current displayed template details with wrapper
	Map<Id, templateHeaderWrapper> thWrapperMap = new Map<Id, templateHeaderWrapper>(); 
	
	public void save(){
		
		if (selectedHeader==''){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a template header.'));
			return;
		}
		
		if (String.isBlank(headerWrapper.th.Commentary__c)){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Please input a commentary.'));
			return;
		}
		
		if (headerWrapper.templateDetailWrapperList.size()<1){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Template must have atleast one field. Please put atleast one field.'));
			return;
		}else{
			Boolean hasNoEmptyField = true;
			for (templateDetailWrapper tdw : headerWrapper.templateDetailWrapperList){
				if ((String.isBlank(tdw.td.Name) || String.isBlank(String.valueOf(tdw.td.Sequence__c))) && !tdw.isDeleted){
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Template field must have name and sequence value.'));
					return;
				}
			}
		}
		
		List<Template_Detail__c> tdToDeleteList = new List<Template_Detail__c>();
		Boolean isValid = true;
		Boolean isSequence = true;
	
		List<Decimal> tmpSeqList = new List<Decimal>();
		//Start Validation
		String infoMessage = '';
		for (templateDetailWrapper temp : headerWrapper.templateDetailWrapperList){
				if (!temp.isDeleted){
							for (templateDetailWrapper temp2 : headerWrapper.templateDetailWrapperList){
								if (temp.td.Name!=null && temp.td.Name!='' && temp.td.Sequence__c!=null && temp.td.Sequence__c!=0){
									if (temp.index != temp2.index){
										if(temp.td.Sequence__c == temp2.td.Sequence__c){
										infoMessage = infoMessage + temp.td.Name + ', ';
										isValid = false;				
										}
									}
								}
							}
					tmpSeqList.add(temp.td.Sequence__c);
				}
				else{
						   if (temp.td.Id != null)	tdToDeleteList.add(temp.td);
				}
		}	
		tmpSeqList.sort();
		Decimal i=1;
		//for(Decimal i=1;i<=(tmpSeqList.size()-1);i++){
			for(Decimal d : tmpSeqList){
				
				if(i!=d){
					
					isSequence = false;
				}
				i++;
			}
		//}
		
		//End Validation
		
		if(isValid && isSequence){
			Template_Header__c tHeadToUpsert = headerWrapper.th;
			tHeadToUpsert.Sector__c = selectedSector;
			upsert tHeadToUpsert;
			
			List<Template_Detail__c> tdListToUpsert = new List<Template_Detail__c>();
	        
	        
	        
			for (templateDetailWrapper temp : headerWrapper.templateDetailWrapperList){
				//Template_Detail__c tempD = temp.td;
				if (temp.td.Name!=null && temp.td.Name!='' && temp.td.Sequence__c!=null && temp.td.Sequence__c!=0){
					temp.td.Name = temp.td.Name.trim();
					if (temp.td.Template_Header__c==null)	temp.td.Template_Header__c = tHeadToUpsert.Id;
					tdListToUpsert.add(temp.td);
				}
			}
			
			
			
			if (tdListToUpsert.size()>0)	upsert tdListToUpsert;
			if (tdToDeleteList.size()>0)	delete tdToDeleteList;
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO,'The records have been successfully saved.'));
			
			List<Template_Header__c> tHeadToDeativateList = new List<Template_Header__c>();
			for (Template_Header__c tempTH: sectorIdTHListMap.get(selectedSector)){
				if (tHeadToUpsert.Id != tempTH.Id){
					tempTH.Active__c = 'No';
					tHeadToDeativateList.add(tempTH);
				}
			}
			if (tHeadToDeativateList.size()>0)	update tHeadToDeativateList;
		}
		else{
			if(!isValid)
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid data: Duplicate sequence for ' + infoMessage));
			if(!isSequence)
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid data: Sequence of field name must have no gap and should begin from 1.'));
		}

	}

	public PageReference dummy(){
		return null;
	}
	
	final static Integer MAX_REC = 50;
	public String selectedPageNum {get;set;}
	public Integer totalPageNum {get;set;}
	public Map<Integer, List<sectorWrapper>> mainList {get;set;}
	public List<sectorWrapper> listToDisplay {get;set;}
	public List<SelectOption> pageNumOptions {get;set;}
	
	public PageReference getRefreshList(){
		listToDisplay = mainList.get(Integer.valueOf(selectedPageNum));
		return null;
	}
	
	public List<SelectOption> getPageNumbers(){
		pageNumOptions = new List<SelectOption>();
		for (Integer i=1 ; i<=totalPageNum ; i++){
			pageNumOptions.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
		}
		return pageNumOptions;
	}
	
	// CONSTRUCTOR
	public SW_TemplateCreateController(){
		
		mainList = new Map<Integer, List<sectorWrapper>>();
		
		selectedSector = '';
		selectedHeader = '';
		
		queryExistingTemplates();
		populateSector();
		
		// SET THE MAP FOR THE PAGINATION
		Integer pageNum = 1;
		list<sectorWrapper> tempList = new List<sectorWrapper>();
		for (sectorWrapper sw : sectorList){
			if (tempList.size() == MAX_REC){
				pageNum++;
				tempList = new list<sectorWrapper>();
				tempList.add(sw);
			}else{
				tempList.add(sw);
			}
			mainList.put(pageNum,tempList);
		}
		totalPageNum = pageNum;
		listToDisplay = mainList.get(1);
		// END SETTING MAP FOR THE PAGINATION
		
		
	}
	
	
	
	public PageReference addField(){
		TemplateDetailWrapper tempdw = new TemplateDetailWrapper(new Template_Detail__c());
		if (lastIndex!=null) 	lastIndex++;
		else					lastIndex = 1;
		tempdw.index = lastIndex;
		headerWrapper.templateDetailWrapperList.add(tempdw);
		return null;
	}
	
	/*public List<SelectOption> getSectorOptions(){
		List<SelectOption> listOptions = new List<SelectOption>();
		for (Sector__c s : sectorList){
			listOptions.add(new Selectoption(s.Id, s.Name));
		}
		return listOptions;
	}*/
	
	
	
	
	public List<SelectOption> getActivePickListOptions(){
		List<SelectOption> options = new List<SelectOption>();
		
		Schema.DescribeFieldResult fieldResult = Template_Header__c.Active__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		
		for( Schema.PicklistEntry f : ple)
   		{
      		options.add(new SelectOption(f.getLabel(), f.getValue()));
   		}       
   	
		return options;  
		
	}
	
	//for getting all the sectors. to-do: create search feature and pagination. 
	public List<sectorWrapper> getSectorList(){
			
			return sectorList;
	}
	
	
	public String getKeyword(){
		return keyword;
	}
	
	public void setKeyword(String searchword){
		if(searchword!=null){
				keyword = searchword;
		}
	}
	
	//action Link
	public PageReference showTemplates(){
			
		selectedSector = ApexPages.currentPage().getParameters().get('firstParam');
		sectorName= ApexPages.currentPage().getParameters().get('2ndParam');
		selectedHeader = '';
		headerWrapper = new TemplateHeaderWrapper(new Template_Header__c(),new List<TemplateDetailWrapper>());
		return null;
	}
	
	Map<Id,Template_Header__c> thIdTHMap = new Map<Id, Template_Header__c>();
	
	// this is the getter method of all template header for a section
	public List<SelectOption> getTemplateHeaderOptions(){
		List<SelectOption> listOptions = new List<SelectOption>();
		//get the appropriate templates for sectors
		if (selectedSector!=null && selectedSector!=''){
			listOptions.add(new Selectoption('','--None--'));
			listOptions.add(new Selectoption('--Create New--','--Create New--'));
			for (Template_Header__c th : sectorIdTHListMap.get(selectedSector)){
				thIdTHMap.put(th.Id,th);
				System.debug('ACI selectedSector ==========================>' + selectedSector);
				listOptions.add(new Selectoption(th.Id, th.Name));
			}
		}
		return listOptions;
	}
	
	public PageReference reloadTemplateDetails(){
		
		if(selectedHeader==null) selectedHeader='';
		if (selectedHeader=='--Create New--' || selectedHeader==''){
			List<TemplateDetailWrapper> tempDWList = new List<TemplateDetailWrapper>();
			tempDWList.add(new TemplateDetailWrapper(new Template_Detail__c()));
			headerWrapper = new templateHeaderWrapper(new Template_Header__c(), tempDWList);
			lastIndex = 0;
		}else{
			Integer i = 0;
			lastIndex = 1;
			Template_Header__c tempTH = thIdTHMap.get(selectedHeader);
			List<templateDetailWrapper> tempTemplateDetailWrapperList = new List<templateDetailWrapper>();
			for (Template_Detail__c td : tempTH.Template_Managers__r){
				TemplateDetailWrapper tempDW = new TemplateDetailWrapper(td);
				tempTemplateDetailWrapperList.add(tempDW);
				tempDW.isDeleted = false;
				tempDW.index = i++;
			}
			headerWrapper = new templateHeaderWrapper(tempTH, tempTemplateDetailWrapperList);
			lastIndex = headerWrapper.templateDetailWrapperList[headerWrapper.templateDetailWrapperList.size()-1].index + 1;
		}
		
		return null; 
	}
	
	//Map<Id,List<Template_Header__c>> sectorIdTHListMap = new Map<Id, List<Template_Header__c>>();
	
	void queryExistingTemplates(){
		Set<Id> sIdSet = new Set<Id>();
		 List<Sector__c> tmpSectorList = new List<Sector__c>();
		
		// query sectors
		templateHeaderList = [select Name, Active__c, Id, Sector__c, Commentary__c, (select Id, Name, Sequence__c, Template_Header__c from Template_Managers__r order by Sequence__c ASC) from Template_Header__c order by Sector__c];
		
		//set the id of sector
		for (Template_Header__c th : templateHeaderList){
			sIdSet.add(th.Sector__c);
			List<Template_Header__c> tempTHList = new List<Template_Header__c>();
			if (sectorIdTHListMap.containsKey(th.Sector__c)){
				tempTHList = sectorIdTHListMap.get(th.Sector__c);
				tempTHList.add(th);
				sectorIdTHListMap.put(th.Sector__c,tempTHList);
			}else{
				tempTHList.add(th);
				sectorIdTHListMap.put(th.Sector__c,tempTHList);
			}
		}
		
		tmpSectorList = [select Name, Id from Sector__c where Id IN :sIdSet order by Name];
		//templateHeaderList = [select Name, Active__c, Id, Sector__c, Commentary__c, (select Id, Name, Sequence__c, Template_Header__c from Template_Managers__r order by Sequence__c ASC) from Template_Header__c order by Sector__c limit 2];
		sectorListCont = tmpSectorList.clone();
		//this is for demo test purposes only
		
		
		
	}
	
	void populateSector(){
			sectorList = new List<sectorWrapper>();
			
			for(Sector__c s: sectorListCont){
				SectorWrapper sectorWrapper = new SectorWrapper(s.id,s.name);
				sectorList.add(sectorWrapper);
			}
	}
}