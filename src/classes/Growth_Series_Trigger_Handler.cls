public class Growth_Series_Trigger_Handler {
    
    public static void handleGSTaxStructures(List<Growth_Series__c> gsTaxes) {
    
        Set<Id> investmentCodes = new Set<Id>();
        Map<Id, Id> investmentBenchmarkMap = new Map<Id, Id>(); //id = investment, value = bc

        for(Growth_Series__c gs: gsTaxes) {
             investmentCodes.add(gs.Tax_Structure_Investment_Code__c); 
        }
        
        System.debug('jmdbg-investmentCodes' + investmentCodes);
        
        //query the matching benchmark codes fron funds
        for(Fund__c f: [Select Investment_Code_Used_for_Growth_Series__c, 
                               Benchmark_Code_Used_for_Growth_Series__c
                        From   Fund__c
                        Where  Investment_Code_Used_for_Growth_Series__c IN: investmentCodes]) {
        
            if(f.Benchmark_Code_Used_for_Growth_Series__c!=null) {
                investmentBenchmarkMap.put(f.Investment_Code_Used_for_Growth_Series__c, f.Benchmark_Code_Used_for_Growth_Series__c);
            }
        }
        System.debug('jmdbg-processRecentGrowthSeries' + investmentBenchmarkMap);
        
        if(investmentBenchmarkMap.size()>0) {
            processRecentGrowthSeries(investmentBenchmarkMap);
        }

    }
    
    public static void handleGSBenchmarkCodes(List<Growth_Series__c> gsBenchmarks) {
    
        Set<String> benchmarkCodes = new Set<String>();
        Map<Id, Id> investmentBenchmarkMap = new Map<Id, Id>(); //id = investment, value = bc

        for(Growth_Series__c gs: gsBenchmarks) {
            System.debug('jmdbg-String.valueOf(gs.Benchmark_Code__c).length()' + String.valueOf(gs.Benchmark_Code__c).length());
             if(String.valueOf(gs.Benchmark_Code__c).length()==18) {
                 System.debug('jmdbg-String.valueOf(gs.Benchmark_Code__c).substring(0,15)' + String.valueOf(gs.Benchmark_Code__c).substring(0,15));
                 benchmarkCodes.add(String.valueOf(gs.Benchmark_Code__c).substring(0,15)); 
             } else {
                 System.debug('gs.Benchmark_Code__c' + gs.Benchmark_Code__c);
                 benchmarkCodes.add(gs.Benchmark_Code__c); 
             }
        } 
        
        System.debug('jmdbg-benchmarkCodes' + benchmarkCodes);
        
        //query the matching investment codes from funds
        for(Fund__c f: [Select Investment_Code_Used_for_Growth_Series__c, 
                               Benchmark_Code_Used_for_Growth_Series__c
                        From   Fund__c
                        Where  Benchmark_Code_Used_for_Growth_Series__c IN: benchmarkCodes]) {
        
            if(f.Investment_Code_Used_for_Growth_Series__c!=null) {
                investmentBenchmarkMap.put(f.Investment_Code_Used_for_Growth_Series__c, f.Benchmark_Code_Used_for_Growth_Series__c);
            }
        }
        
        System.debug('jmdbg-processRecentGrowthSeries' + investmentBenchmarkMap);
        
        if(investmentBenchmarkMap.size()>0) {
            processRecentGrowthSeries(investmentBenchmarkMap);
        }
    
    }
    
    private static void processRecentGrowthSeries(Map<id,id> investmentBenchmarkMap) {
    
        Map<Id, Growth_Series__c> investmentCodesWithGSToUpdate = new Map<Id, Growth_Series__c>(); // key = investment code id, value = most recent growth series
        Map<Id, Decimal> investmentCodesWithInvestmentValue = new Map<Id, Decimal>(); // key = investment code id, value = pre-computed fund value
        Map<Id, Decimal> benchmarkCodesWithBenchmarkValue = new Map<Id, Decimal>(); // key = benchmark code id, value = pre-computed benchmark value
        Map<Id, Decimal> benchmarkCodesWithTreshholdValue = new Map<Id, Decimal>(); // key = benchmark code id, value = benchmark treshhold value
        List<Growth_Series__c> gsToUpdateList = new List<Growth_Series__c>();
        
        //use a parent to child query to limit only the TWO most recent GS per investment code
        for(Investment_Code__c ic: [Select Id, 
                                           (Select    Id, 
                                                      Lonsec_Value__c, 
                                                      Is_Validated_Data__c 
                                            from      Growth_Series__r 
                                            order by  Month_End__c DESC
                                            LIMIT     2) 
                                    from   Investment_Code__c 
                                    where  Id IN: investmentBenchmarkMap.keySet()]) {
                                    
            List<Growth_Series__c> gsList = new List<Growth_Series__c>();
            for(Growth_Series__c gs: ic.Growth_Series__r) {
                gsList.add(gs);
            }
            
            System.debug('jmdbg-gsList 1' + gsList);
            
            //ensure that there is "current" and "previous"
            if(gsList.size()==2) {
                investmentCodesWithGSToUpdate.put(ic.Id, gsList[0]);
                investmentCodesWithInvestmentValue.put(ic.Id, (gsList[0].Lonsec_Value__c / gsList[1].Lonsec_Value__c)-1);
            }
        }
        System.debug('jmdbg-investmentCodesWithGSToUpdate' + investmentCodesWithGSToUpdate);
        System.debug('jmdbg-investmentCodesWithInvestmentValue' + investmentCodesWithInvestmentValue);
        
        //use a parent to child query to limit only the TWO most recent GS per benchmark code
        for(Benchmark_Code__c bc: [Select Id,
                                          Benchmark__r.Performance_Threshold__c,
                                           (Select    Id, 
                                                      Lonsec_Value__c 
                                            from      Growth_Series__r 
                                            order by  Month_End__c DESC
                                            LIMIT     2) 
                                   from   Benchmark_Code__c
                                   where  Id IN: investmentBenchmarkMap.values()]) {
                                    
            List<Growth_Series__c> gsList = new List<Growth_Series__c>();
            for(Growth_Series__c gs: bc.Growth_Series__r) {
                gsList.add(gs);
            }
            
            System.debug('jmdbg-gsList 2' + gsList);
            
            //ensure that there is "current" and "previous"
            if(gsList.size()==2) {
                benchmarkCodesWithTreshholdValue.put(bc.Id, bc.Benchmark__r.Performance_Threshold__c);
                benchmarkCodesWithBenchmarkValue.put(bc.Id, (gsList[0].Lonsec_Value__c / gsList[1].Lonsec_Value__c)-1);
            }
        }
        
        System.debug('jmdbg-benchmarkCodesWithTreshholdValue' + benchmarkCodesWithTreshholdValue);
        System.debug('jmdbg-benchmarkCodesWithBenchmarkValue' + benchmarkCodesWithBenchmarkValue);
        
        for(Id invCode: investmentBenchmarkMap.keySet()) {
            
            Id benchCode = investmentBenchmarkMap.get(invCode);
        
            //ensure that all parts of the formula are present
            if(investmentCodesWithGSToUpdate.containsKey(invCode)&&
               investmentCodesWithInvestmentValue.containsKey(invCode) &&
               benchmarkCodesWithBenchmarkValue.containsKey(benchCode) &&
               benchmarkCodesWithTreshholdValue.containsKey(benchCode) ) {
               
               Decimal absValue = math.abs(investmentCodesWithInvestmentValue.get(invCode) - benchmarkCodesWithBenchmarkValue.get(benchCode));
               Boolean isValid = (absValue > benchmarkCodesWithTreshholdValue.get(benchCode)) ? false : true;
               
               System.debug('jmdbg-absValue ' + absValue );
               System.debug('jmdbg-isValid ' + isValid );
               System.debug('jmdbg-investmentCodesWithGSToUpdate.get(invCode).Is_Validated_Data__c' + investmentCodesWithGSToUpdate.get(invCode).Is_Validated_Data__c);
               
               //only trigger the UPDATE if the most existing validated data value does not match the computation result
               //the validation will prevent recursive loop.
               if(investmentCodesWithGSToUpdate.get(invCode).Is_Validated_Data__c != isValid) {
                   Growth_Series__c gsToUpdate = investmentCodesWithGSToUpdate.get(invCode);
                   gsToUpdate.Is_Validated_Data__c = isValid;
                   gsToUpdateList.add(gsToUpdate);
               }
                     
            }
        
        }
        
        System.debug('jmdbg-gsToUpdateList' + gsToUpdateList);
        
        if (gsToUpdateList.size()>0) {
            update gsToUpdateList;
        }

    }
    
}