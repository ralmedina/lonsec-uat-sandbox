public without sharing class GetContent {
	
    public List<ContentVersion> getContentVersions() {
        return [select id, Title, Description, FileType,
        Owner.Name, VersionNumber from ContentVersion
        Where IsLatest = true];
    }
    
    public PageReference getContentURL() { 
		
		ContentVersion []cv = [
			select id, ContentDocumentId from 
			ContentVersion where 
			contentdocumentid = '069O00000008o3b']; 
		
		return new PageReference('/' + cv[0].id); 
	}
	
}