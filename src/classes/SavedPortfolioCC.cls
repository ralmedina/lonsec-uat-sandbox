public class SavedPortfolioCC {

    public List<Saved_Portfolio__c> savedPortList = new List<Saved_Portfolio__c>();
    public List<Saved_Portfolio_Fund__c> savedPortFundList = new List<Saved_Portfolio_Fund__c>();
    public Saved_Portfolio__c portfolioToBeEditList { get; set; }
    
    public String cid = '';
    public String cidClient = '';
    public String investCode = '';
    
    public Saved_Portfolio__c sp {get;set;}
    
    public Set<Id> fundIdSet = new Set<Id>();
    public Set<Id> taxStrucIdSet = new Set<Id>();
    public Set<Id> invCodeIds = new Set<Id>();
    
    public String inputName {get; set;}
    public String txtInvestCode { get; set; }
    
    public Boolean checkShare {get; set;}
    public Boolean blnWeight { get; set; }
    public Boolean blnCreatePortSection {get; set;}
    public Boolean succMsgs {get; set;}
    public Boolean blnShowFundSection { get; set; }
    public Boolean blnEditWeightField { get; set; }
    public Boolean allowEditPortfolio { get; set; }
    public Boolean allowCopyShared { get; set; }
    public Boolean blnCopyPortSection { get; set; }
    
    public Id selectedValueToEdit { get; set; }
    public Id selectedValue {get; set;}
    public List<SelectOption> spPickListValue {get; set;}
    
    public String strName {get;set;}
    
    public List<TempDocument> loadPortfolios { get; set; }
    public List<TempDocument> tempPort = new List<TempDocument>();
    
    public class TempDocument {
        public TempDocument(Id idSPF, Id fundId, String spFundApir, String spFundName, String spFundSector, Decimal weight){
            this.idSPF= idSPF;
            this.fundId= fundId;
            this.InvCode= InvCode;
            this.spFundApir= spFundApir;
            this.spFundName= spFundName;
            this.spFundSector= spFundSector;
            this.weight= weight;
            selected = false;
        }
        public Id idSPF{ get; set; }
        public Id fundId{ get; set; }
        public String InvCode{ get; set; }
        public String spFundApir{ get; set; }
        public String spFundName{ get; set; }
        public String spFundSector{ get;set; }
        public Decimal weight { get; set; }
        public Boolean selected {get; set;}
    }
    
    public List<TempFundWrapper> loadFunds { get; set; }
    
    public class TempFundWrapper{
        public Id tfwFundId{ get; set; }
        public String tfwInvCode{ get; set; }
        public String tfwFundName{ get; set; }
        public String tfwStructName{ get; set;}
        public String tfwFundSector{ get;set; }
        public Boolean tfwselected {get; set;}
        
        public TempFundWrapper(Id tfwFundId, String tfwInvCode, String tfwFundName, String tfwStructName, String tfwFundSector){
            this.tfwFundId= tfwFundId;
            this.tfwInvCode= tfwInvCode;
            this.tfwFundName= tfwFundName;
            this.tfwStructName= tfwStructName;
            this.tfwFundSector= tfwFundSector;
            tfwselected = false;
        }
    }
    public SavedPortfolioCC(){
        loadFunds = new List<TempFundWrapper>();
        loadPortfolios = new List<TempDocument>();
        portfolioToBeEditList = new Saved_Portfolio__c();
        blnCreatePortSection = false;
        succMsgs = false;
        //investCode = '';
        
        for(User u : [Select Id, ContactId from User where Id = :UserInfo.getUserId()]){
          cid = u.ContactId;
          
        }
        for(Contact c : [SELECT Id, Account.Name FROM Contact WHERE Id =: cid]){
            cidClient = c.Account.Name;
        }
        
        /*// get Ids of all investment record depending on the user's input
        for(Investment_Code__c ic: [SELECT Id, Investment_Code__c, Tax_Structure__r.Fund__r.Name, Tax_Structure__r.Tax_Structure__c, Tax_Structure__r.Fund__r.Sector__r.Name FROM Investment_Code__c]){ // WHERE Investment_Code__c LIKE : investCode + '%'
            loadFunds.add(new TempFundWrapper(ic.Tax_Structure__r.Fund__c, ic.Investment_Code__c, ic.Tax_Structure__r.Fund__r.Name, ic.Tax_Structure__r.Tax_Structure__c, ic.Tax_Structure__r.Fund__r.Sector__r.Name));
        }*/
    }
    
    public List<selectOption> getSavedPortfolios(){
        Set<String> savePort = new Set<String>();
        List<selectOption> portfolioOptions = new List<selectOption>();
        
        savedPortList = [SELECT Id, Name, Contact__c, Share_with_colleagues__c FROM Saved_Portfolio__c WHERE Contact__c =: cid OR (Share_with_colleagues__c = true AND Contact__r.Account.Name = :cidClient) ORDER BY Name ASC LIMIT 1000];
        
        portfolioOptions.add(new SelectOption('','-- None --'));
        
        for(Saved_Portfolio__c sp: savedPortList){
            if(sp.Name != null){
                if(sp.Share_with_colleagues__c == true && sp.Contact__c != cid){
                    portfolioOptions.add(new SelectOption(sp.Id,sp.Name+'[Shared]'));
                }
                if((sp.Share_with_colleagues__c == true || sp.Share_with_colleagues__c == false) && sp.Contact__c == cid){
                    portfolioOptions.add(new SelectOption(sp.Id,sp.Name));
                }
            }
        }
        return portfolioOptions;
    }
    
    public List<selectOption> getSavedPortfoliosToEdit(){
        Set<String> savePort = new Set<String>();
        List<selectOption> portfolioOptions = new List<selectOption>();
        
        savedPortList = [SELECT Id, Name, Contact__c, Share_with_colleagues__c FROM Saved_Portfolio__c WHERE Contact__c =: cid OR (Share_with_colleagues__c = true AND Contact__r.Account.Name = :cidClient) ORDER BY Name ASC LIMIT 1000];
        
        portfolioOptions.add(new SelectOption('','-- None --'));
        
        for(Saved_Portfolio__c sp: savedPortList){
            if(sp.Name != null){
                if(sp.Share_with_colleagues__c == true && sp.Contact__c != cid){
                    portfolioOptions.add(new SelectOption(sp.Id,sp.Name+'[Shared]'));
                }
                if((sp.Share_with_colleagues__c == true || sp.Share_with_colleagues__c == false) && sp.Contact__c == cid){
                    portfolioOptions.add(new SelectOption(sp.Id,sp.Name));
                }
            }
        }
        return portfolioOptions;
    }
    
    public void getDocumentList(){
    
        String currentUser;
        blnShowFundSection = false;
        loadPortfolios.clear();
        tempPort.clear();
        savedPortFundList = [SELECT Id, Name, Fund__c, Fund__r.Name, Fund__r.APIR_Code__c, Fund__r.Sector__r.Name, Saved_Portfolio__r.Share_with_colleagues__c, Saved_Portfolio__r.Contact__c, Weighting__c FROM Saved_Portfolio_Fund__c WHERE Saved_Portfolio__c = : selectedValue LIMIT 1000];
        
        for(Saved_Portfolio_Fund__c sp : savedPortFundList) {
          loadPortfolios.add(new TempDocument(sp.Id, sp.Fund__c, sp.Fund__r.APIR_Code__c, sp.Fund__r.Name, sp.Fund__r.Sector__r.Name, sp.Weighting__c));
          fundIdSet.add(sp.Fund__c);
          currentUser = sp.Saved_Portfolio__r.Contact__c;
        }
        
        // loop fund using set of Fund ID from the query on portfolio and get all child ts ids of each fund
        for(Fund__c f: [SELECT Id, (SELECT Id FROM Tax_Structures__r) FROM Fund__c WHERE Id IN :fundIdSet LIMIT 1000]){
            for (Tax_Structure__c ts : f.Tax_Structures__r){
                taxStrucIdSet.add(ts.Id);
            }
        }
        
        // use the set of ts IDs and query child investment code records
        Map<String,String> fundIdInvCodeMap = new Map<String,String>();
        for(Tax_Structure__c t: [ SELECT Id, Fund__c, (SELECT Investment_Code__c FROM Investment_Codes__r) FROM Tax_Structure__c WHERE Id IN: taxStrucIdSet LIMIT 1000]){
            
            // gather all investment codes of the ts
            String invCode = '';
            for (Investment_Code__c i : t.Investment_Codes__r){
                invCode += (invCode!='') ? ','+i.Investment_Code__c : i.Investment_Code__c;
            }
            
            // check the fund investment code map for existing value and add the new investment codes from the new ts
            String tempMapInvCode = '';
            if (fundIdInvCodeMap.containsKey(t.Fund__c)){
                tempMapInvCode = fundIdInvCodeMap.get(t.Fund__c);
                tempMapInvCode += (tempMapInvCode!='') ? ','+invCode : invCode;
            }else{
                tempMapInvCode = invCode;
            }
            fundIdInvCodeMap.put(t.Fund__c, tempMapInvCode);
        }
        
        for(TempDocument td: loadPortfolios){
            td.InvCode = fundIdInvCodeMap.get(td.fundId);
        }
        
        if(currentUser != cid){
            blnEditWeightField = false;
            allowCopyShared = false;
        }else
            if(currentUser == cid){
                blnEditWeightField = true;
                allowCopyShared = true;
            }
    }
    
    public void save(){
        if(inputName == null || inputName == ''){
            succMsgs = true;
            ApexPages.Message msgss = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill up the necessary information!');
            ApexPages.addMessage(msgss);
        }else{
            Saved_Portfolio__c newPort = new Saved_Portfolio__c();
            newPort.Name = inputName;
            newPort.Contact__c = cid;
            newPort.Share_with_colleagues__c = checkShare;
            insert newPort;
            inputName = '';
            blnCreatePortSection = false;
            checkShare = false;
            succMsgs = true;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Succesfully created!');
            ApexPages.addMessage(msg);
        }
    }
    
    public void createPortfolio(){
        blnCreatePortSection = true;
    }
    
    public void savePortfolioFund(){
        Decimal weightTotal = 0;
        String conId = '';
        List<Saved_Portfolio_Fund__c> updateSVFund = new List<Saved_Portfolio_Fund__c>();
        
        // get the selected portfolio that needs to be check
        for(Saved_Portfolio__c sp: [SELECT Id, Contact__c FROM Saved_Portfolio__c WHERE Id = :selectedValue LIMIT 1000]){
            conId = sp.Contact__c;
        }
        
        // check if current user want to update his/her portfolio and not allow if it's not
        if(conId == cid){
            for(TempDocument td: loadPortfolios){
                weightTotal = weightTotal + td.weight;
            }
            if(weightTotal > 100){
                ApexPages.Message msgss = new ApexPages.Message(ApexPages.Severity.ERROR, 'Weighting must not exceed to 100%');
                ApexPages.addMessage(msgss);
            }else{
                for(TempDocument td: loadPortfolios){
                    for(Saved_Portfolio_Fund__c spf: savedPortFundList){
                        if(td.idSPF == spf.Id){
                            spf.Weighting__c = td.weight;
                            updateSVFund.add(spf);
                        }
                    }
                }
                update updateSVFund;
                succMsgs = true;
                if(weightTotal < 100){
                    ApexPages.Message msgss = new ApexPages.Message(ApexPages.Severity.WARNING, 'Warning! Please make sure that the total of all weighting is 100%.');
                    ApexPages.addMessage(msgss);
                }else
                    if(weightTotal == 100){
                        showSuccUpdated();
                    }
            }
        }else{
            succMsgs = true;
            showMessageRightPortfolio();
        }
    }
    
    public void searchInvestCode(){
        loadFunds.clear();
        investCode = txtInvestCode+'%';
        
        // get Ids of all investment record depending on the user's input
        for(Investment_Code__c ic: [SELECT Id, Investment_Code__c, Tax_Structure__r.Fund__r.Name, Tax_Structure__r.Tax_Structure__c, Tax_Structure__r.Fund__r.Sector__r.Name FROM Investment_Code__c WHERE Investment_Code__c LIKE : investCode LIMIT 1000]){
            loadFunds.add(new TempFundWrapper(ic.Tax_Structure__r.Fund__c, ic.Investment_Code__c, ic.Tax_Structure__r.Fund__r.Name, ic.Tax_Structure__r.Tax_Structure__c, ic.Tax_Structure__r.Fund__r.Sector__r.Name));
        }
        System.debug('!!!may laman tong tax structure ids' + taxStrucIdSet);
        
        System.debug('!!!may laman tong load funds' + loadFunds);
    }
    
    public void addToSelectedPortfolioFund(){
    List<Saved_Portfolio_Fund__c> spfList = new List<Saved_Portfolio_Fund__c>();
    Map<Id,String> fundName = new Map<Id,String>();
    Set<Id> presentFundIds = new Set<Id>();
    String conId = '';
    List<Saved_Portfolio_Fund__c> savedPortfolioList = new List<Saved_Portfolio_Fund__c>();
    for(Saved_Portfolio__c sp: [SELECT Id, Contact__c FROM Saved_Portfolio__c WHERE Id = :selectedValue LIMIT 1000]){
        conId = sp.Contact__c;
    }
    if(conId == cid){
            for(TempFundWrapper tempFund: loadFunds){
                if(tempFund.tfwselected == true){
                    fundName.put(tempFund.tfwFundId, tempFund.tfwFundName);
                }
            }
            for(Saved_Portfolio_Fund__c spf: [SELECT Fund__c FROM Saved_Portfolio_Fund__c WHERE Fund__c IN :fundName.keySet() AND Saved_Portfolio__c = :selectedValue LIMIT 1000]){
                presentFundIds.add(spf.Fund__c);
            }
            for(Id ids: fundName.keySet()){
                if(!presentFundIds.contains(ids)){
                    Saved_Portfolio_Fund__c spfInsert = new Saved_Portfolio_Fund__c(Saved_Portfolio__c = selectedValue, Fund__c = ids, Weighting__c = 0, Document__c = '');
                    spfList.add(spfInsert);
                }
            }
            insert spfList;
            getDocumentList();
            searchInvestCode();
    }else{
        succMsgs = true;
        showMessageRightPortfolio();
    }
    }
    
    public void deleteSavedPortfolioFund(){
    Set<Id> spfIds = new Set<Id>();
    String conId = '';
        for(TempDocument td: loadPortfolios){
            if(td.selected == true){
                spfIds.add(td.idSPF);
            }
        }
        for(Saved_Portfolio__c sp: [SELECT Id, Contact__c FROM Saved_Portfolio__c WHERE Id = :selectedValue]){
            conId = sp.Contact__c;
        }
        if(conId == cid){
            if(spfIds.size() > 0){
                List<Saved_Portfolio_Fund__c> spfToDelete = [SELECT Id FROM Saved_Portfolio_Fund__c WHERE Id IN :spfIds AND Saved_Portfolio__c = :selectedValue LIMIT 1000];
                delete spfToDelete;
                getDocumentList();
                
            }
        }else{
        succMsgs = true;
        showMessageRightPortfolio();
        }
        searchInvestCode();
    }
    
    public void allowEditSelectPortfolio(){
            
            // get the selected portfolio that needs to be check
            for(Saved_Portfolio__c sp: [SELECT Id, Name, Contact__c, Share_with_colleagues__c FROM Saved_Portfolio__c WHERE Id = :selectedValue LIMIT 1000]){
                //conId = sp.Contact__c;
                portfolioToBeEditList = sp;
            }
            if(portfolioToBeEditList.Id != null && portfolioToBeEditList.Contact__c == cid){
                allowEditPortfolio = true;
            }else{
                succMsgs = true;
                showMessageRightPortfolio();
            }
    }
    
    public void copyPortfolio(){
        Saved_Portfolio__c spCopy = [SELECT Id, Name, Contact__c, Share_with_colleagues__c FROM Saved_Portfolio__c WHERE Id = :selectedValue LIMIT 1000];
        Saved_Portfolio__c spToCopy = new Saved_Portfolio__c();
        List<Saved_Portfolio_Fund__c> spfList = [SELECT Name, Document__c, Fund__c, Weighting__c, Saved_Portfolio__c FROM Saved_Portfolio_Fund__c WHERE Saved_Portfolio__c = :spCopy.Id LIMIT 1000];
        List<Saved_Portfolio_Fund__c> portFundToUpdate = new List<Saved_Portfolio_Fund__c>();
        if(inputName == null || inputName == ''){
            
        }else{
            spToCopy.Name = inputName;
            spToCopy.Contact__c = cid;
            spToCopy.Share_with_colleagues__c = checkShare;
        }
        insert spToCopy;
        
        for(Saved_Portfolio_Fund__c spfc: spfList){
            Saved_Portfolio_Fund__c newChild = spfc.clone(false,true);
            newChild.Saved_Portfolio__c = spToCopy.Id;
            portFundToUpdate.add(newChild);
        }
        insert portFundToUpdate;
    }
    
    public void allowCopyPortfolio(){
        blnCopyPortSection = true;
    }
    
    public void updatePortfolio(){
        update portfolioToBeEditList;
        allowEditPortfolio = false;
        succMsgs = true;
        showSuccUpdated();
    }
    
    public void cancelCreate(){
        blnCreatePortSection = false;
        blnCopyPortSection = false;
    }
    
    public void cancelUpdatePortfolio(){
        allowEditPortfolio = false;
    }
    
    public void deletePortfolio(){
        // get the selected portfolio that needs to be check
        try{
        Saved_Portfolio__c spDelete = [SELECT Id, Name, Contact__c, Share_with_colleagues__c FROM Saved_Portfolio__c WHERE Id = :selectedValue AND Contact__c = :cid LIMIT 1000];
        delete spDelete;
        }catch(Exception e){
            succMsgs = true;
            showMessageRightPortfolio();
            getDocumentList();
        }
    }
    
    public void showSuccUpdated(){
        ApexPages.Message msgss = new ApexPages.Message(ApexPages.Severity.INFO, 'Successfully updated!');
        ApexPages.addMessage(msgss);
    }
    
    public void showMessageRightPortfolio(){
        ApexPages.Message msgss = new ApexPages.Message(ApexPages.Severity.ERROR, 'Kindly select the right portfolio!');
        ApexPages.addMessage(msgss);
    }
}