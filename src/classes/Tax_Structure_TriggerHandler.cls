public class Tax_Structure_TriggerHandler {
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	public Tax_Structure_TriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	private void checkMainStructure(Tax_Structure__c[] pTList){
		
		// gather all tax structure's Fund Ids
		Set<Id> fundIdSet 	= new Set<Id>();
		Set<Id> tIdSet 		= new Set<Id>();
		for (Tax_Structure__c t : pTList){
			if (t.Is_Main_Tax__c && t.Fund__c!=null){
				fundIdSet.add(t.Fund__c);
				if (t.Id!=null)	tIdSet.add(t.Id);
			}
		}
		
		List<Tax_Structure__c> tList = [select Id, Fund__c, Is_Main_Tax__c 
										from Tax_Structure__c 
										where Fund__c IN :fundIdSet 
										AND Id NOT IN :tIdSet 
										AND Is_Main_Tax__c = true
										order by Fund__c];
		
		
		for (Tax_Structure__c newTS : pTList){
			for (Tax_Structure__c oldTS : tList){
				if (newTS.Fund__c == oldTS.Fund__c)	newTS.Is_Main_Tax__c.addError('There can only be one Main Tax Structure per Fund.');
			}
		}
		
	}
	
	public void OnBeforeInsertUpdate(Tax_Structure__c[] pTList){
		checkMainStructure(pTList);
	}
}