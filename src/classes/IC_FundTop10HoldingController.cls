public class IC_FundTop10HoldingController{
    
    public String fundId {get;set;}
    public Fund__c fund { get; set; }
    public List<Fund_Top_10_Holding__c> fthList { get; set; }
    public List<fthWrapper> newFTHWrapper { get; set; }
    
    
    public class fthWrapper{
        public Fund_Top_10_Holding__c fth {get;set;}
        public Boolean isDeleted {get;set;}
        public Integer index {get;set;}
        
        public fthWrapper(Fund_Top_10_Holding__c pFTH, Boolean pIsDeleted, Integer pIndex){
            fth = pFTH;
            isDeleted = pIsDeleted;
            index = pIndex;
        }
    }
    
    public IC_FundTop10HoldingController(ApexPages.standardcontroller controller){
        initialize();
    }
    
    public IC_FundTop10HoldingController(){
        initialize();
    }
    
    public void initialize(){ 
        try{
            fundId = Apexpages.currentPage().getParameters().get('fundId');
            
            newFTHWrapper = new List<fthWrapper>();
            
            //getRelatedBenchmarkRiskProfileDetais();
            if (fundId!=null && fundId!=''){
                fund = [select Id, (select Id, Name, Fund__c, Value__c, Active__c, Holding_Code__c, Holdings_Date__c, Main_Fund__c from Fund_Top_10_Holdings1__r) from Fund__c where Id = :fundId];
                fthList = fund.Fund_Top_10_Holdings1__r;
            }
        
        }catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
            System.debug('!!!' + e.getMessage());
        }
    }
    
    public PageReference addFTH(){
        try{
        List<fthWrapper> tempList = new List<fthWrapper>();
        
        if (newFTHWrapper.size()>0)
            newFTHWrapper.add(new fthWrapper(new Fund_Top_10_Holding__c(Main_Fund__c=fund.Id,Active__c=true),false,newFTHWrapper[newFTHWrapper.size()-1].index+1));
        else
            newFTHWrapper.add(new fthWrapper(new Fund_Top_10_Holding__c(Main_Fund__c=fund.Id,Active__c=true),false,0));
            
        }catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }
        return null;
    }
    
    public PageReference save(){
        // All validations done on the page level
        List<Fund_Top_10_Holding__c> fthToInsert = new List<Fund_Top_10_Holding__c>();
        List<Fund_Top_10_Holding__c> brpdToDelete = new List<Fund_Top_10_Holding__c>();
        Decimal weight = 0;
        //Set<Id> fundProfileIds = new Set<Id>();
        
        for(fthWrapper fth : newFTHWrapper){
            if (!fth.isDeleted){
                if (fth.fth.Value__c!=null){
                    if (fth.fth.Value__c<=0){
                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Current Value__c should be greater than 0.'));
                        return null;
                    }
                    //fundProfileIds.add(fth.fth.Fund__c);
                    fthToInsert.add(fth.fth);
                    weight += fth.fth.Value__c;
                }
                else{
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Current Value__c should not be blank.'));
                    return null;
                }       
            }
        }
        
        if (fthToInsert.size()>0)    insert fthToInsert;
        if (brpdToDelete.size()>0)    delete brpdToDelete;
        return new PageReference('/'+fund.Id);
    }
    
    public PageReference cancel(){
        return new PageReference('/'+fund.Id);
    }
}