Public Class SearchFundsController{
    Public List<DocumentWrapper> documentDisplay {get;set;}
    Public List<Document_Repository__c> DocumentList {get;set;}
    Public List<String> selectedDocument {get;set;}
    Public List<String> dNameList = new List<String>();
    Public List<Document_Repository__c> documents {get;set;}
    ApexPages.standardController controller = null;
    
    Public SearchFundsController(ApexPages.standardController sc){
        documentDisplay = new List<DocumentWrapper>();
        documents = new List<Document_Repository__c>();
        controller = sc;
        
        for(Document_Repository__c DP: [Select Name FROM Document_Repository__c]){
            DocumentWrapper temp = new DocumentWrapper();
            temp.documentName = DP.Name;
            //temp.isChecked;
            documentDisplay.add(temp);
        }
        
    }
    Public class DocumentWrapper{
        Public Boolean isChecked{get;set;}
        Public String documentName{get;set;}
    }
    
    Public void search(){
    
    selectedDocument = new List<String>();
    dNameList = new List<String>();
    Integer a=0;
    
        for(DocumentWrapper DW: documentDisplay){
            if(DW.isChecked == true){
                selectedDocument.add(DW.documentName);
                dNameList.add(DW.documentName);
                a++;
            }
        }    
        
    documents = [SELECT Id, Name, Fund__r.Name FROM Document_Repository__c WHERE Name IN: dNameList];
    
    System.debug('@@@ ' + a);
    system.debug('@@@ ' + selectedDocument);
    system.debug('@@@ ' + dNameList);
    System.debug('@@@ ' + documents);
    }
    
}