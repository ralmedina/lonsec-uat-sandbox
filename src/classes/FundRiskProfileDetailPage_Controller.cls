public class FundRiskProfileDetailPage_Controller{
    public List<Fund_Risk_Profile_Detail__c> frpdList {get;set;}
    public List<frpdWrapper> oldFRPDWrapperList {get;set;}
    public List<frpdWrapper> newFRPDWrapperList {get;set;}
    public String frpId {get;set;}
    public String frpdId {get;set;}
    public Fund_Risk_Profile__c b {get;set;}
    
    public class frpdWrapper{
        public Fund_Risk_Profile_Detail__c frpd {get;set;}
        public Boolean isDeleted {get;set;}
        public Integer index {get;set;}
        
        public frpdWrapper(Fund_Risk_Profile_Detail__c pBrpd, Boolean pIsDeleted, Integer pIndex){
            frpd = pBrpd;
            isDeleted = pIsDeleted;
            index = pIndex;
        }
    }
    
    public FundRiskProfileDetailPage_Controller(ApexPages.standardcontroller controller){
        initialize();
    }
    
    public FundRiskProfileDetailPage_Controller(){
        initialize();
    }
    
    public void initialize(){ 
        try{
            frpId = Apexpages.currentPage().getParameters().get('frpId'); // fund risk profile Id (used if from fund risk profile page NEW)
            frpdId = Apexpages.currentPage().getParameters().get('Id'); // fund risk profile details Id (used if from fund risk profile details page NEW and UPDATE)
            
            oldFRPDWrapperList = new List<frpdWrapper>();
            newFRPDWrapperList = new List<frpdWrapper>();
            
            getRelatedFundRiskProfileDetais();
            
            Integer i = 0;
            for (Fund_Risk_Profile_Detail__c tempBRPD : frpdList){
                oldFRPDWrapperList.add(new frpdWrapper(tempBRPD,false,i));
                i++;
            }
        }catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
            System.debug('!!!' + e.getMessage());
        }
    }
    
    public void getRelatedFundRiskProfileDetais(){
        try{
            if (frpId!=null && frpId!=''){
                b = [select Id, (select Id, Name, Fund_Risk_Profile__c, Fund__c, Investment_Code__c, Date__c, Current_Weight__c from Fund_Risk_Profile_Details__r) from Fund_Risk_Profile__c where Id = :frpId];
                frpdList = b.Fund_Risk_Profile_Details__r;
            }else if (frpdId!=null && frpdId!=''){
                Fund_Risk_Profile_Detail__c frpd = [select Id, Name, Fund_Risk_Profile__c, Fund__c, Investment_Code__c, Date__c, Current_Weight__c from Fund_Risk_Profile_Detail__c where Id = :frpdId];
                b = [select Id, (select Id, Name, Fund_Risk_Profile__c, Fund__c, Investment_Code__c, Date__c, Current_Weight__c from Fund_Risk_Profile_Details__r) from Fund_Risk_Profile__c where Id = :frpd.Fund_Risk_Profile__c];
                frpdList = b.Fund_Risk_Profile_Details__r;
            }
        }catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
            System.debug('!!!' + e.getMessage());
        }
    }
    
    public PageReference addFundRiskProfileDetail(){
        try{
        List<frpdWrapper> tempList = new List<frpdWrapper>();
        Date newdate = System.today().toStartofMonth();
        
        if (newFRPDWrapperList.size()>0)
            newFRPDWrapperList.add(new frpdWrapper(new Fund_Risk_Profile_Detail__c(Fund_Risk_Profile__c=b.Id,Date__c=newdate),false,newFRPDWrapperList[newFRPDWrapperList.size()-1].index+1));
        else
            newFRPDWrapperList.add(new frpdWrapper(new Fund_Risk_Profile_Detail__c(Fund_Risk_Profile__c=b.Id,Date__c=newdate),false,0));
            
        }catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }
        return null;
    }
    
    public PageReference save(){
        // All validations done on the page level
        List<Fund_Risk_Profile_Detail__c> frpdToUpsert = new List<Fund_Risk_Profile_Detail__c>();
        List<Fund_Risk_Profile_Detail__c> frpdToDelete = new List<Fund_Risk_Profile_Detail__c>();
        Decimal weight = 0;
        Set<Id> componentIdSet = new Set<Id>();
        for (frpdWrapper brpdw : oldFRPDWrapperList){
            if (!brpdw.isDeleted){
                if (componentIdSet.contains(brpdw.frpd.Fund__c)){
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Fund should not be the same with the Fund.'));
                    return null;
                }
                componentIdSet.add(brpdw.frpd.Fund__c);
                if (brpdw.frpd.Current_Weight__c!=null){
                    if (brpdw.frpd.Current_Weight__c<=0){
                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Current Weight should be greater than 0.'));
                        return null;
                    }
                    frpdToUpsert.add(brpdw.frpd);
                    weight += brpdw.frpd.Current_Weight__c;
                }
                else{
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Current Weight should not be blank.'));
                    return null;
                }   
            }
            else    frpdToDelete.add(brpdw.frpd);
        }
        
        for (frpdWrapper brpdw : newFRPDWrapperList){
            if (!brpdw.isDeleted){
                if (componentIdSet.contains(brpdw.frpd.Fund__c)){
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Fund should not be the same with the Fund.'));
                    return null;
                }
                componentIdSet.add(brpdw.frpd.Fund__c);
                if (brpdw.frpd.Current_Weight__c!=null){
                    if (brpdw.frpd.Current_Weight__c<=0){
                        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Current Weight should be greater than 0.'));
                        return null;
                    }
                    frpdToUpsert.add(brpdw.frpd);
                    weight += brpdw.frpd.Current_Weight__c;
                }
                else{
                    ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Current Weight should not be blank.'));
                    return null;
                }       
            }
        }
        
        if (weight!=100 && frpdToUpsert.size()>0){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'Total of all benchmark weights must be equal to 100'));
            return null;
        }
        
        if (frpdToUpsert.size()>0)    upsert frpdToUpsert;
        if (frpdToDelete.size()>0)    delete frpdToDelete;
        return new PageReference('/'+b.Id);
    }
    
    public PageReference cancel(){
        return new PageReference('/'+b.Id);
    }
    
    public void updateDate(){
        frpId = Apexpages.currentPage().getParameters().get('frpId');
        Date newdate = System.today().toStartofMonth();
        
        List<Fund_Risk_Profile_Detail__c> frpListToUpdate = new List<Fund_Risk_Profile_Detail__c>();
        List<Fund_Risk_Profile_Detail__c> tempListToUpdate = new List<Fund_Risk_Profile_Detail__c>();
        if (frpId!=null && frpId!=''){
            frpListToUpdate = [select Id, Name, Fund_Risk_Profile__c, Fund__c, Investment_Code__c, Date__c, Current_Weight__c from Fund_Risk_Profile_Detail__c where Fund_Risk_Profile__c = :frpId];
            //brpList = b.Benchmark_Risk_Profile_Details__r;
            //brpListToUpdate = b.Benchmark_Risk_Profile_Details__r;
            for(Fund_Risk_Profile_Detail__c frpd: frpListToUpdate){
                frpd.date__c = newdate;
                tempListToUpdate.add(frpd);
            }
            update tempListToUpdate;
        }
    }
}