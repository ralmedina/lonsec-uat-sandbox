public class Test_Util {
	
	public Test_Util(){
		
	}
	
	public static Sector__c createSector(Id parentId){
		Sector__c s = new Sector__c();
		s.Name = 'testFund';
		if (parentId!=null)	s.Sector__c = parentId;
		return s;
	}
	
	public static Fund__c createFund(Id sectorId){
		Fund__c f = new Fund__c();
		f.Name = 'testSector';
		if (sectorId!=null)	f.Sector__c = sectorId;
		return f;
	}
	
	public static Sector_Review_Plan__c createSRP(){
		Sector_Review_Plan__c srp = new Sector_Review_Plan__c();
		srp.Name = 'testSRP';
		return srp;
	}
	
	public static Included_Sector__c createIS(Id srpId, Id sectorId){
		Included_Sector__c is = new Included_Sector__c();
		is.Sector_Review_Plan_Record__c = srpId;
		is.Sector__c = sectorId;
		return is;
	}
	
	public static Covered_Fund__c createCF(Id srpId, Id fundId){
		Covered_Fund__c cf = new Covered_Fund__c();
		cf.Sector_Review_Plan_Record__c = srpId;
		cf.Fund__c = fundId;
		return cf;
	}
	
	public static Rating_Model__c createRM(){
		Rating_Model__c rm = new Rating_Model__c();
		rm.Consistency__c = 20;
		rm.Lonsec_Fund_View__c = 20;
		rm.Port_Const__c = 20;
		rm.Research__c = 20;
		rm.Return__c = 20;
		rm.Risk__c = 20;
		rm.Risk_Mgt__c = 20;
		rm.Style__c = 20;
		rm.Team__c = 20;
		return rm;
	}
	
	public static File_Note__c createFN(Id fundId){
		File_Note__c fn = new File_Note__c();
		fn.Product_Strategy__c = fundId;
		return fn;
	}
	
	public static Attendee__c createA(){
		Attendee__c a = new Attendee__c();
		a.Attendee_Name__c = [select Id from Contact limit 1].Id;
		a.Attendee_User__c = [select Id from User limit 1].Id;
		a.Attendee_Type__c = 'Contact'; //'User';
		return a;
	}
	
	public static Resource__c createResource(Id pCFId, Id pContacUsertId){
		Resource__c r = new Resource__c();
		r.Covered_Fund__c = pCFId;
		r.Role__c = 'test role';
		r.Resource_Name__c = pContacUsertId;
		return r;
	}
	
	public static Qualitative_Rating__c createQR(Covered_Fund__c cf){
		Qualitative_Rating__c qr = new Qualitative_Rating__c();
		qr.Sector_Review_Fund__c = cf.Id;
		qr.Fund__c = cf.Fund__c;
		return qr;
	}
	
	public static Fund_Rating__c createFundRating(Fund__c f){
		Fund_Rating__c fr = new Fund_Rating__c();
		fr.Fund__c = f.Id;
		return fr;
	}
	
	public static Fund_Recommendation__c createFR(Id fId){
		Fund_Recommendation__c fr = new Fund_Recommendation__c();
		fr.Fund__c = fId;
		fr.Section__c = 'Direct Equities';
		fr.Send_for_Publishing__c = true;
		return fr;
	}

}