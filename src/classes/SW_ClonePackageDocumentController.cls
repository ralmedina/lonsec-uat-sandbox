Public with sharing class SW_ClonePackageDocumentController{
    List<String> documentStrings = new List<String>();
    List<Id> documentIds = new List<Id>();
    
    Public List<Document_Repository__c> documentsDisplay {get;set;}
    Public String PackageName {get;set;}
    Public SW_ClonePackageDocumentController(ApexPages.standardController controller){
        String docParams = String.valueOf(ApexPages.currentPage().getParameters().get('docs'));
            //filtering paramater on the URL before assigning to set of ID
            String param2 = docParams.replaceAll('}','');
            String param3 = param2.replaceAll('\\{','');
            String param4 = param3.replaceAll('%2C','');
            String finalparam = param4.replaceAll(' ', ''); 
            //end of filtering
            documentStrings.addAll(finalparam.split(','));
            
            for(String idVar: documentStrings){
                documentIds.add(idVar);
            }
    documentsDisplay = new List<Document_Repository__c>();
    documentsDisplay = [SELECT Id, Name, Author__c, Fund__r.Name, Sector__r.Name, Publish_Date__c FROM Document_Repository__c WHERE ID IN: documentIds ORDER BY Name ASC];
    }
    
    Public PageReference CloneDocuments(){
        List<Package_Document__c> packagesToInsert = new List<Package_Document__c>();
        if(PackageName == null){
        }
        else{
            Package_Repository__c pr = new Package_Repository__c();
            pr.Name = PackageName;
            insert pr;
            
                for(Document_Repository__c dr: documentsDisplay){
                    Package_Document__c pd = new Package_Document__c();
                    pd.Package_Repository__c = pr.Id;
                    pd.Document__c = dr.Id;
                    packagesToInsert.add(pd);
                }
        }
        if(packagesToInsert != null){
            insert packagesToInsert;
            PageReference pageRef = page.SW_AssignDocument;
            pageRef.setRedirect(true);
            return pageRef;
        }
        else{
            return null;
        }
    }
    
}