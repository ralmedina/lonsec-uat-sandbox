public class CoveredFund_TriggerHandler {
	private boolean m_isExecuting = false;
	private integer BatchSize = 0;
	
	public CoveredFund_TriggerHandler(boolean isExecuting, integer size){
		m_isExecuting = isExecuting;
		BatchSize = size;
	}
	
	private void copyMATLABDetailsToFund(Covered_Fund__c[] pCFList){
		
		Map<Id,Covered_Fund__c> fIdCFMap = new Map<Id,Covered_Fund__c>();
		
		for (Covered_Fund__c cf : pCFList){
			fIdCFMap.put(cf.Fund__c,cf);
		}
		
		// query the the child sector to get the parent sector id
		List<Fund__c> fundsList = new List<Fund__c>();
		for (Fund__c f : [select Id, MATLAB_Consistency__c, MATLAB_Return__c, MATLAB_Risk__c, Model_Rating__c from Fund__c where Id in :fIdCFMap.keySet()]){
			if (fIdCFMap.containsKey(f.Id)){
				Covered_Fund__c tempCF 	= fIdCFMap.get(f.Id);
				f.MATLAB_Consistency__c	= tempCF.MATLAB_Consistency__c;
				f.MATLAB_Return__c		= tempCF.MATLAB_Return__c;
				f.MATLAB_Risk__c		= tempCF.MATLAB_Risk__c;
				f.Model_Rating__c		= tempCF.Model_Rating__c;
				fundsList.add(f);
			}
		}
		
		if (fundsList.size()>0)	update fundsList;
	}
	
	public void OnAfterUpdate(Covered_Fund__c[] pCFList){
		copyMATLABDetailsToFund(pCFList);
	}
	
	static testMethod void test () {
        Sector__c s = Test_Util.createSector(null);
        insert s;
        
        List<Fund__c> fList = new List<Fund__c>();
        for (Integer i=0 ; i<20 ; i++){
        	Fund__c f = Test_Util.createFund(s.Id);
        	
        	fList.add(f);
        }
        insert fList;
        
        Contact c = new Contact(LastName='tc');
        insert c;
        
        Sector_Review_Plan__c srp = Test_Util.createSRP();
        insert srp;
        
        Included_Sector__c is = Test_Util.createIS(srp.Id, s.Id);
        insert is;
        
        Covered_Fund__c cf = Test_Util.createCF(srp.Id, fList[0].Id);
        insert cf;
        
        cf.MATLAB_Consistency__c = 11;
        update cf;
        
        Resource__c r = Test_Util.createResource(cf.Id,UserInfo.getUserId());
        r.SRP_Function__c = 'Report Responsible';
        insert r;
        
	}
}