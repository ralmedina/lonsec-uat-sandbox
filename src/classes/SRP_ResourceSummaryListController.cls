public with sharing class SRP_ResourceSummaryListController {

	public List<resourceWrapper> rwList {get;set;}
	public String srpID {get;set;}
	Map<Id,Integer> rIdFNCount 	= new Map<Id,Integer>();
	Map<Id,Integer> rIdRRCount 	= new Map<Id,Integer>();
	Map<Id,Integer> rIdMACount 	= new Map<Id,Integer>();
	Map<Id,Set<String>>  rIdRole 	= new Map<Id,Set<String>>();

	public SRP_ResourceSummaryListController(ApexPages.StandardController controller){
		srpID = ApexPages.currentPage().getParameters().get('id');
		
		rwList = new List<resourceWrapper>();
		
		List<Resource__c> rList = [select Id, SRP_Function__c, Covered_Fund__c, Resource_Name__c, Role__c from Resource__c where Covered_Fund__r.Sector_Review_Plan_Record__c = :srpID];
		for (Resource__c x : rList){
			
			if (x.SRP_Function__c!=null){
				if( x.SRP_Function__c.contains( 'File Note' ) ) {
					if (rIdFNCount.containsKey(x.Resource_Name__c)){
						Integer temp = rIdFNCount.get(x.Resource_Name__c)+1;
						rIdFNCount.put(x.Resource_Name__c,temp);
					}else	rIdFNCount.put(x.Resource_Name__c,1);
				}
				
				if( x.SRP_Function__c.contains( 'Report' ) ) {
					if (rIdRRCount.containsKey(x.Resource_Name__c)){
						Integer temp = rIdRRCount.get(x.Resource_Name__c)+1;
						rIdRRCount.put(x.Resource_Name__c,temp);
					}else	rIdRRCount.put(x.Resource_Name__c,1);
				}
				
				if( x.SRP_Function__c.contains( 'Meeting' ) ) {
					if (rIdMACount.containsKey(x.Resource_Name__c)){
						Integer temp = rIdMACount.get(x.Resource_Name__c)+1;
						rIdMACount.put(x.Resource_Name__c,temp);
					}else	rIdMACount.put(x.Resource_Name__c,1);
				}
			}
			
			
			if (rIdRole.containsKey(x.Resource_Name__c)){
				Set<String> tempRole = rIdRole.get(x.Resource_Name__c);
				tempRole.add(x.Role__c);
				rIdRole.put(x.Resource_Name__c,tempRole);
			}else{
				Set<String> tempRole = new Set<String>();
				tempRole.add(x.Role__c);
				rIdRole.put(x.Resource_Name__c,tempRole);
			}
		
		}
		
		Set<Id> rIDUnique = new Set<Id>();
		for (Resource__c x : rList){
			Integer tempFN = (rIdFNCount.containsKey(x.Resource_Name__c)) ? rIdFNCount.get(x.Resource_Name__c) : 0;
			Integer tempRR = (rIdRRCount.containsKey(x.Resource_Name__c)) ? rIdRRCount.get(x.Resource_Name__c) : 0;
			Integer tempMA = (rIdMACount.containsKey(x.Resource_Name__c)) ? rIdMACount.get(x.Resource_Name__c) : 0;
			Set<String> role = (rIdRole.containsKey(x.Resource_Name__c)) ? rIdRole.get(x.Resource_Name__c) : new Set<String>();
			
			if (!rIDUnique.contains(x.Resource_Name__c)){
				rwList.add(new resourceWrapper(x,tempFN,tempRR,tempMA,role));
				rIDUnique.add(x.Resource_Name__c);
			}
		}
		
	}
	
	public class resourceWrapper{
		
		public Integer fn {get;set;}
		public Integer rr {get;set;}
		public Integer ma {get;set;}
		public Resource__c r {get;set;}
		public String role {get;set;}
		
		public resourceWrapper(Resource__c pR, Integer pFN, Integer pRR, Integer pMA, Set<String> pRole){
			r = pR;
			fn = pFN;
			rr = pRR;
			ma = pMA;
			role = (pRole.size()>0) ? String.valueOf(pRole) : '';
			role = role.remove('{');
			role = role.remove('}');
		}
	}

}