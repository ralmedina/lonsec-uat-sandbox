trigger Structured_Product_Analyst_Rating_Trigger on Structured_Product_Analyst_Rating__c (before insert, after update) {
	
	Structured_Product_Rating_TriggerHandler handler = new Structured_Product_Rating_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if (Trigger.isInsert && Trigger.isBefore){
		handler.OnBeforeInsert(Trigger.new);
	}
	
	if ((Trigger.isUpdate || Trigger.isInsert) && Trigger.isAfter){
		handler.OnAfterUpdate(Trigger.new);
	}
}