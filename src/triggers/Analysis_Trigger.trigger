trigger Analysis_Trigger on Analysis__c (after insert, after update) {
    if(trigger.isInsert || trigger.isUpdate){
        if(trigger.isAfter){
            Set<Id> mpIdSet = new Set<Id>();
            for(Analysis__c a: trigger.new){
                mpIdSet.add(a.Model_Portfolio__c);
            }
            ModelPortfolioTriggerHandler.archiveModelPortfolios(mpIdSet);
        }
    }
}