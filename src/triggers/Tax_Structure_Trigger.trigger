trigger Tax_Structure_Trigger on Tax_Structure__c (before insert, before update) {
	Tax_Structure_TriggerHandler handler = new Tax_Structure_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if((Trigger.isInsert || Trigger.isUpdate) && Trigger.isBefore){
		handler.OnBeforeInsertUpdate(Trigger.new);
	}
}