trigger Investment_Code_Trigger on Investment_Code__c (before insert, after insert, after update) {
  if(trigger.isInsert || trigger.isUpdate){
    if(trigger.isAfter){
    
        Map<id, string> taxIdWithInvestmentName = new Map<id, string>();
        Map<id, id> fundIdsWithInvestmentName = new Map<id, id>();
        List<Investment_Code__c> investToUpdate = new List<Investment_Code__c>();
        List<Fund__c> fundToUpdate = new List<Fund__c>();
        
        //filter the trigger.new, get only the tax__c of investments with Used_for_Growth_Series__c = true; this will further filter out the later SOQL results
        for(Investment_Code__c i : trigger.new) {
            if(i.Used_for_Growth_Series__c == true) {
                taxIdWithInvestmentName.put(i.Tax_Structure__c, i.Id);
            }
        }
    
        //map tax Ids with their corresponding funds
        for(Tax_Structure__c t: [Select id, fund__c from Tax_Structure__c where id IN: taxIdWithInvestmentName.keySet()]) {
           fundIdsWithInvestmentName.put(t.Fund__c, taxIdWithInvestmentName.get(t.Id));
        }
    
        for(Investment_Code__c i: [Select Id, Investment_Code__c, Used_for_Growth_Series__c from Investment_Code__c where Used_for_Growth_Series__c = true AND Tax_Structure__r.Fund__c IN: fundIdsWithInvestmentName.keySet() and id NOT IN: trigger.newMap.keySet()]) {
            i.Used_for_Growth_Series__c = false;
            investToUpdate.add(i);
        }
        
        //NEW ---
        for(Fund__c f: [Select Id, Investment_Code_Used_for_Growth_Series__c from Fund__c where Id IN :fundIdsWithInvestmentName.keySet()]) {
            f.Investment_Code_Used_for_Growth_Series__c = fundIdsWithInvestmentName.get(f.Id);
            fundToUpdate.add(f);
        }
        //NEW ---
    
        if(investToUpdate.size()>0){
            update investToUpdate;
        }
        
        //NEW ---
        if(fundToUpdate.size()>0) {
            update fundToUpdate;
        }
        //NEW --
    /****************************************************************************
        Set<id> taxIds = new Set<id>();
        Set<Id> fundIds = new Set<Id>();
        List<Investment_Code__c> investToUpdate = new List<Investment_Code__c>();
        Fund__c tempFundToUpdate = new Fund__c();
        List<Fund__c> fundToUpdate = new List<Fund__c>();
        
        //filter the trigger.new, get only the tax__c of investments with Used_for_Growth_Series__c = true; this will further filter out the later SOQL results
        for(Investment_Code__c i : trigger.new) {
            if(i.Used_for_Growth_Series__c == true) {
                taxIds.add(i.Tax_Structure__c);
            }
        }
    
        //map tax Ids with their corresponding funds
        for(Tax_Structure__c t: [Select id, fund__c from Tax_Structure__c where id IN: taxIds]) {
           fundIds.add(t.Fund__c);
        }
    
        for(Investment_Code__c i: [Select Id, Investment_Code__c, Used_for_Growth_Series__c from Investment_Code__c where Used_for_Growth_Series__c = true AND Tax_Structure__r.Fund__c IN: fundIds and id NOT IN: trigger.newMap.keySet()]) {
            i.Used_for_Growth_Series__c = false;
            investToUpdate.add(i);
        }
    
        if(investToUpdate.size()>0){
            update investToUpdate;
        }
    ********************************************************/
    }
  }
    if(trigger.isInsert || trigger.isUpdate){
        if(trigger.isBefore){
            Map<Id, List<Investment_Code__c>> fundWithInvestmentMap = new Map<Id, List<Investment_Code__c>>();

            Map<id, id> taxFundMap = new Map<id, id>();
            Set<id> taxIds = new Set<id>();
            
            //iterate through the trigger.new to get the tax Ids
            for(Investment_Code__c i: trigger.new) {
                taxIds.add(i.Tax_Structure__c);
            }
            
            //map tax Ids with their corresponding funds
            for(Tax_Structure__c t: [Select id, fund__c from Tax_Structure__c where id IN: taxIds]) {
                taxFundMap.put(t.Id, t.Fund__c);
            }
            
            //iterate again through trigger.new; group the invoices PER grand-parent FUND.
            for(Investment_Code__c i : trigger.new) {
                //use the fund Id stored in the taxFundMap as key for fundWithInvestmentMap map (use the tax Id to get the fund id).
                if(fundWithInvestmentMap.containsKey(taxFundMap.get(i.Tax_Structure__c))) {
                    fundWithInvestmentMap.get(taxFundMap.get(i.Tax_Structure__c)).add(i);
                } else {
                    fundWithInvestmentMap.put(taxFundMap.get(i.Tax_Structure__c), new List<Investment_Code__c>{i});
                }
            }
            
            //now that Investment_Code__c are grouped per fund, iterate through the keySet
            for(Id fundIds: fundWithInvestmentMap.keySet()) {
                //per fund grouping, initialize the count to 0.
                Integer count = 0;
                for(Investment_Code__c inv: fundWithInvestmentMap.get(fundIds)) {
                    //increase count by 1 for every checked fields
                    if(inv.Used_for_Growth_Series__c == true) {
                        count++;
                    }
                    //add error if count is more than 1
                    if(count>1) {
                        inv.addError('Each fund must only use 1 Investment_Code__c for growth');
                        //reset the count to 1 in case where there are other records marked as true
                        count = 1;
                    }
                }
            }

        }
    }
}