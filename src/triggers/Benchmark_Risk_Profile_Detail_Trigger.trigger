trigger Benchmark_Risk_Profile_Detail_Trigger on Benchmark_Risk_Profile_Detail__c (before insert, before update, before delete, after insert, after update, after delete) {
    /*if(trigger.isInsert || trigger.isUpdate){
        if(trigger.isBefore){
            Map<Id, List<Benchmark_Risk_Profile_Detail__c>> benchRiskProfileMap = new Map<Id, List<Benchmark_Risk_Profile_Detail__c>>();
            for(Benchmark_Risk_Profile_Detail__c brpd: trigger.new){
                if(benchRiskProfileMap.containsKey(brpd.Benchmark_Risk_Profile__c)) {
                    benchRiskProfileMap.get(brpd.Benchmark_Risk_Profile__c).add(brpd);
                } else {
                    benchRiskProfileMap.put(brpd.Benchmark_Risk_Profile__c, new List<Benchmark_Risk_Profile_Detail__c>{brpd});
                }
            }
            
            for(Id benchRiskProfileIds: benchRiskProfileMap.keySet()) {
                //per risk profile grouping, initialize the weightTotal to 0.
                Decimal weightTotal = 0;
                for(Benchmark_Risk_Profile_Detail__c brpd: benchRiskProfileMap.get(benchRiskProfileIds)) {
                    if(brpd.Current_Weight__c != null){
                        weightTotal += brpd.Current_Weight__c;
                    }
                }
                if(weightTotal > 100) {
                    for(Benchmark_Risk_Profile_Detail__c brpd: benchRiskProfileMap.get(benchRiskProfileIds)) {
                        brpd.addError('Weight must not exceed to 100%');
                    } 
                }
            }
        }
    }*/
    
    if(trigger.isInsert){
        if(trigger.isAfter){
            Map<Id, Set<Id>> sectorFundIds = new Map<Id, Set<Id>>(); //this contains fund ids PER sector
            Map<Id, Set<Id>> sectorPortfolioIds = new Map<Id, Set<Id>>(); //this contains model portfolio ids PER sector
            Set<String> existingSectorPortfolio = new Set<String>(); //this contains sector-portfolio Id combinations
            
            Set<Id> portfoliosToCheck = new Set<Id>(); 
            Set<String> brpSecIdSet = new Set<String>();
            
            for(Benchmark_Risk_Profile_Detail__c brpd: [Select Model_Portfolio__c, Sector__c, Benchmark_Risk_Profile__c from Benchmark_Risk_Profile_Detail__c where Id IN: trigger.newMap.keySet()]) {
                portfoliosToCheck.add(brpd.Model_Portfolio__c);
                if(sectorPortfolioIds .containsKey(brpd.Sector__c)) {
                    sectorPortfolioIds.get(brpd.Sector__c).add(brpd.Model_Portfolio__c);
                } else {
                    sectorPortfolioIds.put(brpd.Sector__c, new Set<Id>{brpd.Model_Portfolio__c});
                }
            }
            //n
            system.debug('!!sectorPortfolioIds'+sectorPortfolioIds);
            for(Fund__c f: [Select Id, Investment_Consulting_Sector__c from Fund__c where Investment_Consulting_Sector__c IN: sectorPortfolioIds.keySet()]) {
                if(sectorFundIds.containsKey(f.Investment_Consulting_Sector__c)) {
                    sectorFundIds.get(f.Investment_Consulting_Sector__c).add(f.Id);
                } else {
                    sectorFundIds.put(f.Investment_Consulting_Sector__c, new Set<Id>{f.Id});
                }
            }
            system.debug('!!sectorFundIds'+sectorFundIds);

            for(Analysis__c a: [Select sector__c, Model_Portfolio__c from Analysis__c where Model_Portfolio__c IN: portfoliosToCheck]) {
                if(a.sector__c!=null) {
                    existingSectorPortfolio.add(a.sector__c + '-' + a.Model_Portfolio__c);
                }
            }
            
            List<Analysis__c> analysisToInsert = new List<Analysis__c>();
            
            //loop through the sector keysets
            for(Id sectorId: sectorPortfolioIds.keySet()) {
            
                for(Id mPortfolioId: sectorPortfolioIds.get(sectorId)) {
                    //check if sector - mportfolio combination already exists. create model portfolio if it does not exists yet.
                    if(!existingSectorPortfolio.contains(sectorId + '-' + mPortfolioId)){
                        analysisToInsert.add(new Analysis__c(Sector__c = sectorId, Model_Portfolio__c = mPortfolioId));
                    }
                     
                } //end mPortfolio loop
                
            } //end sector loop
            
            if(analysisToInsert.size()>0) {
                insert analysisToInsert;
                
                Map<Id,Id> brpFRBMap = new Map<Id,Id>();
                Map<Id,Set<Id>> brpIdSecIdsMap = new Map<Id,Set<Id>>();
                Map<Id,Set<Id>> mpIdBrpIdsMap = new Map<Id,Set<Id>>();
                for(Benchmark_Risk_Profile__c brp: [select Id, Model_Portfolio__c, (select Id from Fund_Risk_Profiles__r), (select Id, Sector__c from Benchmark_Risk_Profile_Details__r) from Benchmark_Risk_Profile__c where Model_Portfolio__c = :portfoliosToCheck]){
                    for(Fund_Risk_Profile__c frp: brp.Fund_Risk_Profiles__r){
                        brpFRBMap.put(brp.Id, frp.Id);
                    }
                    for(Benchmark_Risk_Profile_Detail__c brpd: brp.Benchmark_Risk_Profile_Details__r){
                        if(brpIdSecIdsMap.containsKey(brp.Id)){
                            brpIdSecIdsMap.get(brp.Id).add(brpd.Sector__c);
                        }else{
                            brpIdSecIdsMap.put(brp.Id, new Set<Id>{brpd.Sector__c});
                        }
                    }
                    if(mpIdBrpIdsMap.containsKey(brp.Model_Portfolio__c)){
                        mpIdBrpIdsMap.get(brp.Model_Portfolio__c).add(brp.Id);
                    }else{
                        mpIdBrpIdsMap.put(brp.Model_Portfolio__c, new Set<Id>{brp.Id});
                    }
                }
                Map<Id,Set<Id>> frpFundIdSetMap = new Map<Id,Set<Id>>();
                for(Fund_Risk_Profile__c frp: [select Id, (select Id, Fund__c from Fund_Risk_Profile_Details__r) from Fund_Risk_Profile__c where Model_Portfolio__c = :portfoliosToCheck]){
                    for(Fund_Risk_Profile_Detail__c frpd: frp.Fund_Risk_Profile_Details__r){
                        if(frpFundIdSetMap.containsKey(frp.Id)){
                            frpFundIdSetMap.get(frp.Id).add(frpd.Fund__c);
                        }else{
                            frpFundIdSetMap.put(frp.Id, new Set<Id>{frpd.Fund__c});
                        }
                    }
                    
                }
                //List<Fund_Commentaries__c> fcList = new List<Fund_Commentaries__c>();
                List<Fund_Commentaries__c> fCommToInsert = new List<Fund_Commentaries__c>();
                for(Analysis__c a: analysisToInsert){
                    Set<Id> fundsFromModel = new SEt<Id>();
                    Set<Id> fundsFromSector = new SEt<Id>();
                    Set<Id> fundsToCreate = new SEt<Id>();
                    if(sectorFundIds.containsKey(a.Sector__c)){
                        fundsFromSector = sectorFundIds.get(a.Sector__c);
                    }
                    if(mpIdBrpIdsMap.containsKey(a.Model_Portfolio__c)){
                        for(Id brpKey: mpIdBrpIdsMap.get(a.Model_Portfolio__c)){
                            if(brpFRBMap.containsKey(brpKey)){
                                if(frpFundIdSetMap.containsKey(brpFRBMap.get(brpKey)))
                                fundsFromModel.addAll(frpFundIdSetMap.get(brpFRBMap.get(brpKey)));
                            }
                        }
                    }
                    for(Id f: fundsFromSector){
                        if(fundsFromModel.contains(f)){
                            fundsToCreate.add(f);
                        }
                    }
                    for(Id f: fundsToCreate){
                        Fund_Commentaries__c fcList = new Fund_Commentaries__c(Fund__c = f, Analysis__c = a.Id);
                        fCommToInsert.add(fcList);
                    }
                }
                if(fCommToInsert.size()>0) insert fCommToInsert;
                
            }
        }
    }
    
    if(trigger.isInsert || trigger.isUpdate){
        if(trigger.isAfter){
            Set<Id> mpIdSet = new Set<Id>();
            Set<Id> brpIdSet = new Set<Id>();
            for(Benchmark_Risk_Profile_Detail__c brpd: trigger.new){
                //mpIdSet.add(frpd.Fund_Risk_Profile__r.Model_Portfolio__c);
                brpIdSet.add(brpd.Benchmark_Risk_Profile__c);
            }
            for(Benchmark_Risk_Profile__c brp: [select    id,
                                                     Name,
                                                     Model_Portfolio__c
                                           from      Benchmark_Risk_Profile__c
                                           where     Id IN :brpIdSet]){
                mpIdSet.add(brp.Model_Portfolio__c);
            }
            //Fund_Risk_Profile_Detail_Trigger.onBefore(mpIdSet);
            
            ModelPortfolioTriggerHandler.archiveModelPortfolios(mpIdSet);
        }
    }
    
    if(trigger.isDelete){
        if(trigger.isAfter){
            Set<Id> mpIdSet = new Set<Id>();
            Set<Id> brpIdSet = new Set<Id>();
            for(Benchmark_Risk_Profile_Detail__c brpd: trigger.old){
                //mpIdSet.add(frpd.Fund_Risk_Profile__r.Model_Portfolio__c);
                brpIdSet.add(brpd.Benchmark_Risk_Profile__c);
            }
            for(Benchmark_Risk_Profile__c brp: [select    id,
                                                     Name,
                                                     Model_Portfolio__c
                                           from      Benchmark_Risk_Profile__c
                                           where     Id IN :brpIdSet]){
                mpIdSet.add(brp.Model_Portfolio__c);
            }
            //Fund_Risk_Profile_Detail_Trigger.onBefore(mpIdSet);
            
            ModelPortfolioTriggerHandler.archiveModelPortfolios(mpIdSet);
        }
    }
    /*//just to be ready
    if(trigger.isDelete && trigger.isAfter){

        Set<Id> portfoliosToCheck = new Set<Id>();
        Set<String> existingSectorPortfolio = new Set<String>();
        List<Analysis__c> analysisToDelete = new List<Analysis__c>();
        Map<Id, List<Id>> sectorIdBrpIdMap = new Map<Id,List<Id>>();
        Map<Id,Id> brpIdModelPortfolioMap = new Map<Id,Id>();
        Set<Id> benchmarkRiskProfiles = new Set<Id>();
        
        for(Benchmark_Risk_Profile_Detail__c brpd: trigger.old){
            benchmarkRiskProfiles.add(brpd.Benchmark_Risk_Profile__c);
            if(brpd.Sector__c!=null){
                if(sectorIdBrpIdMap.containsKey(brpd.Sector__c)) {
                    sectorIdBrpIdMap.get(brpd.Sector__c).add(brpd.Benchmark_Risk_Profile__c);
                } else {
                    sectorIdBrpIdMap.put(brpd.Sector__c, new List<Id> {brpd.Benchmark_Risk_Profile__c});
                }
                
            }
        }
        
        for(Benchmark_Risk_Profile__c brp: [select Id, Benchmark_Portfolio__r.Model_Portfolio__c from Benchmark_Risk_Profile__c where Id IN: benchmarkRiskProfiles ]){
            portfoliosToCheck.add(brp.Benchmark_Portfolio__r.Model_Portfolio__c);
            if(brp.Benchmark_Portfolio__r.Model_Portfolio__c!=null){
                brpIdModelPortfolioMap.put(brp.Id,brp.Benchmark_Portfolio__r.Model_Portfolio__c);
            }
        }
        
        for(Id sectorId: sectorIdBrpIdMap.keySet()){
            for(Id brpId: sectorIdBrpIdMap.get(sectorId)) {
                if(brpIdModelPortfolioMap.containsKey(brpId)) {
                    existingSectorPortfolio.add(sectorId + '-' + brpIdModelPortfolioMap.get(brpId));
                }
            }
        }

        for(Analysis__c a: [Select Sector__c, Model_Portfolio__c from Analysis__c where Model_Portfolio__c IN: portfoliosToCheck]) {
            if(existingSectorPortfolio.contains(a.Sector__c + '-' + a.Model_Portfolio__c)){
                analysisToDelete.add(a);
            }
        }
        
        system.debug('!!analysisToDelete'+analysisToDelete);
        if(analysisToDelete.size()>0){
            delete analysisToDelete;
        }
        
    }*/
    
    /*if(trigger.isDelete && trigger.isAfter){
        Set<Id> portfoliosToCheck = new Set<Id>();
        Set<String> existingSectorPortfolio = new Set<String>();
        List<Analysis__c> analysisToDelete = new List<Analysis__c>();
        Map<Id,Id> sectorIdBrpdIdOldMap = new Map<Id,Id>();
        Map<Id,Id> sectorIdBrpdIdNewMap = new Map<Id,Id>();
        for(Benchmark_Risk_Profile_Detail__c brpd: trigger.old){
            if(brpd.Sector__c!=null){
                sectorIdBrpdIdOldMap.put(brpd.Sector__c,brpd.Id);
            }
        }
        system.debug('!!sectorIdBrpdIdOldMap'+sectorIdBrpdIdOldMap);
        for(Benchmark_Risk_Profile_Detail__c brpd: [select Id, Sector__c, Model_Portfolio__c from Benchmark_Risk_Profile_Detail__c]){
            portfoliosToCheck.add(brpd.Model_Portfolio__c);
            if(brpd.Sector__c!=null){
                sectorIdBrpdIdNewMap.put(brpd.Sector__c,brpd.Model_Portfolio__c);
            }
        }
        system.debug('!!sectorIdBrpdIdNewMap'+sectorIdBrpdIdNewMap);
        for(Id secIdNew: sectorIdBrpdIdNewMap.keySet()){
            for(Id secIdOld: sectorIdBrpdIdOldMap.keySet()){
                if(secIdOld != secIdNew){
                    existingSectorPortfolio.add(secIdNew + '-' + sectorIdBrpdIdNewMap.get(secIdNew));
                }
            }
        }
        
        for(Analysis__c a: [Select Sector__c, Model_Portfolio__c from Analysis__c where Model_Portfolio__c IN: portfoliosToCheck]) {
            if(existingSectorPortfolio.contains(a.Sector__c + '-' + a.Model_Portfolio__c)){
                analysisToDelete.add(a);
            }
        }
        system.debug('!!analysisToDelete'+analysisToDelete);
        if(analysisToDelete.size()>0){
            delete analysisToDelete;
        }
    }*/
}