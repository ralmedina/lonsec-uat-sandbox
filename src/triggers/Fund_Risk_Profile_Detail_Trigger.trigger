trigger Fund_Risk_Profile_Detail_Trigger on Fund_Risk_Profile_Detail__c (before insert, before update, before delete, after insert, after update, after delete) {
    /*if(trigger.isInsert || trigger.isUpdate){
        if(trigger.isBefore){
            Map<Id, List<Fund_Risk_Profile_Detail__c>> fundRiskProfileMap = new Map<Id, List<Fund_Risk_Profile_Detail__c>>();
            for(Fund_Risk_Profile_Detail__c frpd: trigger.new){
                if(fundRiskProfileMap.containsKey(frpd.Fund_Risk_Profile__c)) {
                    fundRiskProfileMap.get(frpd.Fund_Risk_Profile__c).add(frpd);
                } else {
                    fundRiskProfileMap.put(frpd.Fund_Risk_Profile__c, new List<Fund_Risk_Profile_Detail__c>{frpd});
                }
            }
            
            for(Id benchRiskProfileIds: fundRiskProfileMap.keySet()) {
                //per risk profile grouping, initialize the weightTotal to 0.
                Decimal weightTotal = 0;
                for(Fund_Risk_Profile_Detail__c frpd: fundRiskProfileMap.get(benchRiskProfileIds)) {
                    if(frpd.Current_Weight__c != null){
                        weightTotal += frpd.Current_Weight__c;
                    }
                }
                if(weightTotal > 100) {
                    for(Fund_Risk_Profile_Detail__c frpd: fundRiskProfileMap.get(benchRiskProfileIds)) {
                        frpd.addError('Weight must not exceed to 100%');
                    } 
                }
            }
        }
    }*/
    
    //add all funds with an existing combinations of FRP and sectors
    if(trigger.isInsert && trigger.isAfter) {
        
        //Map<String, Set<Fund_Risk_Profile_Detail__c>> toBeMatchedFunds = new Map<String,Set<Fund_Risk_Profile_Detail__c>>(); //key is model portfolio id - sector id. Value is Fund_Risk_Profile_Detail__c record
        Map<String, Set<Id>> existingFunds = new Map<String,Set<Id>>(); //key is model portfolio id - sector id. Value is fund id
        Map<String, Id> analysisParents = new Map<String,Id>(); //key is model portfolio id - sector id. Value is analysis id
        Set<Id> mpIds = new Set<Id>();
        Set<Id> sectorIds = new Set<Id>();
        List<Fund_Risk_Profile_Detail__c> frpdTriggerList = new List<Fund_Risk_Profile_Detail__c>();
        List<Fund_Commentaries__c> fcToInsert = new List<Fund_Commentaries__c>();
        
        //add all to-be-inspected mpIds and sectorIds
        for(Fund_Risk_Profile_Detail__c frpd: [Select   Fund__r.Investment_Consulting_Sector__c, 
                                                        Fund_Risk_Profile__r.Model_Portfolio__c,
                                                        Fund__c
                                               From     Fund_Risk_Profile_Detail__c
                                               WHERE    Id IN: trigger.newMap.keySet()]) {
            System.debug('jmd-frpd.Fund_Risk_Profile__r.Model_Portfolio__c: ' + frpd.Fund_Risk_Profile__r.Model_Portfolio__c);
            System.debug('jmd-frpd.Fund__r.Investment_Consulting_Sector__c: ' + frpd.Fund__r.Investment_Consulting_Sector__c);
            mpIds.add(frpd.Fund_Risk_Profile__r.Model_Portfolio__c);
            sectorIds.add(frpd.Fund__r.Investment_Consulting_Sector__c);
            frpdTriggerList.add(frpd);
        }
        
        //select all existing fund commentaries, match it with model portfolio id and sector
        for(Analysis__c a: [SELECT  Model_Portfolio__c, 
                                    Sector__c, 
                                    Id, 
                                    (Select Fund__c FROM Fund_Commentaries1__r)
                             FROM   Analysis__c
                             WHERE  Model_Portfolio__c IN: mpIds
                              AND   Sector__c IN: sectorIds]) {
            
            String mapKey = a.Model_Portfolio__c + '-' + a.Sector__c;
            for(Fund_Commentaries__c fc : a.Fund_Commentaries1__r) {
                if(existingFunds.containsKey(mapKey)) {
                    existingFunds.get(mapKey).add(fc.Fund__c );
                } else {
                    existingFunds.put(mapKey, new Set<Id> {fc.Fund__c});
                }
                System.debug('jmd-fc.Fund__c: ' + fc.Fund__c);
            }
            
            //map modelportfolio id and sector id combination with analysis id
            analysisParents.put(mapKey, a.Id);
            System.debug('jmd-a.Id: ' + a.Id);
            System.debug('jmd-mapKey: ' + mapKey);
            
        }

        //match the newly inserted funds, check if such funds already exists
        for(Fund_Risk_Profile_Detail__c frpd: frpdTriggerList) {
            
            String mapKey = frpd.Fund_Risk_Profile__r.Model_Portfolio__c + '-' + frpd.Fund__r.Investment_Consulting_Sector__c;
            System.debug('jmd-mapKey2: ' + mapKey);
            
            //by having the condition below = true, it implies that such analysis already exists to act as fund commentary parent
            System.debug('jmd-analysisParents.containsKey(mapKey): ' + analysisParents.containsKey(mapKey));
            if(analysisParents.containsKey(mapKey)) {
                //create a fund commentary if fund id is not present yet for model portfolio-sector combination
                System.debug('jmd-existingFunds.get(mapKey).contains(frpd.Fund__c): ' + existingFunds.get(mapKey).contains(frpd.Fund__c));
                Boolean createFundCommentary = true;
                
                System.debug('jmd-existingFunds.containsKey(mapKey): ' + existingFunds.containsKey(mapKey));
                if(existingFunds.containsKey(mapKey)) {
                    System.debug('jmd-existingFunds.get(mapKey).contains(frpd.Fund__c): ' + existingFunds.get(mapKey).contains(frpd.Fund__c));
                    if(existingFunds.get(mapKey).contains(frpd.Fund__c)) {
                        createFundCommentary = false;
                    }
                }
                
                if(createFundCommentary) {
                    fcToInsert.add(new Fund_Commentaries__c(Analysis__c = analysisParents.get(mapKey),
                                                            Fund__c = frpd.Fund__c
                                                            ));
                }
            }

        }
        System.debug('jmd-fcToInsert: ' + fcToInsert);
        if(fcToInsert.size()>0) {
            insert fcToInsert;
        }
    
    }
    
    if(trigger.isInsert || trigger.isUpdate){
        if(trigger.isAfter){
            Set<Id> mpIdSet = new Set<Id>();
            Set<Id> frpIdSet = new Set<Id>();
            for(Fund_Risk_Profile_Detail__c frpd: trigger.new){
                //mpIdSet.add(frpd.Fund_Risk_Profile__r.Model_Portfolio__c);
                frpIdSet.add(frpd.Fund_Risk_Profile__c);
            }
            for(Fund_Risk_Profile__c frp: [select    id,
                                                     Name,
                                                     Model_Portfolio__c
                                           from      Fund_Risk_Profile__c
                                           where     Id IN :frpIdSet]){
                mpIdSet.add(frp.Model_Portfolio__c);
            }
            //Fund_Risk_Profile_Detail_Trigger.onBefore(mpIdSet);
            
            ModelPortfolioTriggerHandler.archiveModelPortfolios(mpIdSet);
        }
    }
    
    if(trigger.isDelete){
        if(trigger.isAfter){
            Set<Id> mpIdSet = new Set<Id>();
            Set<Id> frpIdSet = new Set<Id>();
            for(Fund_Risk_Profile_Detail__c frpd: trigger.old){
                //mpIdSet.add(frpd.Fund_Risk_Profile__r.Model_Portfolio__c);
                frpIdSet.add(frpd.Fund_Risk_Profile__c);
            }
            for(Fund_Risk_Profile__c frp: [select    id,
                                                     Name,
                                                     Model_Portfolio__c
                                           from      Fund_Risk_Profile__c
                                           where     Id IN :frpIdSet]){
                mpIdSet.add(frp.Model_Portfolio__c);
            }
            //Fund_Risk_Profile_Detail_Trigger.onBefore(mpIdSet);
            
            ModelPortfolioTriggerHandler.archiveModelPortfolios(mpIdSet);
        }
    }
}