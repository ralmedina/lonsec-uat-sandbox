trigger QualitativeRating_Trigger on Qualitative_Rating__c (before insert, before update) {
	QualitativeRating_TriggerHandler handler = new QualitativeRating_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if (Trigger.isBefore){
		if (Trigger.isInsert || Trigger.isUpdate){
			handler.OnBeforeInsertUpdate(Trigger.new);
		}
	}
	
}