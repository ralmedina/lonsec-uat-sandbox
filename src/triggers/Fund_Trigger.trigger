trigger Fund_Trigger on Fund__c (after delete, after insert, after update) {
	Fund_TriggerHandler handler = new Fund_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if (Trigger.isAfter){
		if (Trigger.isInsert || Trigger.isUpdate){
			handler.OnAfterInsertUpdateDelete(Trigger.new);
		}else if (Trigger.isDelete){
			handler.OnAfterInsertUpdateDelete(Trigger.old);
		}
	}
	
}