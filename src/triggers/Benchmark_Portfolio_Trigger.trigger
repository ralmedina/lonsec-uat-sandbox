trigger Benchmark_Portfolio_Trigger on Benchmark_Portfolio__c (before insert, after insert, after update) {
     if(trigger.isInsert || trigger.isUpdate){
        if(trigger.isBefore){
            Map<Id, List<Benchmark_Portfolio__c>> modelPortWithBenchmarkPortMap = new Map<Id, List<Benchmark_Portfolio__c>>();
            
            // put model portfolio with list of its benchmark portfolio
            for(Benchmark_Portfolio__c bp: trigger.new){
                if(modelPortWithBenchmarkPortMap.containsKey(bp.Model_Portfolio__c)){
                    modelPortWithBenchmarkPortMap.get(bp.Model_Portfolio__c).add(bp);
                }else{
                    modelPortWithBenchmarkPortMap.put(bp.Model_Portfolio__c,new List<Benchmark_Portfolio__c>{bp});
                }
            }
            
            for(Id modelPort: modelPortWithBenchmarkPortMap.keySet()){
                Integer count = 0;
                for(Benchmark_Portfolio__c bp: modelPortWithBenchmarkPortMap.get(modelPort)){
                    if(bp.Default_Benchmark_Portfolio__c == true){
                        count++;
                    }
                    if(count > 1){
                        bp.addError('Each Model Portfolio must use only one(1) default benchmark portfolio');
                        count=1;
                    }
                }
            }
        }
    }
    
    if(trigger.isInsert || trigger.isUpdate){
        if(trigger.isAfter){
            Set<Id> mpIds = new Set<Id>();
            List<Benchmark_Portfolio__c> benchmarkToUpdate = new List<Benchmark_Portfolio__c>();
            for(Benchmark_Portfolio__c bp: trigger.new){
                if(bp.Default_Benchmark_Portfolio__c == true){
                    mpIds.add(bp.Model_Portfolio__c);
                }
            }
            
            for(Benchmark_Portfolio__c bp: [SELECT Id, Default_Benchmark_Portfolio__c, Model_Portfolio__c FROM Benchmark_Portfolio__c WHERE Default_Benchmark_Portfolio__c = true AND Model_Portfolio__c IN: mpIds AND Id NOT IN: trigger.newMap.keySet()]) {
                bp.Default_Benchmark_Portfolio__c = false;
                benchmarkToUpdate.add(bp);
            }
            
            if(benchmarkToUpdate.size()>0){
                update benchmarkToUpdate;
            }
            
        }
    }
}