trigger Fund_Recommendation_Trigger on Fund_Recommendation__c (before insert, before update, after update) {
	
	Fund_Recommendation_TriggerHandler handler = new Fund_Recommendation_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if (Trigger.isInsert && Trigger.isBefore){
		handler.OnBeforeInsert(Trigger.new);
	}
	
	if (Trigger.isUpdate && Trigger.isBefore){
		handler.OnBeforeUpdate(Trigger.new, Trigger.oldMap, Trigger.newMap);
	}
	
	if (Trigger.isUpdate && Trigger.isAfter){
		handler.OnAfterUpdate(Trigger.new, Trigger.oldMap);
	}
}