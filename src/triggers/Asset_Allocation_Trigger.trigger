trigger Asset_Allocation_Trigger on Asset_Allocation__c (before insert) {
	
	Asset_Allocation_TriggerHandler handler = new Asset_Allocation_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if (Trigger.isInsert && Trigger.isBefore){
		handler.OnBeforeInsert(Trigger.new);
	}
	
	/*if (Trigger.isUpdate && Trigger.isBefore){
		handler.OnBeforeUpdate(Trigger.new, Trigger.oldMap, Trigger.newMap);
	}
	
	if (Trigger.isUpdate && Trigger.isAfter){
		handler.OnAfterUpdate(Trigger.new, Trigger.oldMap);
	}*/
	
}