trigger Benchmark_Code_Trigger on Benchmark_Code__c (before insert, after insert, before update, after update) {
    if(trigger.isInsert || trigger.isUpdate){
        if(trigger.isAfter){
            Id bcIds;
            Map<Id,List<Benchmark_Code__c>> bcWithBcCodeMap = new Map<Id,List<Benchmark_Code__c>>();
            Map<Id, Benchmark_Code__c> bcToUpdate = new Map<Id, Benchmark_Code__c>();
            List<Benchmark_Code__c> bcToUpdate1 = new List<Benchmark_Code__c>();
            
            for(Benchmark_Code__c bc: trigger.new){
                if(bc.Used_for_Growth_Series__c == true){
                    if(bc.Benchmark__c!=null) {
                        if(bcWithBcCodeMap.containsKey(bc.Benchmark__c)){
                            bcWithBcCodeMap.get(bc.Benchmark__c).add(bc);
                        }else{
                            bcWithBcCodeMap.put(bc.Benchmark__c,new List<Benchmark_Code__c>{bc});
                        }
                    }
                }
                if(bc.Default_Cash_Growth_Series__c == true){
                    bcIds = bc.Id;
                    System.debug('jmdbg--bcIds inserted:' + bcIds);
                }
            }
            
            if(bcWithBcCodeMap.size()>0) {
                for(Benchmark_Code__c bc: [select Id, Name, Benchmark__c, Used_for_Growth_Series__c, Default_Cash_Growth_Series__c from Benchmark_Code__c where Used_for_Growth_Series__c = true and Id NOT IN :trigger.newMap.keySet() and Benchmark__c IN :bcWithBcCodeMap.keySet()]){
                    bc.Used_for_Growth_Series__c = false;
                    bcToUpdate.put(bc.Id, bc);
                }
            }
            
            System.debug('jmdbg-bcIds to query:' + bcIds);
            if(bcIds!=null){
                for(Benchmark_Code__c bc: [select Id, Name, Benchmark__c, Used_for_Growth_Series__c, Default_Cash_Growth_Series__c from Benchmark_Code__c where Default_Cash_Growth_Series__c = true and Id != :bcIds]){
                    bc.Default_Cash_Growth_Series__c = false;
                    if(bcToUpdate.containsKey(bc.Id)) {
                        bcToUpdate.get(bc.Id).Default_Cash_Growth_Series__c = false;
                    } else {
                        bcToUpdate.put(bc.Id, bc);
                    }
                }
            }
        
            System.debug('jmdbg-bcToUpdate content:' + bcToUpdate);
            if(bcToUpdate.size()>0) {
                update bcToUpdate.values();
            }
            List<Benchmark__c> bList = new List<Benchmark__c>();
            for(Benchmark__c b: [select Id, Benchmark_Code_Used_for_Growth_Series__c, (select Id, Name, Benchmark__r.Id, Default_Cash_Growth_Series__c, Used_for_Growth_Series__c from Benchmark_Codes__r where Used_for_Growth_Series__c = true) from Benchmark__c where Id IN :bcWithBcCodeMap.keySet()]){
                for(Benchmark_Code__c bc: b.Benchmark_Codes__r){
                    if(bc.Benchmark__r.Id == b.Id){
                        b.Benchmark_Code_Used_for_Growth_Series__c = bc.Id;
                        bList.add(b);
                    }
                }
            }
            update bList;
        }
    }

    if(trigger.isInsert || trigger.isUpdate){
        if(trigger.isBefore){
            //Integer counter = 0;
            Map<Id,List<Benchmark_Code__c>> bcWithBcCodeMap = new Map<Id,List<Benchmark_Code__c>>();
            for(Benchmark_Code__c bc: trigger.new){
                if(bcWithBcCodeMap.containsKey(bc.Benchmark__c)){
                    bcWithBcCodeMap.get(bc.Benchmark__c).add(bc);
                }else{
                    bcWithBcCodeMap.put(bc.Benchmark__c,new List<Benchmark_Code__c>{bc});
                }
            }
            
            for(Id b: bcWithBcCodeMap.keySet()){
                Integer count = 0;
                for(Benchmark_Code__c bc: bcWithBcCodeMap.get(b)){
                    if(bc.Used_for_Growth_Series__c == true){
                        count++;
                    }
                    if(count>1) {
                        bc.addError('Each Benchmark must only use 1 Benchmark Code for growth');
                        count = 1;
                    }
                }
            }
            
            /*for(Benchmark_Code__c bc: trigger.new){
                if(bc.Default_Cash_Growth_Series__c == true){
                    counter++;
                }
                if(counter>1) {
                    bc.addError('Only one(1) Benchmark Code can use as Default Cash Growth Series');
                    counter = 1;
                }
            }*/
        }
    }
}