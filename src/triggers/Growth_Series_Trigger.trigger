trigger Growth_Series_Trigger on Growth_Series__c (after insert, after update) {
    if((trigger.isUpdate || trigger.isInsert) && trigger.isAfter) {
        List<Growth_Series__c> gcWithTax = new List<Growth_Series__c>();
        List<Growth_Series__c> gcWithBCs = new List<Growth_Series__c>();
        
        for(Growth_Series__c gs: trigger.new) {
            if(gs.Benchmark_Code__c!=null) {
                gcWithBCs.add(gs);
            } else if(gs.Tax_Structure_Investment_Code__c!=null) {
                gcWithTax.add(gs);
            }
        }
        
        if(gcWithBCs.size()>0) {
            Growth_Series_Trigger_Handler.handleGSBenchmarkCodes(gcWithBCs);
        } 
        
        if(gcWithTax.size()>0) {
            Growth_Series_Trigger_Handler.handleGSTaxStructures(gcWithTax);
        }
        
    }
}