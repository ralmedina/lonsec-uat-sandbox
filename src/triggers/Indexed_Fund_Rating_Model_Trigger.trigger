trigger Indexed_Fund_Rating_Model_Trigger on Indexed_Fund_Rating_Model__c (after insert) {
	
	Indexed_Fund_Rating_Model_TriggerHandler handler = new Indexed_Fund_Rating_Model_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if (Trigger.isInsert && Trigger.isAfter){
		handler.OnAfterInsert(Trigger.new, Trigger.newMap.keySet());
	}
}