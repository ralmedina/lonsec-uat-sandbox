trigger Tax_Structure_Trigger_APIRcode on Tax_Structure__c (after insert, after update, after delete) {
    if(trigger.isInsert || trigger.isUpdate){
        if(trigger.isAfter){
            Set<Id> parentIdsFund = new Set<Id>();
            for(Tax_Structure__c ts: trigger.new){
                parentIdsFund.add(ts.Fund__c);
            }
            Tax_Structure_Trigger_APIRcode_Handler.onAfterInsertUpdate(parentIdsFund);
        }
    }
    
    if(trigger.isDelete){
        if(trigger.isAfter){
            Set<Id> parentIdsFund = new Set<Id>();
            for(Tax_Structure__c ts: trigger.old){
                parentIdsFund.add(ts.Fund__c);
            }
            Tax_Structure_Trigger_APIRcode_Handler.onAfterInsertUpdate(parentIdsFund);
        }
    }
}