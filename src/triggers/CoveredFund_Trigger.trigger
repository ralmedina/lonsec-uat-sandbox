trigger CoveredFund_Trigger on Covered_Fund__c (after update) {
	CoveredFund_TriggerHandler handler = new CoveredFund_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if (Trigger.isAfter){
		if (Trigger.isUpdate){
			handler.OnAfterUpdate(Trigger.new);
		}
	}
	
}