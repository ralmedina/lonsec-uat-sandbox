trigger Model_Portfolio_Trigger on Model_Portfolio__c (after update, after insert) {
     if(trigger.isUpdate || trigger.isInsert){
        if(trigger.isAfter){
            Set<Id> mpIdSet = new Set<Id>();
            for(Model_Portfolio__c  mp: trigger.new){
                mpIdSet.add(mp.Id);
            }
            ModelPortfolioTriggerHandler.archiveModelPortfolios(mpIdSet);
        }
    }
}