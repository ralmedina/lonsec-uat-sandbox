/*
@Name        :Update_SRP_CountOf_SRF
@Description :Trigger is written to get all existing covered funds related to the same SRP and checking their style value 
              If they have the same style value, createed records on the new object (called Related Style) using their id on the lookup field 
              If the record is updated and the style is changed, deleted all the records on the Related Style object who's covered fund id is the same as the record that triggered the event
@author      :Rimali Gurav  
@date        :29th May 2013
*/


trigger Update_SRP_CountOf_SRF on Covered_Fund__c (after delete, after insert, after undelete, after update, before delete) {

    set<id> SRPId = new set<id>();
    Set<Id> cfSRP = new Set<Id>();
    
    if(trigger.isBefore && trigger.isDelete){
        
        Covered_Fund_Trigger_Class.checkStyledelete(trigger.old);
        Covered_Fund_Trigger_Class.deleteRelatedRating(trigger.old);
    }
    
    if(trigger.isAfter){
        
        if(!CoverFund_ToAvoidRecursion_Class.hasExecutedflag()){
            
            CoverFund_ToAvoidRecursion_Class.setExecutedflag();
            
            if (trigger.isUnDelete ){
                for(Covered_Fund__c  objCF : trigger.new){
                    SRPId.add(objCF.Sector_Review_Plan_Record__c);
                }

                Covered_Fund_Trigger_Class.callTrigger(SRPId);
                Covered_Fund_Trigger_Class.CallQualityRank(SRPId);
            }
            
            if (trigger.isInsert ){
                for(Covered_Fund__c  objCF : trigger.new){
                    SRPId.add(objCF.Sector_Review_Plan_Record__c);
                }
            
                Covered_Fund_Trigger_Class.callTrigger(SRPId);
                Covered_Fund_Trigger_Class.CallQualityRank(SRPId);
                //Covered_Fund_Trigger_Class.checkStyle(SRPId, trigger.new);
                Covered_Fund_Trigger_Class.checkStyleManager(trigger.new);
                Covered_Fund_Trigger_Class.createRelatedRating(trigger.new);
            }
            
            if(trigger.isDelete ) {
                for(Covered_Fund__c  objCF : trigger.old){
                    SRPId.add(objCF.Sector_Review_Plan_Record__c);
                }
            
                Covered_Fund_Trigger_Class.callTrigger(SRPId);
                Covered_Fund_Trigger_Class.CallQualityRank(SRPId);
                
            }
            
            /*if(trigger.isUpdate){
                for(Covered_Fund__c  objCF : trigger.new){
                    SRPId.add(objCF.Sector_Review_Plan_Record__c);
                }
                
                Covered_Fund_Trigger_Class.CallQualityRank(SRPId);
                Covered_Fund_Trigger_Class.checkStyleUpdate(SRPId, trigger.new, trigger.oldmap);
            
            }*/
            if(trigger.isUpdate){
                for(Covered_Fund__c cf: trigger.new){
                    cfSRP.add(cf.Sector_Review_Plan_Record__c);
                }
                    Covered_Fund_Trigger_Class.checkStyleManager(trigger.new);
                    Covered_Fund_Trigger_Class.createRelatedRating(trigger.new);
                    Covered_Fund_Trigger_Class.CallQualityRank(cfSRP);
            }
        }
    }
}