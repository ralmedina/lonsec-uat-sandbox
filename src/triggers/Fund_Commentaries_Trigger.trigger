trigger Fund_Commentaries_Trigger on Fund_Commentaries__c (after insert, after update) {
    if(trigger.isInsert || trigger.isUpdate){
        if(trigger.isAfter){
            Set<Id> anaIdSet = new Set<Id>();
            Set<Id> mpIdSet = new Set<Id>();
            for(Fund_Commentaries__c a: trigger.new){
                anaIdSet.add(a.Analysis__c);
            }
            
            for(Analysis__c a: [select    Id,
                                          Model_Portfolio__c,
                                          Sector__c,
                                          Comment__c
                                from      Analysis__c
                                where     Id IN :anaIdSet]){
                mpIdSet.add(a.Model_Portfolio__c);
            }
            ModelPortfolioTriggerHandler.archiveModelPortfolios(mpIdSet);
        }
    }
}