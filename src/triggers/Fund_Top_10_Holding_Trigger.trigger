trigger Fund_Top_10_Holding_Trigger on Fund_Top_10_Holding__c (before insert) {
	
	Fund_Top_10_Holding_TriggerHandler handler = new Fund_Top_10_Holding_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if (Trigger.isInsert && Trigger.isBefore){
		handler.OnBeforeInsert(Trigger.new);
	}
	
	/*if (Trigger.isUpdate && Trigger.isBefore){
		handler.OnBeforeUpdate(Trigger.new, Trigger.oldMap, Trigger.newMap);
	}
	
	if (Trigger.isUpdate && Trigger.isAfter){
		handler.OnAfterUpdate(Trigger.new, Trigger.oldMap);
	}*/
	
}