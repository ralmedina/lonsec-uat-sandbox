trigger FundRating_Trigger on Fund_Rating__c (before insert) {
	FundRating_TriggerHandler handler = new FundRating_TriggerHandler(Trigger.isExecuting, Trigger.size);
	
	if (Trigger.isBefore){
		if (Trigger.isInsert){
			handler.OnBeforeInsert(Trigger.new);
		}
	}
	
}